<?php 
include "headerccc.php";
include "menubarccc.php";
?>
<BR><BR>
<section class="service style-2 sec-padd" style="margin-top:-70px;">
    <div class="container">
      <div class="col-lg-9 col-md-8 col-sm-12">
            <div class="outer-box">
                <div class="row">
                <div class="section-title">
            <h2>แผนยุทธศาสตร์</h2>
        </div>                 
                    <!-- <div class="gfg"></div>
                    <div class="geeks"></div> -->
                    <div class="geeks_content">
                    <ul class="brochures-lists">
                   
                            <li><A HREF='https://drive.google.com/file/d/1GnY9452wDrlscL02VR7KZ0d3y48gkitN/view?usp=sharing' target='_blank'><span
                                        class='fa fa-file-pdf-o fa-lg'></span>ตัวชี้วัดเป้าประสงค์-2563 ไตรมาส 4</A></li>
                            
                            <li><A HREF='https://drive.google.com/file/d/1ZuTMFkwS7xcAPdEJM-hDLTncDh9rzs66/view?usp=sharing' target='_blank'><span
                                        class='fa fa-file-pdf-o fa-lg'></span>ตัวชี้วัดโครงการ-2563 ไตรมาส 4</A></li>
                                        <li><A HREF='https://drive.google.com/file/d/1Gd2uAxnahD5_67ooCJR9oNddM5eNVDDg/view?usp=sharing' target='_blank'><span
                                        class='fa fa-file-pdf-o fa-lg'></span>แผนยุทธศาสตร์-ระยะ20ปี-พ.ศ.2560-2579-วไลยอลงกรณ์2</A></li>
                                        <li><A HREF='https://drive.google.com/file/d/1nW_w-pqwFexabPmgHYfIbKf3TNrIkFI5/view?usp=sharing' target='_blank'><span
                                        class='fa fa-file-pdf-o fa-lg'></span>แผนกลยุทธ-ปี-60-64คณะวิทย์ปรับปรุงตามแผนมหาวิทยาลัยตุลาคม 63</A></li>
                                        <li><A HREF='https://drive.google.com/file/d/1MmRp02LmkMv3dcdJ3hpZD3af-eHjmkF5/view?usp=sharing' target='_blank'><span
                                        class='fa fa-file-pdf-o fa-lg'></span>แผนปฏิบัติการคณะวิทยาศาสตร์และเทคโนโลยีปี 2564</A></li>
                                        <li><A HREF='https://drive.google.com/file/d/1nW_w-pqwFexabPmgHYfIbKf3TNrIkFI5/view?usp=sharing' target='_blank'><span
                                        class='fa fa-file-pdf-o fa-lg'></span>แผนยุทธศาสตร์ คณะวิทย์ 2560-2564</A></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<BR><BR>
<?php
include "footerccc.php";
?>