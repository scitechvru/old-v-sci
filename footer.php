<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
.footer {
   position: relative;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: #2f2f2f;
   color: #fff;
   text-align: center;
   font: 12px Montserrat, sans-serif;
   line-height: 1.5;  
}
</style>
</head>
<body>

<!-- start footer -->    
  <div class="footer">
	  <BLOCKQUOTE>
		  Copyright © 2019 คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์    <br> 
		 อาคาร 5 เลขที่ 1 หมู่ 20 ต.คลองหนึ่ง อ.คลองหลวง จ.ปทุมธานี 13180
	  </BLOCKQUOTE>
  </div>
  <div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
  </div>
    <!-- end footer -->
</body>
</html> 
