<?php include "chksession_admin.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>SCI VRU</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: max;}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
 
li a.active {
  background-color: #2B80E7;
  color: white;
}

li a:hover:not(.active) {
  background-color: #2B80E7;
  color: white;
}
    
    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 700px) {
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse visible-xs">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="user_home.php"><span class="glyphicon glyphicon-home"> HOME </span></a></li>
        <li><a href="user_profile.php"><span class="glyphicon glyphicon-user"> ประวัติส่วนตัว </span></a></li>
        <li><a href="user_ed.php"><span class="glyphicon glyphicon-education"> ประวัติการศึกษา </span></a></li>
        <li><a href="user_ap.php"><span class="glyphicon glyphicon-king"> ตำแหน่งวิชาการ </span></a></li>
        <li><a href="user_ex.php"><span class="glyphicon glyphicon-tower"> ตำแหน่งทางการบริหาร </span></a></li>
		<li><a href="#"><span class="glyphicon glyphicon-folder-open"> ผลงาน</span></a></li>
		<li><a href="#"><span class="glyphicon glyphicon-retweet"> เปลี่ยนรหัสผ่าน</span></a></li>
		<li><a href="admin_logout.php"><span class="glyphicon glyphicon-log-in"> Logout </span></a></li>

      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row content" >
    <div class="col-sm-3 sidenav hidden-xs">
      <h2>Logo</h2>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="user_home.php"><span class="glyphicon glyphicon-home"> HOME </span></a></li>
        <li><a href="user_profile.php"><span class="glyphicon glyphicon-user"> ประวัติส่วนตัว </span></a></li>
        <li><a href="user_ed.php"><span class="glyphicon glyphicon-education"> ประวัติการศึกษา </span></a></li>
        <li><a href="user_ap.php"><span class="glyphicon glyphicon-king"> ตำแหน่งวิชาการ </span></a></li>
        <li><a href="user_ex.php"><span class="glyphicon glyphicon-tower"> ตำแหน่งทางการบริหาร </span></a></li>
		<li><a href="#"><span class="glyphicon glyphicon-folder-open"> ผลงาน</span></a></li>
		<li><a href="user_chpass.php"><span class="glyphicon glyphicon-retweet"> เปลี่ยนรหัสผ่าน </span></a></li>
		<li><a href="admin_logout.php"><span class="glyphicon glyphicon-log-in"> Logout </span></a></li>

      </ul><br>
    </div>
    <br>
</body>
</html>
