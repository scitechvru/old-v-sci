<?php include "headerccc.php"?>
<?php include "menubarccc.php"?>
<br>
<!--///////////////////////////////////////////////////////////////////////////////////////////////-->
<div class="container">
    <h1 class="my-4" style="font-family: 'Fira Sans Extra Condensed', sans-serif;color: #086A87; font-size:50px">
        Philosophy Vision Mission Identity</h1>
    <div class="row" style="text-align: left">
        <h3 class="my-4" style="font-family: 'Fira Sans Extra Condensed', sans-serif;color: #086A87; font-size:30px">
            ปรัชญา</h3>
        <h4>
            <BLOCKQUOTE>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                วิทยาศาสตร์ก้าวล้ำ คุณธรรมเด่น เน้นการพัฒนา ด้วยภูมิปัญญาและเทคโนโลยี
            </BLOCKQUOTE>
        </h4>
    </div>
    <div class="row" style="text-align: left">
        <h3 class="my-4" style="font-family: 'Fira Sans Extra Condensed', sans-serif;color: #086A87; font-size:30px">
            วิสัยทัศน์</h3>
        <h4>
            <BLOCKQUOTE>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            ร่วมแรงร่วมใจในการยกระดับคุณภาพและคุณค่าขององค์กรอย่างต่อเนื่อง  ผ่านการผลิตบัณฑิตที่รับผิดชอบต่อสังคม สร้างนวันกรรมเชิงบูรณาการที่ตอบสนองต่อความต้องการของผู้มีส่วนได้ส่วนเสีย และการเสริมสร้างความเข้มแข็งให้กับชุมชนภาคีเครือข่ายผ่านงานพันธกิจสัมพันธ์
            </BLOCKQUOTE>
        </h4>
    </div>
    <div class="row" style="text-align: left">
        <h3 class="my-4" style="font-family: 'Fira Sans Extra Condensed', sans-serif;color: #086A87; font-size:30px">
            พันธกิจ</h3>
        <h4>
            <BLOCKQUOTE>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            1. รับผิดชอบต่อสังคมในการผลิตบัณฑิตที่มีความรู้คู่คุณธรรม ส่งเสริมการเรียนรู้ตลอดชีวิตในชุมชน ด้วยวิทยาศาสตร์และเทคโนโลยี โดยการจัดการเรียนรู้เชิงผลิตภาพ (Productive Learning) ด้วยรูปแบบ ABCD เพื่อช่วยให้คนในท้องถิ่นรู้เท่าทันการเปลี่ยนแปลง <br>
2. ศึกษา วิจัย แสวงหาความจริง บนพื้นฐานของภูมิปัญญาท้องถิ่น ภูมิปัญญาไทยและภูมิปัญญาสากล รวมถึงการแสวงหาแนวทางเพื่อส่งเสริมให้เกิดการจัดการ การบำรุงรักษา และการใช้ประโยชน์จากทรัพยากรธรรมชาติและสิ่งแวดล้อมอย่างสมดุลและยั่งยืน สู่การบูรณาการที่ตอบสนองความต้องการในการพัฒนาชุมชนและท้องถิ่น <br>
     	3. บูรณาการภูมิปัญญาพื้นบ้านกับเทคโนโลยีสมัยใหม่ที่เหมาะสมกับการดำรงชีวิต และการประกอบอาชีพของคนในท้องถิ่น เสริมสร้างงานพันธกิจสัมพันธ์ด้วยการประสานความร่วมมือระหว่างหน่วยงานและผู้มีส่วนได้ส่วนเสีย เพื่อความเข้มแข็งของคนในชุมชน     <br>
4.เสริมสร้างความรู้ความเข้าใจในคุณค่า ความสำนึก และความภูมิใจในวัฒนธรรมและภูมิปัญญาของท้องถิ่น มีการบูรณาการเข้ากับงานด้านพันธกิจสัมพันธ์และวิจัย และมีการเชิดชูเผยแพร่ จนได้รับการยกย่องจากชุมชน ภาคีเครือข่าย ผู้มีส่วนได้ส่วนเสียในภาคต่างๆ อย่างต่อเนื่อง<br>
5. ประสานความร่วมมือและช่วยเหลือเกื้อกูลของบุคลากรในองค์กร เพื่อการยกระดับการบริหารจัดการภายในที่ดี เป็นแบบอย่างให้กับหน่วยงานภายนอกได้ <br> 
        	6. เสริมสร้างผู้ประกอบการรายใหม่จากศิษย์ปัจจุบัน ศิษย์เก่า ชุมชน ผู้มีส่วนได้ส่วนเสีย ผ่านระบบบริหารจัดการ และการเสริมสร้างองค์ความรู้ที่เหมาะสม

            </BLOCKQUOTE>
        </h4>
    </div>
    <div class="row" style="text-align: left">
        <h3 class="my-4" style="font-family: 'Fira Sans Extra Condensed', sans-serif;color: #086A87; font-size:30px">
            เอกลักษณ์</h3>
        <h4>
            <BLOCKQUOTE>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                เป็นคณะที่น้อมนำแนวทางการดำเนินชีวิตตามหลักเศรษฐกิจพอเพียง
            </BLOCKQUOTE>
        </h4>
    </div>
    <div class="row" style="text-align: left">
        <h3 class="my-4" style="font-family: 'Fira Sans Extra Condensed', sans-serif;color: #086A87; font-size:30px">
            อัตลักษณ์</h3>
        <h4>
            <BLOCKQUOTE>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                บัณฑิตจิตอาสา พัฒนาท้องถิ่น
            </BLOCKQUOTE>
        </h4>
    </div>
</div>

<BR><BR>
<?php include "footerccc.php" ?>