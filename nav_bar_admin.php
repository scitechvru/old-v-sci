<?php include "chksession_admin.php"; ?>
<html lang="en">
<head>
  <title>SCI VRU</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
	.navbar {
     margin-bottom: 0;
      border-color:#555555;
  	  background-color: #555555;
	}

    }
  </style>
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar" >
      <ul class="nav navbar-nav ">
		  <li><a href="admin_home.php">HOME</a></li>
		  <li><a href="admin_major.php">หน่วยงาน</a></li>
		  <li><a href="admin_per.php">บุคลากร</a></li>
		  <li><a href="admin_files.php">แบบฟอร์ม</a></li>   
		  <li><a href="admin_report.php">วาระการประชุม</a></li>   
		  <li><a href="admin_banner.php">Banner</a></li>
		  <li><a href="admin_image.php">กิจกรรม</a></li>
		  <li><a href="admin_news.php">News</a></li>   
		  <li><a href="admin_admins.php">Admin</a></li>   
	  </ul>
      <ul class="nav navbar-nav navbar-right">
		<li><a href="admin_logout.php"> Logout <span class="glyphicon glyphicon-log-in"></span></a></li>
	  </ul>
    </div>
  </div>
</nav>


</body>
</html>
