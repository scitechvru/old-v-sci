<?php include 'headerccc.php';?>
<?php include 'menubarccc.php';
?>
<br>
<?php  include 'banner.php';?>
<br>
<?php include 'Newccc.php';?>


<!--start activity -->
<section class="service sec-padd2">
    <div class="container">
        <div class="section-title">
            <h2>กิจกรรม</h2>
        </div>
        <p align="right" style="margin-top:-25px;"><a href="activityccc.php"><i class="fa fa-picture-o"></i>
                กิจกรรมทั้งหมด</a> </p>
        <div class="service_carousel">
            <?php
require_once 'inc/connect.php';
$no = 1;
$sql = "select * from tb_picup where major_img='OF' order by date_img desc";
$result = $conn->query($sql) or die($conn->error);
if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()){
        if ($title_img1 != $row[title_img]) {
            $title_img1 = $row[title_img];
/* $result = mysql_db_query($dbname, $sql); */

/* $num = mysql_num_rows($result);
if ($num > 0) {
    while ($r = mysql_fetch_array($result)) {
            $date_img = $r[date_img];
            $place_img = $r[place_img];
            $cover_img = $r[cover_img];
            $major_img1 = $r[major_img];
            $Date = explode('-', $date_img);
            $date_img = $Date[2] . '-' . $Date[1] . '-' . $Date[0]; */?>
            <!--Featured Service -->
            <article class="single-column">
                <div class="item">
                    <figure class="img-box">
                        <img src="backend/pic_upload/<?php echo $row['cover_img']; ?>" alt="" width="260" height="160">
                        <figcaption class="default-overlay-outer">
                            <div class="inner">
                                <div class="content-layer">
                                    <a href="view_activity.php?id_cover=<?php echo $row['cover_img']; ?>"
                                    class="thm-btn thm-tran-bg">อ่านต่อ</a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="content center">
                        <div class="text">
                            <p style="font-size:13px; text-align:left;"><?php echo $title_img1; ?></p>
                        </div>
                        <p style="font-size:13px; color:#14944C;" align="right"><i class="fa fa-calendar"></i>
                        <?php echo date_format(new DateTime($row['date_img']),"j F Y"); ?></p>
                        <p style="font-size:13px; color:#14944C;" align="right"><i class="fa fa-flag"></i>
                        <?php echo $row['place_img'];?></p>
                    </div>
                </div>
            </article>
            <?php
        if ($no == 5) {
            break;
        } //endif
        $no++;
    } //endwhile
} //end if
} //end if

?>
        </div>
    </div>
</section>
<!-- end activity-->
<section class="service">
    <div class="container">
        <div class="section-title">
            <h2>สื่อวิดีโอเผยแพร่</h2>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <div class="service_carousel">
                    <?php

$no = 1;
$sql = "select * from tb_youtube where major_youtube='OF' order by date_youtube desc";
$result = $conn->query($sql) or die($conn->error);
if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()){
        if ($title_img1 != $row[youtube_code]) {
            $title_img1 = $row[youtube_code];
?>
                    <!--Featured Service -->
                    <article class="single-column">
                        <div class="item">
                            <figure class="img-box">
                                <iframe width="260" height="160"
                                    src="https://www.youtube.com/embed/<?php echo $title_img1;?>" frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                            </figure>
                            <div class="content center">
                                <div class="text">
                                    <p style="font-size:13px; text-align:left;"><?php echo $row['title_youtube']; ?></p>
                                </div>
                                <p style="font-size:13px; color:#14944C;" align="right"><i class="fa fa-calendar"></i>
                                    <?php echo date_format(new DateTime($row['date_youtube']),"j F Y");  ?></p>
                                <p style="font-size:13px; color:#14944C;" align="right"><a
                                        href="<?php echo $row['link_youtube']; ?>" target="_blank"
                                        rel="noopener noreferrer">รับชมเพิ่มเติม</a>
                                </p>
                            </div>
                        </div>
                    </article>
                    <?php
} //end if
if ($no == 5) {
    break;
} //endif
$no++;
    } //endwhile
} //end if

?>
                </div>
                <!--<iframe width="260" height="160" src="https://www.youtube.com/embed/RQk8PlwXc5M" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>-->
            </div>
        </div>
    </div>
</section>
<br>
<br>
<!--calender-->
<section class="service">
    <div class="container">
        <div class="section-title">
            <h2>ปฏิทินกิจกรรมมหาวิทยาลัย</h2>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <iframe
                    src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=300&amp;wkst=1&amp;bgcolor=%23ffffff&amp;src=plan.vru2011%40gmail.com&amp;color=%23182C57&amp;ctz=Asia%2FBangkok"
                    style=" border-width:0 " width="100%" height="500" frameborder="0" scrolling="no"></iframe>
            </div>
        </div>
    </div>
</section>
<!--end calender -->
<br>
<!--calender
<section class="service">
    <div class="container">
        <div class="section-title">
            <h2>ปฏิทินการศึกษานักศึกษาระดับปริญญาตรีเต็มเวลา จันทร์-ศุกร์ ภาคการศึกษา 1/2563</h2><br>
            <p>กำหนดการพบอาจารย์ที่ปรึกษาทุกวันพุธแรกของเดือน ตรวจสอบเวลาและห้องพบอาจารย์ที่ปรึกษาได้ที่ตารางเรียน</p>
        </div>
        <div class="row">
            <table class="table table-light">
                <thead class="thead-light">
                    <tr>
                        <th class="text-center">กำหนดการ</th>
                        <th class="text-center">ระยะเวลา</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">การจองรายวิชาผ่านระบบออนไลน์ของทุกชั้นปี</td>
                        <td class="text-center">28 มีนาคม - 31 พฤษภาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">การชำระเงินลงทะเบียนเรียนที่มหาวิทยาลัย
                            ธนาคารกรุงไทยและเคาน์เตอร์เซอร์วิส ทุกสาขา</td>
                        <td class="text-center">1 มิถุนายน - 21 กรกฏาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            การชำระเงินลงทะเบียนเรียนพร้อมค่าปรับการลงทะเบียนเรียนช้ากว่ากำหนด(ชำระที่มหาวิทยาลัยเท่านั้น)
                        </td>
                        <td class="text-center">22 กรกฏาคม - 14 ตุลาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">การยื่นคำร้องขอเปิดหมู่พิเศษ</td>
                        <td class="text-center">8 มิถุนายน - 7 กรกฏาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">เปิดภาคการศึกษา</td>
                        <td class="text-center">1 กรกฏาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">การเทียบโอนรายวิชาสำหรับนักศึกษาใหม่ ภาคการศึกษาที่ 1/2563</td>
                        <td class="text-center">1 กรกฏาคม - 20 ตุลาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">การขอเพิ่ม ขอถอนรายวิชา</td>
                        <td class="text-center">1 - 21 กรกฏาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">การขอยกเลิกรายวิชาเรียน</td>
                        <td class="text-center">22 กรกฏาคม - 14 ตุลาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">สอบกลางภาค</td>
                        <td class="text-center">19 - 25 สิงหาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">ประกาศคะแนนสอบกลางภาค</td>
                        <td class="text-center">26 สิงหาคม - 8 กันยายน 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">ประกาศคะแนนเก็บระหว่างภาค</td>
                        <td class="text-center">30 กันยายน - 6 ตุลาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">ประเมินผลการสอนและประเมินอาจารย์ที่ปรึกษาออนไลน์</td>
                        <td class="text-center">30 กันยายน - 20 ตุลาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">วันสุดท้ายของการเรียน</td>
                        <td class="text-center">20 ตุลาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">สอบปลายภาครายวิชาศึกษาทั่วไป</td>
                        <td class="text-center">21 - 22,26 ตุลาคม 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">สอบปลายภาครายวิชาของคณะ</td>
                        <td class="text-center">27 - 30 ตุลาคม , 2 - 4 พฤศจิกายน 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">วันสิ้นสุดภาคการศึกษาที่ 1/2563</td>
                        <td class="text-center">4 พฤศจิกายน 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">วันสุดท้ายของการส่งผลการเรียนแก้ I ภาคการศึกษาที่ 2/2562</td>
                        <td class="text-center">4 พฤศจิกายน 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">วันสุดท้ายของการส่งผลการเรียน ภาคการศึกษาที่ 1/2563</td>
                        <td class="text-center">13 พฤศจิกายน 2563</td>
                    </tr>
                    <tr>
                        <td class="text-center">เปิดภาคการศึกษาที่ 2/2563</td>
                        <td class="text-center">16 พฤศจิกายน 2563</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</section>
end calender -->
<br>
<!-- Load Facebook SDK for JavaScript -->

<?php include 'footerccc.php';?>
</body>

</html>