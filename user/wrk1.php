<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>ประวัติการทำงาน</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มประวัติการทำงาน</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if(isset($_POST["submit"])){
                            addWork($_POST["id"],$_POST["ms_perwo"],$_POST["now_perwo"],$_POST["me_perwo"],$_POST["position_perwo"],
                            $_POST["organi_perwo"]);
                      } ?>

                            <form NAME="" METHOD="POST" ACTION="" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <input type="hidden" name="id" value="<?php echo "$idcard_per ";?>">
                                    
                                        <!-- text input -->
                                        <div class="col">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>วันที่เริ่มทำงาน</label>
                                                <input type="date" name="ms_perwo" id="ms_perwo" class="form-control" require>
                                            </div>
                                        </div>
                                    
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>วันที่ออกจากงาน หรือ ปัจจุบัน</label>
                                            <INPUT TYPE="checkbox" NAME="now_perwo" id="now_perwo" VALUE="1">ปัจจุบัน
                                            <input type="date" name="me_perwo" id="me_perwo" class="form-control" require>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อตำแหน่ง</label>
                                            <input type="text" name="position_perwo" id="position_perwo" class="form-control" require>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>หน่วยงาน</label>
                                            <input type="text" name="organi_perwo" id="organi_perwo" class="form-control" require>
                                        </div>
                                    </div>
                                </div>




                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>