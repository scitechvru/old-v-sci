<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>เพิ่มงานประชุมวิชาการ</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มงานประชุมวิชาการ</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if(isset($_POST["submit"])){
                            addConfer($_POST["id"],$_POST["assr_proceeding"],$_POST["assy_proceeding"],$_POST["parti_proceeding"],$_POST["part_proceeding"],
                            $_POST["level_proceeding"],$_POST["type2_proceeding"],$_POST["worke_proceeding"],$_POST["workt_proceeding"],$_POST["name_proceeding"],$_POST["assr_proceeding"]
                            ,$_POST["y_proceeding"],$_POST["num_proceeding"],$_POST["date_proceeding"],$_POST["page_proceeding"],$_POST["location_proceeding"]
                        ,$_FILES["doc_proceeding"]["name"]);
                            } ?>

                            <form NAME="" METHOD="POST" ACTION="" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <input type="hidden" name="id" value="<?php echo "$idcard_per ";?>">

                                    <!-- text input -->
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>รอบการประเมิน</label>
                                            <select name='assr_proceeding' id='assr_proceeding' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="1"> รอบที่ 1 </option>
                                                <option value="2"> รอบที่ 2 </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ปีการประเมิน</label>
                                            <select name='assy_proceeding' id='assy_proceeding' class="form-control">
                                                <?php 
                                $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                                for($i=1;$i<=3;$i++)
                                  {
                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                  }
                               ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>การมีส่วนร่วม</label>
                                            <select name='parti_proceeding' id='parti_proceeding' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="1"> ผู้จัดทำหลัก </option>
                                                <option value="2"> ผู้จัดทำร่วม </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>สัดส่วนรับผิดชอบ</label>
                                            <select NAME="part_proceeding" id='part_proceeding' class="form-control">
                                                <?php 
                                  for($i=100;$i>=1;$i--)
                                    {
                                      echo "<option value='$i'>$i %</option>";
                                    }
                                 ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>ระดับการประชุม</label>
                                        <select name='level_proceeding' id='level_proceeding' class="form-control" >
                                <option value="0"> ---Select--- </option>
                                <option value="na"> รายงานการประชุมวิชาการระดับชาติ </option>
                                <option value="in"> รายงานการประชุมวิชาการระดับนานาชาติ </option>
                            </select> 
                                    </div>
                                    <div class="col">
                                        <label>ประเภทการเผยแพร่</label>
                                        <select name='type2_proceeding' id='type2_proceeding' class='form-control' >
                              <option value='0'> ---SELECT---  </option>
                              <option value='1'> วารสารที่สภามหาวิทยาลัยอนุมัติ </option>
                              <option value='2'> การประชุมวิชาการระดับชาติ </option>
                              <option value='3'> การประชุมวิชาการระดับนานาชาติ </option>
                              <option value='4'> ฐานข้อมูล TCI กลุ่มที่ ๒ </option>
                              <option value='5'> ฐานข้อมูล TCI กลุ่มที่ ๑ </option>
                              <option value='6'> ฐานข้อมูลการจัด อันดับวารสาร SJR </option>
                            </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ชื่อผลงานอังกฤษ</label>
                                        <INPUT TYPE="text" class="form-control" NAME="worke_proceeding">   
                                    </div>
                                    <div class="col">
                                        <label>ชื่อผลงานไทย</label>
                                        <INPUT TYPE="text" class="form-control" NAME="workt_proceeding">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ชื่อการประชุม</label>
                                        <INPUT TYPE="text" class="form-control" NAME="name_proceeding">
                                    </div>
                                    <div class="col">
                                        <label>ปีพ.ศ.</label>
                                        <select name='y_proceeding' id='y_proceeding' class="form-control" >
                              <?php 
                                $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                                for($i=1;$i<=20;$i++)
                                  { echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';  }
                               ?>
                            </select>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ครั้งที่</label>
                                        <INPUT TYPE="text" class="form-control" NAME="num_proceeding">
                                    </div>
                                    <div class="col">
                                        <label>วันที่</label>
                                        <INPUT TYPE="date" class="form-control" NAME="date_proceeding" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>หน้าที่</label>
                                        <INPUT TYPE="text" class="form-control" NAME="page_proceeding">
                                    </div>
                                    <div class="col">
                                        <label>สถานที่</label>
                                        <INPUT TYPE="text" class="form-control" NAME="location_proceeding">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>หลักฐาน</label><FONT COLOR=red>(File formats doc,docx,pdf,jpg,jpeg)</FONT> </label>
                                        <INPUT TYPE="file" NAME="doc_proceeding" id="doc_proceeding" class="form-control" onchange="return fileValidation()">
                                    </div>
                                    
                                </div>


                                <br>

                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<script>
function fileValidation() {
    var fileInput = document.getElementById('doc_proceeding');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.png)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('รองรับแค่ไฟล์ .doc .docx .pdf .jpg .jpeg หรือ .png เท่านั้น.');
        fileInput.value = '';
        return false;
    }
}
</script>