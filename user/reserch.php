<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require "../inc/educationlist.php";
require "../backend/array.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
$getData_reserch = viewReserch($idcard_per);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>งานวิจัย</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="add_reserch.php">
                            <i class="fas fa-plus"></i> เพิ่มงานวิจัย
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <?php if(empty($getData_reserch)){?>

                <div class="col">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>กรุณาอัพเดทข้อมูล!</strong> งานวิจัย
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <?php }else{?>
                <?php $i=1; ?>
                <?php foreach ($getData_reserch as $data){ ?>
                <div class="col-md-3">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title "><?php echo $data["ym_perre"];?></h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                        <dt>ชื่องานวิจัย</dt>
                            <dd>&nbsp; &nbsp; <?php echo $data["namet_perre"];?></dd>
                            <dt>สถานภาพ</dt>
                            <dd>&nbsp; &nbsp; <?php echo $satre_perre[$data["satre_perre"]];?></dd>
                            <dt>สถานภาพผลงาน</dt>
                            <dd>&nbsp; &nbsp; <?php echo $satwo_perre[$data["satwo_perre"]];?></dd>
                        </div>
                        <!-- /.card-body -->
                        <a href="user_del.php?id_perre=<?php echo $data["id_perre"];?>&idcard_per=<?php echo $data["idcard_per"];?>&doc_perre=<?php echo $data["doc_perre"];?>" onclick="return confirm  ('คุณแน่ใจที่จะลบข้อมูล  <?php echo $data['namee_perre'];?> ออกจากระบบ  เนื่องจากการลบ <?php echo $data['namee_perre'];?> มีผลต่อระบบฐานข้อมูล  จะทำให้เกิดความเสียหายนะครับ?')" class="btn btn-danger btn-block" > Del</a>
                    </div>
                    <!-- /.card -->
                </div>
                <?php }?>
                <?php }?>
                <!-- /.col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>