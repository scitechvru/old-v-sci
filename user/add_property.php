<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>เพิ่มทรัพย์สินทางปัญญา</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มทรัพย์สินทางปัญญา</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if(isset($_POST["submit"])){
                            addProperty($_POST["id"],$_POST["assr_intel"],$_POST["assy_intel"],$_POST["parti_intel"],$_POST["part_intel"],
                            $_POST["type_intel"],$_POST["num_intel"],$_POST["country_intel"],$_POST["workt_intel"],$_POST["worke_intel"]
                            ,$_POST["datepre_intel"],$_FILES["doc_pre"]["name"],$_POST["dateapp_intel"],$_FILES["doc_app"]["name"]);
                            
                           } ?>

                            <form NAME="" METHOD="POST" ACTION="" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <input type="hidden" name="id" value="<?php echo "$idcard_per ";?>">

                                    <!-- text input -->
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>รอบการประเมิน</label>
                                            <select name='assr_intel' id='assr_intel' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="1"> รอบที่ 1 </option>
                                                <option value="2"> รอบที่ 2 </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ปีการประเมิน</label>
                                            <select name='assy_intel' id='assy_intel' class="form-control">
                                                <?php 
                                $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                                for($i=1;$i<=3;$i++)
                                  {
                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                  }
                               ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>การมีส่วนร่วม</label>
                                            <select name='parti_intel' id='parti_intel' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="1"> ผู้จัดทำหลัก </option>
                                                <option value="2"> ผู้จัดทำร่วม </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>สัดส่วนรับผิดชอบ</label>
                                            <select NAME="part_intel" id='part_intel' class="form-control">
                                                <?php 
                                  for($i=100;$i>=1;$i--)
                                    {
                                      echo "<option value='$i'>$i %</option>";
                                    }
                                 ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col">
                                        <label>ประเภท</label>
                                        <select name="type_intel" id="type_intel" class="form-control">
                                            <OPTION VALUE='0'> ----- เลือก ----- </OPTION>
                                            <OPTION VALUE='1'> สิทธิบัตร </OPTION>
                                            <OPTION VALUE='2'> อนุสิทธิบัตร </OPTION>
                                            <OPTION VALUE='3'> ลิขสิทธิ์ผลงาน </OPTION>
                                            <OPTION VALUE='4'> เครื่องหมายการค้า </OPTION>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label>เลขที่</label>
                                        <INPUT TYPE="text" NAME="num_intel" class="form-control">
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <label>ประเทศ</label>
                                        <select name="country_intel" id="country_intel" class="form-control">
                                            <?php 
                                include "../inc/countrylist.php";
                                for ($i=1;$i<=count($s);$i++)
                                {  echo "<OPTION VALUE='$i'> $s[$i] </OPTION>";  }
                              ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ชื่อผลงานไทย</label>
                                        <INPUT TYPE="text" NAME="workt_intel" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label>ชื่อผลงานอังกฤษ</label>
                                        <INPUT TYPE="text" NAME="worke_intel" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ยื่นเมื่อ</label>
                                        <INPUT TYPE="date" NAME="datepre_intel" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label>หลักฐาน</label>
                                        <INPUT TYPE="file" NAME="doc_pre" id="doc_pre" class="form-control" onchange="return fileValidation()">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ได้รับเมื่อ</label>
                                        <INPUT TYPE="date" NAME="dateapp_intel" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label>หลักฐาน</label>
                                        <INPUT TYPE="file" NAME="doc_app" id="doc_app" class="form-control" onchange="return fileValidation2()"> 
                                    </div>
                                </div>


                                <br>

                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<script>
function fileValidation() {
    var fileInput = document.getElementById('doc_pre');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.png)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('รองรับแค่ไฟล์ .doc .docx .pdf .jpg .jpeg หรือ .png เท่านั้น.');
        fileInput.value = '';
        return false;
    }
}
</script>

<script>
function fileValidation2() {
    var fileInput = document.getElementById('doc_app');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.png)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('รองรับแค่ไฟล์ .doc .docx .pdf .jpg .jpeg หรือ .png เท่านั้น.');
        fileInput.value = '';
        return false;
    }
}
</script>