<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>เพิ่มเอกสาร/ตำรา</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มเอกสาร/ตำรา</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if(isset($_POST["submit"])){
                            addDocs($_POST["id"],$_POST["assr_docs"],$_POST["assy_docs"],$_POST["part_docs"],$_POST["type_docs"],
                            $_POST["type2_docs"],$_POST["cowrite_docs"],$_POST["namesub_docs"],$_POST["name_docs"],$_POST["y_docs"]
                            ,$_POST["time_docs"],$_POST["num_docs"],$_POST["page_docs"],$_POST["city_docs"],$_POST["print_docs"],$_FILES["Docs_doc"]["name"]);
                            $data=addDocs();
                            if($data=='success'){
                            ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>เพิ่มข้อมูลเรียบร้อย</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php }else {?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>ไม่สามารถเพิ่มข้อมูลได้</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php }?>

                            <?php } ?>

                            <form NAME="" METHOD="POST" ACTION="" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <input type="hidden" name="id" value="<?php echo "$idcard_per ";?>">

                                    <!-- text input -->
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>รอบการประเมิน</label>
                                            <select name='assr_docs' id='assr_docs' class="form-control">
                                                <option value="0"> กรุณาระบุรอบการประเมิน</option>
                                                <option value="1"> รอบที่ 1 </option>
                                                <option value="2"> รอบที่ 2 </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ปีการประเมิน</label>
                                            <select name='assy_docs' id='assy_docs' class="form-control">
                                            <option value="">กรุณาระบุปีการประเมิน</option>
                                                <?php 
                                                    $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                                                    echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                                                    for($i=1;$i<=3;$i++)
                                                    {
                                                        echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                                    }
                                                ?>

                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>การมีส่วนร่วม</label>
                                            <select NAME="part_docs" id='part_docs' class="form-control">
                                            <option value="">กรุณาระบุการมีส่วนร่วม</option>
                                                <?php 
                                  for($i=100;$i>=1;$i--)
                                    {
                                      echo "<option value='$i'>$i %</option>";
                                    }
                                 ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ประเภท</label>
                                            <select name='type_docs' id='type_docs' class="form-control">
                                                <option value="0"> กรุณาระบุประเภท</option>
                                                <option value="1"> เอกสารประกอบการสอน </option>
                                                <option value="2"> เอกสารคำสอน</option>
                                                <option value="3"> ตำรา </option>
                                                <option value="4"> หนังสือ </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>ภาษา</label>
                                        <select name='type2_docs' id='type2_docs' class='form-control'>
                                            <option value='0'> กรุณาระบุภาษา</option>
                                            <option value='t'> ภาษาไทย </option>
                                            <option value='e'> ภาษาต่างประเทศ </option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label>ชื่อผู้แต่งร่วม</label>
                                        <INPUT TYPE="text" class="form-control" NAME="cowrite_docs">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ชื่อเอกสาร</label>
                                        <INPUT TYPE="text" class="form-control" NAME="namesub_docs">
                                    </div>
                                    <div class="col">
                                        <label>ชื่อเรื่อง (ถ้ามี)</label>
                                        <INPUT TYPE="text" class="form-control" NAME="name_docs">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ปีที่พิมพ์</label>
                                        <select name='y_docs' id='y_docs' class="form-control">
                                        <option value="">กรุณาระบุปีที่พิมพ์</option>
                                            <?php 
                                $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                                for($i=1;$i<=60;$i++)
                                  {
                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                  }
                               ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label>ครั้งที่พิมพ์</label>
                                        <INPUT TYPE="text" NAME="time_docs" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>เล่มที่</label>
                                        <INPUT TYPE="text" NAME="num_docs" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label>หน้าที่</label>
                                        <INPUT TYPE="text" NAME="page_docs" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>เมืองที่พิมพ์</label>
                                        <select name="city_docs" id="city_docs" width="30" class="form-control">
                              <OPTION VALUE='0'> ----- เลือก ----- </OPTION>
                              <?php 
                                include "../inc/provincelist.php";
                                for ($i=1;$i<=count($p);$i++)
                                  {
                                    echo "<OPTION VALUE='$i'> $p[$i] </OPTION>";
                                  }
                              ?>
                            </select>  
                                    </div>
                                    <div class="col">
                                        <label>สำนักพิมพ์</label>
                                        <INPUT TYPE="text" NAME="print_docs" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col">
                                    <label>หน้าปก <FONT COLOR=red>(File formats doc,docx,pdf,jpg,jpeg)</FONT> </label>
                                    <INPUT TYPE="file" NAME="Docs_doc" id="Docs_doc" class="form-control" onchange="return fileValidation()">
                                    </div>
                                </div>
                                
<br>

                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>

<script>
function fileValidation() {
    var fileInput = document.getElementById('Docs_doc');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.png)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('รองรับแค่ไฟล์ .doc .docx .pdf .jpg .jpeg หรือ .png เท่านั้น.');
        fileInput.value = '';
        return false;
    }
}
</script>


