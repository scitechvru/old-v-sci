<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>เพิ่มบทความ</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มบทความ</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if(isset($_POST["submit"])){
                            addArticle($_POST["id"],$_POST["assr_journal"],$_POST["assy_journal"],$_POST["parti_journal"],$_POST["part_journal"],
                            $_POST["level_journal"],$_POST["type_journal"],$_POST["nameta_journal"],$_POST["nameea_journal"],$_POST["name_journal"],$_POST["cowrite_journal"]
                            ,$_POST["type2_journal"],$_POST["year1_journal"],$_POST["year2_journal"],$_POST["edtion_journal"],$_POST["month_journal"],$_POST["page_journal"]
                            ,$_POST["city_journal"],$_POST["country_journal"],$_FILES["doc_journal"]["name"]);
                            $data=addArticle();
                            if($data=='success'){
                            ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>เพิ่มข้อมูลเรียบร้อย</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php }else {?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>ไม่สามารถเพิ่มข้อมูลได้</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php }?>

                            <?php } ?>

                            <form NAME="" METHOD="POST" ACTION="" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <input type="hidden" name="id" value="<?php echo "$idcard_per ";?>">

                                    <!-- text input -->
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>รอบการประเมิน</label>
                                            <select name='assr_journal' id='assr_journal' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="1"> รอบที่ 1 </option>
                                                <option value="2"> รอบที่ 2 </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ปีการประเมิน</label>
                                            <select name='assy_journal' id='assy_journal' class="form-control">
                                                <?php 
                                $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                                for($i=1;$i<=3;$i++)
                                  {
                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                  }
                               ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>การมีส่วนร่วม</label>
                                            <select name='parti_journal' id='parti_journal' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="1"> ผู้จัดทำหลัก </option>
                                                <option value="2"> ผู้จัดทำร่วม </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>สัดส่วนรับผิดชอบ</label>
                                            <select NAME="part_journal" id='part_journal' class="form-control">
                                                <?php 
                                  for($i=100;$i>=1;$i--)
                                    {
                                      echo "<option value='$i'>$i %</option>";
                                    }
                                 ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>ระดับวารสาร</label>
                                        <select name="level_journal" id="level_journal" class="form-control">
                                            <OPTION VALUE='0'> ----- เลือก ----- </OPTION>
                                            <OPTION VALUE='na'> วารสารระดับชาติ (National Journal) </OPTION>
                                            <OPTION VALUE='in'> วารสารระดับนานาชาติ (International Journal) </OPTION>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label>ประเภทบทความ</label>
                                        <select name="type_journal" id="type_journal" class="form-control">
                                            <OPTION VALUE='0'> ----- เลือก ----- </OPTION>
                                            <OPTION VALUE='1'> บทความวิชาการ </OPTION>
                                            <OPTION VALUE='2'> บทความวิจัย </OPTION>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ชื่อบทความ - ไทย </label>
                                        <INPUT TYPE="text" class="form-control" NAME="nameta_journal">
                                    </div>
                                    <div class="col">
                                        <label>ชื่อบทความ - อังกฤษ</label>
                                        <INPUT TYPE="text" class="form-control" NAME="nameea_journal">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ชื่อวารสาร</label>
                                        <INPUT TYPE="text" class="form-control" NAME="name_journal">
                                    </div>
                                    <div class="col">
                                        <label>ชื่อผู้แต่งร่วม</label>
                                        <INPUT TYPE="text" class="form-control" NAME="cowrite_journal">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ประเภทการเผยแพร่</label>
                                        <select name='type2_journal' id='type2_journal' class='form-control'>
                                            <option value='0'> ---SELECT--- </option>
                                            <option value='1'> วารสารที่สภามหาวิทยาลัยอนุมัติ </option>
                                            <option value='2'> การประชุมวิชาการระดับชาติ </option>
                                            <option value='3'> การประชุมวิชาการระดับนานาชาติ </option>
                                            <option value='4'> ฐานข้อมูล TCI กลุ่มที่ ๒ </option>
                                            <option value='5'> ฐานข้อมูล TCI กลุ่มที่ ๑ </option>
                                            <option value='6'> ฐานข้อมูลการจัด อันดับวารสาร SJR </option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label>ปีพ.ศ.</label>
                                        <select name='year1_journal' id='year1_journal' class="form-control">
                                            <?php 
                                $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                                for($i=1;$i<=20;$i++)
                                  {
                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                  }
                               ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ปีที่</label>
                                        <INPUT TYPE="text" class="form-control" NAME="year2_journal">
                                    </div>
                                    <div class="col">
                                        <label>ฉบับที่</label>
                                        <INPUT TYPE="text" class="form-control" NAME="edtion_journal">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>เดือน
                                        </label>
                                        <SELECT NAME="month_journal" id="month_journal" class="form-control">
                                            <OPTION VALUE="1" SELECTED> January
                                            <OPTION VALUE="2">Februry
                                            <OPTION VALUE="3">March
                                            <OPTION VALUE="4">April
                                            <OPTION VALUE="5">May
                                            <OPTION VALUE="6">June
                                            <OPTION VALUE="7">July
                                            <OPTION VALUE="8">August
                                            <OPTION VALUE="9">September
                                            <OPTION VALUE="10">October
                                            <OPTION VALUE="11">November
                                            <OPTION VALUE="12">Decembers
                                        </SELECT>
                                    </div>
                                    <div class="col">
                                        <label>หน้า
                                        </label>
                                        <INPUT TYPE="text" class="form-control" NAME="page_journal">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>เมื่องที่พิมพ์</label>
                                        <INPUT TYPE="text" class="form-control" NAME="city_journal">
                                    </div>
                                    <div class="col">
                                        <label>ประเทศที่ตีพิมพ์</label>
                                        <select name="country_journal" id="country_journal" class="form-control">
                               <?php 
                                  include "../inc/countrylist.php";
                                  for ($i=1;$i<=count($s);$i++)
                                    {
                                      echo "<OPTION VALUE='$i'> $s[$i] </OPTION>";
                                    }
                                ?>
                            </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>หลักฐาน <FONT COLOR=red>(File formats doc,docx,pdf,jpg,jpeg)</FONT></label>
                                        <INPUT TYPE="file" NAME="doc_journal" id="doc_journal" class="form-control" onchange="return fileValidation()"> 
                                    </div>
                                    
                                </div>

                                <br>

                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<script>
function fileValidation() {
    var fileInput = document.getElementById('doc_journal');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.png)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('รองรับแค่ไฟล์ .doc .docx .pdf .jpg .jpeg หรือ .png เท่านั้น.');
        fileInput.value = '';
        return false;
    }
}
</script>