<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
$type_per = $_SESSION[sess_admintype];
 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>ตำแหน่งทางวิชาการสายวิชาการ</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มตำแหน่งทางวิชาการสายวิชาการ</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if(isset($_POST["submit"])){
                            addAcp($_POST["id"],$_POST["type_per"],$_POST["yAsi_perjob"],$_POST["yAso_perjob"],$_POST["yPro_Perjob"],$_POST["Yaj"],
                            $_FILES["fileupload"]["name"]);
                            $data=addAcp();
                            } ?>

                            <form NAME="" METHOD="POST" ACTION="" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <input type="hidden" name="id" value="<?php echo "$idcard_per ";?>">
                                    <input type="hidden" name="type_per" value="<?php echo "$type_per";?>">
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ผู้ช่วยศาสตราจารย์</label>
                                            <select name="yAsi_perjob" id="yAsi_perjob" class="form-control" require>
                                                <?php
                                                $xYear=date('Y')+543; // ค.ศ. เป็น พ.ศ.
                                                if(!$yins_peraca){
                                                    echo"<OPTION VALUE='' SELECTED> -Select- </OPTION>";
                                                        echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // »Õ»Ñ¨¨ØºÑ¹
                                                        for($i=1;$i<=60;$i++)
                                                        {  echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';  }
                                                }else{
                                                    if(!$yasi_peraca){
                                                    echo '<option value="'.$yins_peraca.'">--'.$yins_peraca.'--</option>'; // »Õ»Ñ¨¨ØºÑ¹
                                                        for($i=$yins_peraca; $i<=$xYear;$i++){
                                                        echo '<option value="'.($i).'">'.($i).'</option>';
                                                        } // end for	
                                                    }else{
                                                    echo '<option value="'.$yasi_peraca.'">--'.$yasi_peraca.'--</option>'; // »Õ»Ñ¨¨ØºÑ¹
                                                        for($i=$yasi_peraca; $i<=$xYear;$i++){
                                                        echo '<option value="'.($i).'">'.($i).'</option>';
                                                        } // end for	
                                                    }
                                                }
                                        
                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>รองศาตราจารย์</label>
                                            <select name="yAso_perjob" id="yAso_perjob" class="form-control" >
                                            <?php
                                            $xYear=date('Y')+543; // ค.ศ. เป็น พ.ศ.
                                            if(!$yasi_peraca){
                                                echo"<OPTION VALUE='' SELECTED> -Select- </OPTION>";
                                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // »Õ»Ñ¨¨ØºÑ¹
                                                for($i=1;$i<=60;$i++){
                                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                                }
                                            }else{
                                                if(!$yaso_peraca){
                                                    echo '<option value="'.$yasi_peraca.'">--'.$yasi_peraca.'--</option>'; // »Õ»Ñ¨¨ØºÑ¹
                                                    for($i=$yasi_peraca; $i<=$xYear;$i++){
                                                        echo '<option value="'.($i).'">'.($i).'</option>';
                                                    } // end for	
                                                }else{
                                                    echo '<option value="'.$yaso_peraca.'">--'.$yaso_peraca.'--</option>'; // »Õ»Ñ¨¨ØºÑ¹
                                                    for($i=$yaso_peraca; $i<=$xYear;$i++){
                                                        echo '<option value="'.($i).'">'.($i).'</option>';
                                                    } // end for	
                                                }	
                                            }// end if 
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ศาสตราจารย์</label>
                                            <select name="yPro_Perjob" id="yPro_Perjob" class="form-control" >
                                            <?php
                                            $xYear=date('Y')+543; // ค.ศ. เป็น พ.ศ.
                                            if(!$yaso_peraca){
                                                echo"<OPTION VALUE='' SELECTED> -Select- </OPTION>";
                                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // »Õ»Ñ¨¨ØºÑ¹
                                                for($i=1;$i<=60;$i++){
                                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                                }
                                            }else{
                                                if(!$ypro_peraca){
                                                echo '<option value="'.$yaso_peraca.'">--'.$yaso_peraca.'--</option>'; // »Õ»Ñ¨¨ØºÑ¹
                                                    for($i=$yaso_peraca; $i<=$xYear;$i++){
                                                        echo '<option value="'.($i).'">'.($i).'</option>';
                                                    } // end for	
                                                }else{
                                                    echo '<option value="'.$ypro_peraca.'">--'.$ypro_peraca.'--</option>'; // »Õ»Ñ¨¨ØºÑ¹
                                                    for($i=$ypro_peraca; $i<=$xYear;$i++){
                                                        echo '<option value="'.($i).'">'.($i).'</option>';
                                                    } // end for	
                                                }			
                                            }// end if 
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                    

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>อาจารย์</label>
                                            <select name='Yaj' id='Yaj' class="form-control">
                                                <?php 
                                                $xYear=date('Y'); 
                                                echo"<OPTION VALUE='' SELECTED> -Select- </OPTION>";
                                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; 
                                                for($i=1;$i<=60;$i++){
                                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>หลักฐาน (Transcript)</label>
                                            <input type="file" name="fileupload" id="fileupload" class="form-control"  onchange="return fileValidation()">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<script>
        function fileValidation() {
            var fileInput = document.getElementById('namefile');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.doc|\.docx|\.pdf|\.png|\.jpg|\.jpeg)$/i;
            if (!allowedExtensions.exec(filePath)) {
                alert('รองรับแค่ไฟล์นามสกุล .pdf .doc .docx .png .jpg .jpeng เท่านั้น.');
                fileInput.value = '';
                return false;

            }
        }
</script>