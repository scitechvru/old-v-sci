<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>ประวัติการศึกษา</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มประวัติการศึกษา</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if(isset($_POST["submit"])){
                            addEdu($_POST["id"],$_POST["LevelEdu"],$_POST["AbbDeedu"],$_POST["MajorEdu"],$_POST["UniEdu"],
                            $_POST["Other_Edu"],$_POST["OtJob_Perjob"],$_POST["CountryEdu"],$_POST["YEdu"],$_FILES["fileupload"]["name"]);
                            $data=addEdu();
                            } ?>

                            <form NAME="" METHOD="POST" ACTION="" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <input type="hidden" name="id" value="<?php echo "$idcard_per ";?>">
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ระดับการศึกษา</label>
                                            <select name="LevelEdu" id="LevelEdu" class="form-control" require>
                                                <OPTION VALUE='0'> --- Select --- </OPTION>
                                                <OPTION VALUE='1'> ปริญญาตรี </OPTION>
                                                <OPTION VALUE='2'> ปริญญาโท </OPTION>
                                                <OPTION VALUE='3'> ปริญญาเอก </OPTION>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>อักษรย่อปริญญา</label>
                                            <input type="text" name="AbbDeedu" id="AbbDeedu" class="form-control"require>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>สาขาวิชา</label>
                                            <input type="text" name="MajorEdu" id="MajorEdu" class="form-control"require>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>สถานศึกษา</label>
                                            <select name="UniEdu" id="UniEdu" class="form-control"require>
                                                <OPTION VALUE='0'> ----- Select ----- </OPTION>
                                                <?php
                                                include "../inc/educationlist.php";
                                                for ($i=1;$i<=count($p);$i++)
                                                {
                                                echo "<OPTION VALUE='$i'> $p[$i] </OPTION>";
                                                }
                                            ?>
                                            </select>
                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                    $(function() {
                                        $("#Other_Edu").click(function() { // เมื่อคลิกที่ checkbox id=i_accept
                                            if ($(this).attr("checked") == "checked") { // ถ้าเลือก
                                                $("#OtJob_Perjob").attr("disabled",
                                                    "disabled"); // ให้ปุ่ม id=continue_bt ไม่ทำงาน
                                                $("#UniEdu").removeAttr(
                                                    "disabled"
                                                ); // ให้ปุ่ม id=continue_bt ทำงาน สามารถคลิกได้

                                            } else { // ยกเลิกไม่ทำการเลือก
                                                $("#OtJob_Perjob").removeAttr(
                                                    "disabled"
                                                ); // ให้ปุ่ม id=continue_bt ทำงาน สามารถคลิกได้
                                                $("#UniEdu").attr("disabled",
                                                    "disabled"); // ให้ปุ่ม id=continue_bt ไม่ทำงาน
                                            }
                                        });
                                    });
                                    </script>

                                    <div class="col">
                                        <div class="form-group">
                                            <label><INPUT TYPE="checkbox" NAME="Other_Edu" id="Other_Edu"
                                                    VALUE="1">ต่างประเทศ</label>
                                            <INPUT TYPE="text" NAME="OtJob_Perjob" id="OtJob_Perjob"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>ประเทศ</label>
                                            <select name="CountryEdu" id="CountryEdu" class="form-control"
                                                onchange="sSelect()">
                                                <OPTION VALUE='0'> ----- Select ----- </OPTION>
                                                <?php
                                                    include "../inc/countrylist.php";
                                                    for ($i=1;$i<=count($s);$i++)
                                                    {
                                                    echo "<OPTION VALUE='$i'> $s[$i] </OPTION>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>ปีที่สำเร็จ (ค.ศ.)</label>
                                            <select name='YEdu' id='YEdu' class="form-control">
                                                <?php 
                                                $xYear=date('Y'); 
                                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; 
                                                for($i=1;$i<=60;$i++){
                                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>หลักฐาน (Transcript)</label>
                                            <input type="file" name="fileupload" id="fileupload" class="form-control"  onchange="return fileValidation()">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<script>
        function fileValidation() {
            var fileInput = document.getElementById('namefile');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.doc|\.docx|\.pdf|\.xls|\.xlsx|\.png|\.jpg|\.jpeg)$/i;
            if (!allowedExtensions.exec(filePath)) {
                alert('รองรับแค่ไฟล์นามสกุล .pdf .doc .docx .xls .xlsx เท่านั้น.');
                fileInput.value = '';
                return false;

            }
        }
</script>