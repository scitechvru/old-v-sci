<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
$sql="select * from tb_per where idcard_per='$idcard_per' ";        
    $result=mysql_db_query($dbname,$sql);   
    $r=mysql_fetch_array($result);
    $id_per=$r[id_per];
    $fname_per=$r[fnamet_per];
    $lname_per=$r[lnamet_per];
    $idcard=$r[idcard_per];
    $tname_per=$r[tname_per];
    $fnamet_per=$r[fnamet_per];
    $lnamet_per=$r[lnamet_per];
    $fnamee_per=$r[fnamee_per];
    $lnamee_per=$r[lnamee_per];  
    $add_per=$r[add_per];  
    $bdate_per=$r[bdate_per];
    $prov_per=$r[prov_per];
    $zcode_per=$r[zcode_per];
    $phone_per=$r[phone_per];
    $email_per=$r[email_per];
    $img_per=$r[img_per];  

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>ข้อมูลส่วนตัว</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">แก้ไขข้อมูลส่วนตัว</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                        <?php
                        
                        if(isset($_POST["submit"])){
                            updateUser($_POST["id_per"],$_POST["NameTitle"],$_POST["NameFirst"],$_POST["NameLast"],$_FILES["fileupload"]["name"],$_POST["NameEFirst"],
                            $_POST["NameELast"],$_POST["idcard_per"],$_POST["bdate_per"],$_POST["TelMobile"],$_POST["EmailRes"],$_POST["AddRes"],
                            $_POST["AddrProvince"],$_POST["AddrPostalCode"]);
                            } ?>

                            <form  METHOD="POST" ACTION="" ENCTYPE="multipart/form-data">
                                <div class="row">
                                    <input type="hidden" name="id_per" value="<?php echo "$id_per";?>">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">โปรไฟล์</label>
                                            <img src="../backend/img_resize/<?php echo $_SESSION['sess_adminimg']; ?>"
                                                style="height: 120px; width: 100px;" class="form-control rounded-top"
                                                alt="<?php echo $_SESSION['sess_adminfnamet']; ?> <?php echo $_SESSION['sess_adminlnamet']; ?>">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>คำนำหน้า</label>
                                            <select name="NameTitle" id="NameTitle" class="form-control">

                                                <?php 
                                                    echo "<OPTION VALUE='$tname_per'> ----- $tname_per ----- </OPTION>";
                                                ?>
                                                <OPTION VALUE='0'> ----- Select ----- </OPTION>
                                                <OPTION VALUE='นางสาว'>นางสาว (Ms.)</OPTION>
                                                <OPTION VALUE='นาง'>นาง (Mrs.)</OPTION>
                                                <OPTION VALUE='นาย'>นาย (Mr.)</OPTION>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อ - ไทย</label>
                                            <input name="NameFirst" type="text" class="form-control"
                                                value="<?php echo "$fnamet_per";?>">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>นามสกุล - ไทย</label>
                                            <input name="NameLast" type="text" class="form-control"
                                                value="<?php echo "$lnamet_per";?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">อัพรูปโปรไฟล์</label>
                                            <input type="file" name="fileupload" id="fileupload"class="form-control" accept="image/x-png,image/gif,image/jpeg" disabled>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อ - อังกฤษ</label>
                                            <input name="NameEFirst" type="text" class="form-control"
                                                value="<?php echo "$fnamee_per";?>">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>นามสกุล - อังกฤษ</label>
                                            <input name="NameELast" type="text" class="form-control"
                                                value="<?php echo "$lnamee_per";?>">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>หมายเลขบัตรประจำตัวประชาชน</label>
                                            <input type="text" class="form-control" name="idcard_per" value="<?php echo "$idcard_per";?>"
                                                disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <div class="form-group">
                                            <label>วันเดือนปีเกิด</label>
                                            <input type="date" class="form-control" name="bdate_per"  value="<?php echo "$bdate_per";?>">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>เบอร์โทร</label>
                                            <input type="text" name="TelMobile" 
                                                class="form-control" value="<?php echo "$phone_per";?>">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>E-mail</label>
                                            <input name="EmailRes" type="email" class="form-control"
                                                value="<?php echo "$email_per";?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>ที่อยู่</label>
                                            <textarea name="AddRes" id="AddRes"
                                                class="form-control"><?php echo "$add_per";?></textarea>
                                        </div>

                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>จังหวัด</label>
                                            <select name="AddrProvince" class="form-control">
                                                <?php 
                                                include "../inc/provincelist.php";
                                                echo"<OPTION VALUE='$prov_per'>  $p[$prov_per] </OPTION>";
                                                for ($i=1;$i<=count($p);$i++)
                                                    {
                                                    echo "<OPTION VALUE='$i'> $p[$i] </OPTION>";
                                                    }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>รหัสไปรษณีย์</label>
                                            <input name="AddrPostalCode" type="text" class="form-control"
                                                VALUE="<?php echo"$zcode_per";?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>

<?php
 if($_GET['update']=='success'){
    echo '<script type="text/javascript">
    Swal.fire("อัพเดทสถานะสำเร็จ","","success");
    </script>';
    }
    elseif ($_GET['update']=='unsuccess'){
        echo '<script type="text/javascript">
        swal.fire("ไม่สามารถอัพเดทสถานะได้", "", "error");
        </script>';
    }
    ?>