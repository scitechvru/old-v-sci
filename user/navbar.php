<?php
include "chksession_admin.php";
require_once "../inc/function.php";

session_start();



$user = getUser($_SESSION["sess_adminid"]);
if (isset($_GET['logOut'])) {
  logOut();
}

 $sql ="SELECT * FROM tb_ex WHERE id_per= '$_SESSION[sess_adminidcard_per]' order by position_ex DESC ";
    $result = mysql_query($sql,$con);
    $num = mysql_num_rows($result);
    if ($num >= 0) {     
        $r = mysql_fetch_array($result);
        $position_ex = $r[position_ex];
        $major_ex = $r[major_ex];
    }

    mysql_close();
 
?>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>

            </ul>

        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index.php" class="brand-link">
                <img src="../backend/dist/img/logo/logosci.png" alt="SciTech" class="brand-image img-circle elevation-3"
                    style="opacity: .8">
                <span class="brand-text font-weight-light">SciTech VRU</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <?php if(isset($_SESSION['sess_adminid'])) { ?>
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="../backend/img_resize/<?php echo $_SESSION['sess_adminimg']; ?>" class="rounded-top"
                            alt="<?php echo $_SESSION['sess_adminfnamet']; ?> <?php echo $_SESSION['sess_adminlnamet']; ?>">
                    </div>
                    <div class="info">
                        <a class="d-block"><?php echo $_SESSION['sess_adminfnamet']; ?>
                            <?php echo $_SESSION['sess_adminlnamet']; ?></a>
                    </div>  
                    <?php } ?>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-address-card"></i>
                                <p>
                                    ข้อมูลส่วนตัว
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="profile.php" class="nav-link active">
                                        <i class="fas fa-user-edit nav-icon"></i>

                                        <p>ข้อมูลส่วนตัว</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="education.php" class="nav-link active">
                                        <i class="fas fa-user-graduate nav-icon"></i>

                                        <p>ประวัติการศึกษา</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="work.php" class="nav-link active">
                                        <i class="fas fa-user-tie nav-icon"></i>
                                        <p>ประวัติการทำงาน</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="position_ac.php" class="nav-link active">
                                        <i class="fas fa-user-tie nav-icon"></i>
                                        <p>ตำแหน่งทางวิชาการ</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php if(($position_ex < 3 and $position_ex != 0) or ( $position_ex >= 5 and $position_ex <= 8)){ ?>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fas fa-book"></i>
                                <p>
                                    ประเมินสมรรถนะ 
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                               <!--  <li class="nav-item">
                                    <a href="performance_ass.php?level=<?php echo $position_ex ?>&major=<?php echo $major_ex ?>" class="nav-link ">
                                        <i class="fas fa-book-open nav-icon"></i>
                                        <p>ประเมิน </p>
                                    </a>
                                </li> -->
                            </ul>
                        </li>
                        <?php  }  ?>
                        <li class="nav-item has-treeview ">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fas fa-book"></i>
                                <p>
                                    ผลงานทางวิชาการ
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="doc.php" class="nav-link ">
                                        <i class="fas fa-book-open nav-icon"></i>
                                        <p>เอกสาร/ตำรา</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="article.php" class="nav-link ">
                                        <i class="fas fa-seedling nav-icon"></i>
                                        <p>บทความ</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="reserch.php" class="nav-link ">
                                        <i class="fas fa-flask nav-icon"></i>

                                        <p>งานวิจัย</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="confer.php" class="nav-link ">
                                        <i class="fas fa-users nav-icon"></i>
                                        <p>งานประชุมวิชาการ</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="online.php" class="nav-link ">
                                        <i class="fas fa-desktop nav-icon"></i>
                                        <p>ออนไลน์</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="property.php" class="nav-link ">
                                        <i class="far fa-copyright nav-icon"></i>
                                        <p>ทรัพย์สินทางปัญญา</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="?logOut=true" class="nav-link">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>ออกจากระบบ
                                    <span class="right badge badge-danger">ผู้ใช้งาน</span>
                                </p>
                            </a>
                        </li>

                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>