<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require "../inc/educationlist.php";
require "../backend/array.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
$getData_confer = viewConfer($idcard_per);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>งานประชุมวิชาการ</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="add_confer.php">
                            <i class="fas fa-plus"></i> เพิ่มงานประชุมวิชาการ
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <?php if(empty($getData_confer)){?>

                <div class="col">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>กรุณาอัพเดทข้อมูล!</strong> งานประชุมวิชาการ
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <?php }else{?>
                <?php $i=1; ?>
                <?php foreach ($getData_confer as $data){ ?>
                <div class="col-md-3">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title "><?php echo $data["y_perpros"];?>-<?php echo $level_perpros[$data["level_perpros"]];?></h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                        <dt>ชื่องานประชุม</dt>
                            <dd>&nbsp; &nbsp; <?php echo $data["name_perpros"];?></dd>
                            <dt>ชื่อผลงานประชุม</dt>
                            <dd> &nbsp; &nbsp;<?php echo $data["workt_perpros"];?></dd>
                            <dt>วันที่</dt>
                            <dd> &nbsp; &nbsp;<?php echo $data["date_perpros"];?></dd>
                        </div>
                        <!-- /.card-body -->
                        <a href="user_del.php?id_perpros=<?php echo $data["id_perpros"];?>&idcard_per=<?php echo $data["idcard_per"];?>&doc_perpros=<?php echo $data["doc_perpros"];?>" onclick="return confirm ('คุณแน่ใจที่จะลบข้อมูล <?php echo $data['name_perpros'];?> ออกจากระบบ  เนื่องจากการลบ <?php echo $data['name_perpros'];?> มีผลต่อระบบฐานข้อมูล  จะทำให้เกิดความเสียหายนะครับ?')" class="btn btn-danger btn-block" > ลบ</a>
                    </div>
                    <!-- /.card -->
                </div>
                <?php }?>
                <?php }?>
                <!-- /.col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>