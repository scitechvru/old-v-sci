<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>เพิ่มออนไลน์</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มออนไลน์</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if(isset($_POST["submit"])){
                            addOnline($_POST["id"],$_POST["assr_online"],$_POST["assy_online"],$_POST["parti_online"],$_POST["part_online"],
                            $_POST["type_online"],$_POST["workt_online"],$_POST["worke_online"],$_POST["media_online"],$_POST["Other_online"]
                            ,$_POST["mediaN_online"],$_POST["location_online"],$_POST["date_online"],$_POST["time_online1"],$_POST["time_online2"]
                            ,$_POST["num_online"],$_FILES["doc_online"]["name"]);
                            } ?>

                            <form NAME="" METHOD="POST" ACTION="" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <input type="hidden" name="id" value="<?php echo "$idcard_per ";?>">

                                    <!-- text input -->
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>รอบการประเมิน</label>
                                            <select name='assr_online' id='assr_online' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="1"> รอบที่ 1 </option>
                                                <option value="2"> รอบที่ 2 </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ปีการประเมิน</label>
                                            <select name='assy_online' id='assy_online' class="form-control">
                                                <?php 
                                $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                                for($i=1;$i<=3;$i++)
                                  {
                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                  }
                               ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>การมีส่วนร่วม</label>
                                            <select name='parti_online' id='parti_online' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="1"> ผู้จัดทำหลัก </option>
                                                <option value="2"> ผู้จัดทำร่วม </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>สัดส่วนรับผิดชอบ</label>
                                            <select NAME="part_online" id='part_online' class="form-control">
                                                <?php 
                                  for($i=100;$i>=1;$i--)
                                    {
                                      echo "<option value='$i'>$i %</option>";
                                    }
                                 ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>ระดับการเผยแพร่</label>
                                        <select name='type_online' id='type_online' class='form-control'>
                                            <option value='0'> ---SELECT--- </option>
                                            <option value='1'> ผ่านสื่ออิเล็กทรอนิกส์หรือออนไลน์ </option>
                                            <option value='2'> ระดับสถาบัน </option>
                                            <option value='3'> ระดับชาติ </option>
                                            <option value='4'> ระดับความร่วมมือระหว่างประเทศ </option>
                                            <option value='5'> ระดับภูมิภาคอาเซียนหรือระดับนานาชาติ </option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label>ชื่อผลงานไทย</label>
                                        <INPUT TYPE="text" class="form-control" NAME="workt_online">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ชื่อผลงานอังกฤษ</label>
                                        <INPUT TYPE="text" class="form-control" NAME="worke_online">
                                    </div>
                                    <div class="col">
                                        <label>ช่องที่นำเสนอ</label>
                                        <SELECT NAME="media_online" id="media_online" class="form-control">
                              <OPTION VALUE="" SELECTED>-Select-
                              <OPTION VALUE="1">วิทยุ
                              <OPTION VALUE="2">โทรทัศน์
                              <OPTION VALUE="3">Website
                            </SELECT> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label><INPUT TYPE="checkbox" NAME="Other_online" id="Other_online" VALUE="1">แหล่งอื่น ๆ</label>
                                        <INPUT TYPE="text" class="form-control" NAME="mediaN_online" id="mediaN_online" >  
                                    </div>
                                    <div class="col">
                                        <label>สถานที่นำเสนอ</label>
                                        <INPUT TYPE="text" class="form-control" NAME="location_online">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>วันที่นำเสนอ</label>
                                        <INPUT TYPE="date" class="form-control" NAME="date_online">
                                    </div>
                                    <div class="col">
                                        <label>เวลาที่นำเสนอ</label>
                                        <INPUT TYPE="time" class="form-control" NAME="time_online1">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>เวลาที่สิ้นสุดการนำเสนอ</label>
                                        <INPUT TYPE="time" class="form-control" NAME="time_online2">  
                                    </div>
                                    <div class="col">
                                        <label>สำนักพิมพ์</label>
                                        <INPUT TYPE="text" NAME="print_docs" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>จำนวนครั้งที่นำเสนอ
                                        </label>
                                        <INPUT TYPE="text" class="form-control" NAME="num_online">
                                    </div>
                                    <div class="col">
                                        <label>หลักฐาน</label><FONT COLOR=red>(File formats doc,docx,pdf,jpg,jpeg)</FONT>      
                                        </label>
                                        <INPUT TYPE="file" NAME="doc_online" id="doc_online"  class="form-control" onchange="return fileValidation()">
                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<script>
function fileValidation() {
    var fileInput = document.getElementById('doc_online');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.png)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('รองรับแค่ไฟล์ .doc .docx .pdf .jpg .jpeg หรือ .png เท่านั้น.');
        fileInput.value = '';
        return false;
    }
}
</script>