<?php
session_start();
$sess_adminid = $_SESSION[sess_adminid];
$sess_adminuser = $_SESSION[sess_adminuser];
$sess_major = $_SESSION[sess_adminmajor];
$fnamet_per = $_SESSION[sess_adminfnamet];
$lnamet_per = $_SESSION[sess_adminlnamet];
$sess_type = $_SESSION[sess_admintype];

if ($sess_adminid != session_id() or $sess_adminuser == "") {
    header("Location: login.php");
    exit();
}
