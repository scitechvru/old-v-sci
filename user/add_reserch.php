<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
 
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>เพิ่มงานวิจัย</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>-->

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มงานวิจัย</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php if(isset($_POST["submit"])){
                            addReserch($_POST["id"],$_POST["assr_res"],$_POST["assy_res"],$_POST["division_res"],$_POST["part_res"],
                            $_POST["namet_res"],$_POST["namee_res"],$_POST["money_res"],$_POST["money2_res"],$_POST["y_res"]
                            ,$_POST["status_res"],$_POST["s_res"],$_POST["ys_res"],$_FILES["doc_app"]["name"]);
                           
                           } ?>

                            <form NAME="" METHOD="POST" ACTION="" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <input type="hidden" name="id" value="<?php echo "$idcard_per ";?>">

                                    <!-- text input -->
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>รอบการประเมิน</label>
                                            <select name='assr_res' id='assr_res' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="1"> รอบที่ 1 </option>
                                                <option value="2"> รอบที่ 2 </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ปีการประเมิน</label>
                                            <select name='assy_res' id='assy_res' class="form-control">
                                                <?php 
                                $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                                echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                                for($i=1;$i<=3;$i++)
                                  {
                                    echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                  }
                               ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>การมีส่วนร่วม</label>
                                            <select name='division_res' id='division_res' class="form-control">
                                                <option value="0"> ---Select--- </option>
                                                <option value="sc"> วิทยาศาสตร์ </option>
                                                <option value="so"> สังคมศาสตร์ </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>สายงานวิจัย</label>
                                            <select NAME="part_journal" id='part_journal' class="form-control">
                                                <?php 
                                  for($i=100;$i>=1;$i--)
                                    {
                                      echo "<option value='$i'>$i %</option>";
                                    }
                                 ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>สัดส่วนรับผิดชอบ</label>
                                        <select NAME="part_res" id='part_res' class="form-control">
                                            <?php 
                                  for($i=100;$i>=1;$i--)
                                    {
                                      echo "<option value='$i'>$i %</option>";
                                    }
                                 ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label>ชื่องานวิจัย - ไทย</label>
                                        <INPUT TYPE="text" class="form-control" NAME="namet_res">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>ชื่องานวิจัย - อังกฤษ</label>
                                        <INPUT TYPE="text" class="form-control" NAME="namee_res">
                                    </div>
                                    <div class="col">
                                        <label>แหล่งทุน</label>
                                        <select name='money_res' id='money_res' class="form-control">
                                            <option value="0"> ---Select--- </option>
                                            <option value="IN"> แหล่งทุนภายใน </option>
                                            <option value="OUT"> แหล่งทุนภายนอก </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>งบประมาณ</label>
                                        <input type="text" name="money2_res" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label>ปีงบประมาณ</label>
                                        <select name='y_res' id='y_res' class="form-control" >
                            <?php 
                              $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                              echo '<option value="'.$xYear.'">'.$xYear.'</option>'; // ปีปัจจุบัน
                              for($i=1;$i<=3;$i++)
                                {
                                  echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';
                                }
                             ?>
                          </select> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>สถานภาพ</label>
                                        <select name='status_res' id='status_res' class="form-control" >
                            <option value="0"> ---Select--- </option>
                            <option value="L"> หัวหน้าโครงการ </option>
                            <option value="F"> ผู้ร่วมวิจัย </option>
                          </select> 
                                    </div>
                                    <div class="col">
                                        <label>สถานภาพผลงาน</label>
                                        <select name='s_res' id='s_res' class="form-control" >
                            <option value="0"> ---Select--- </option>
                            <option value="p"> กำลังดำเนินการ </option>
                            <option value="f"> สำเร็จ </option>
                          </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label><input type="checkbox" name="ys_res1" value='1'>  ปีที่สำเร็จ</label>
                                        <select name='ys_res' id='ys_res' class="form-control">
                            <?php 
                              $xYear=date('Y')+543; // เก็บค่าปีปัจจุบันไว้ในตัวแปร
                              echo '<option value="0">'.'---Select---'.'</option>'; // ปีปัจจุบัน
                              for($i=1;$i<=60;$i++)
                              { echo '<option value="'.($xYear-$i).'">'.($xYear-$i).'</option>';      }
                            ?> 
                          </select>
                                    </div>
                                    <div class="col">
                                        <label>หลักฐาน</label><FONT COLOR=red>(File formats doc,docx,pdf,jpg,jpeg)</FONT></label>
                                        <INPUT TYPE="file" NAME="doc_app" id="doc_app" class="form-control" onchange="return fileValidation()">
                                    </div>
                                </div>

                               

                                <br>

                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" name="submit"
                                            class="btn btn-primary btn-block">บันทึกข้อมูล</button>
                                    </div>

                                    <!-- /.col -->
                                </div>

                            </form>
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<script>
function fileValidation() {
    var fileInput = document.getElementById('doc_app');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.png)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('รองรับแค่ไฟล์ .doc .docx .pdf .jpg .jpeg หรือ .png เท่านั้น.');
        fileInput.value = '';
        return false;
    }
}
</script>