<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require "../inc/educationlist.php";
require "../backend/array.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
$getData_wrk = viewWork($idcard_per);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>ประวัติการทำงาน</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="wrk1.php">
                            <i class="fas fa-plus"></i> เพิ่มประวัติการทำงาน
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <?php if(empty($getData_wrk)){?>

                <div class="col">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>กรุณาอัพเดทข้อมูล!</strong> ประวัติการทำงาน
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <?php }else{?>
                <?php $i=1; ?>
                <?php foreach ($getData_wrk as $data){ ?>
                <div class="col-md-3">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title "><?php echo $data["ys_perwo"];?>-<?php echo $data["ye_perwo"];?></h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?php echo $data["or_perwo"];?><br>
                            <?php echo $data["job_perwo"];?>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <?php }?>
                <?php }?>
                <!-- /.col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<?php
 if($_GET['update']=='success'){
    echo '<script type="text/javascript">
    Swal.fire("เพิ่มข้อมูลสำเร็จ","","success");
    </script>';
    }
    elseif ($_GET['update']=='unsuccess'){
        echo '<script type="text/javascript">
        swal.fire("ไม่สามารถเพิ่มข้อมูลได้","", "error");
        </script>';
    }
    ?>