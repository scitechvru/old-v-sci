<!DOCTYPE html>
<html>
<head>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap.css">


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-3.2.1.slim.min.js"></script>   
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.js"></script>
<script language="javascript" src="js/jquery-3.2.1.slim.min.js"></script>

<style>
	.nav ul {
	background-color:#777;
	}
	.nav li a {
		color:#000;
		}
    .dropdown:hover .dropdown-menu {
        display: block;
         margin-top: 0;
      }

</style>

</head>
<body>
<!-- start menu --> 
 <div class="container-fluid" style="background-image:url(images/body-bg7.png)"> 
 <div class="container"> 
  <nav class="navbar navbar-expand-lg navbar-light bg-warning">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav">
        <a class="nav-link" href="index.html">HOME</a>

      <li >
        <a class="nav-link" href="">HOME</a>
      </li>

</ul>
<!--
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font color="#000000">
          ����ǡѺ���</font>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="history.html">����ѵ���з����</a>
		  <a class="dropdown-item" href="video.html">�մ����йӤ��</a>
          <a class="dropdown-item" href="philosophy.html">��Ѫ�� ����·�ȹ� �ѹ��Ԩ</a>
          <a class="dropdown-item" href="values.html">��ҹ�����ѡ</a>
          <a class="dropdown-item" href="uploads/plan-2561.pdf">Ἱ��Ժѵԡ�ä���Է����ʵ�����෤����� �� 2561</a>
		  <a class="dropdown-item" href="uploads/plan-2562.pdf">Ἱ��Ժѵԡ�ä���Է����ʵ�����෤����� �� 2562</a>
          <a class="dropdown-item" href="uploads/strategy-60.pdf">Ἱ���ط�줳��Է����ʵ�����෤����� �� 2561</a>
		  <a class="dropdown-item" href="uploads/strategy-62.pdf">Ἱ���ط�줳��Է����ʵ�����෤����� �� 2562</a>
		  <a class="dropdown-item" href="*">��§ҹ��áӡѺ�Դ�����ô��Թ�ҹ���Ἱ��Ժѵ��Ҫ���</a>
        </div>
      </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font color="#000000">
          ��ѡ�ٵ�</font>
        </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://comit.vru.ac.th/">��ѡ�ٵ�෤��������ʹ��</a>
          <a class="dropdown-item" href="http://cs.vru.ac.th/">��ѡ�ٵ��Է�ҡ�ä���������</a>
          <a class="dropdown-item" href="https://biotechvrublog.wordpress.com/">��ѡ�ٵ�෤����ժ���Ҿ</a>
          <a class="dropdown-item" href="https://vrumathematics.wordpress.com/">��ѡ�ٵä�Ե��ʵ�����ء��</a>
          <a class="dropdown-item" href="https://homeeconomics1000.wordpress.com/">��ѡ�ٵäˡ�����ʵ��</a>
          <a class="dropdown-item" href="https://chemistryvru.wordpress.com/">��ѡ�ٵ����</a>
          <a class="dropdown-item" href="https://nutrition2559.wordpress.com/">��ѡ�ٵ�����ҡ����С�á�˹������</a>
          <a class="dropdown-item" href="https://environmental2016science.wordpress.com/">��ѡ�ٵ��Է����ʵ������Ǵ����</a>
          <a class="dropdown-item" href="https://occsafety2016.wordpress.com/">��ѡ�ٵ��Ҫ��͹������Ф�����ʹ���</a>
          <a class="dropdown-item" href="#">��ѡ�ٵá�èѴ�����¾Ժѵ���к�����Ҹ�ó����</a>
          <a class="dropdown-item" href="#">��ѡ�ٵÿ��ԡ�����ء��</a>
          <a class="dropdown-item" href="http://dise.vru.ac.th/index.html#">��ѡ�ٵù�ѵ�����ԨԷ��������ǡ����Ϳ������</a>
		  <a class="dropdown-item" href="http://sci.vru.ac.th/uploads">��ѡ�ٵù�ѵ����������������ͧ���������آ�Ҿ</a>
		  <a class="dropdown-item" href="http://sci.vru.ac.th/imq">��ѡ�ٵ��ҵ��Է���ص��ˡ�������к��س�Ҿ</a>
        </div>
      </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font color="#000000">
          �ç���ҧ��ú�����</font>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="structure.html">�ç���ҧͧ���</a>
          <a class="dropdown-item" href="board-sci.html">��������</a>
          <a class="dropdown-item" href="execultives.html">����º��������</a>
          <a class="dropdown-item" href="board.html">��С�����ú����ä��</a>
          <a class="dropdown-item" href="academic.html">�ؤ�ҡ�����Ԫҡ��</a>
		  <a class="dropdown-item" href="support.html">�ؤ�ҡ����ʹѺʹع</a>
        </div>
      </li>
    <li class="nav-item">
        <a class="nav-link" href="activities.html"><font color="#000000">�Ҿ�Ԩ����</font></a>
      </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font color="#000000">
          ���С�û�Ъ��</font>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="Boardmeeting.html">��Ъ����С�����ú����ä��</a>
		  <a class="dropdown-item" href="Conference.html">��Ъ����������Ԫҡ�ä��</a>
        </div>
      </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font color="#000000">
          ��Сѹ�س�Ҿ</font>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="qa.html">������ Ẻ�����</a>
          <a class="dropdown-item" href="http://www.cheqa.mua.go.th/">CHE QA 3D ONLINE SYSTEM</a>
          <a class="dropdown-item" href="http://qa.vru.ac.th/">�ӹѡ�ҵðҹ��ШѴ��äس�Ҿ</a>
        </div>
      </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font color="#000000">
          �����Ѵ</font>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="research1.html">��µç��������</a>
		  <a class="dropdown-item" href="">Ẻ���Ǩ�����֧��㨢ͧ������ԡ��</a>
		  <a class="dropdown-item" href="research.html">�Ԩ��</a>
		  <a class="dropdown-item" href="IT.html">���ʹ��</a>
		  <a class="dropdown-item" href="#">��������</a>
        </div>
	  </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><font color="#000000">
          �����͡�û�Ժѵԧҹ</font>
        </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		  <a class="dropdown-item" href="http://comit.vru.ac.th/">�ҹ��ԡ��</a>
      </li>
    </ul>
   </div>
</nav> 
</div> 
</div> 
<!-- end menu -->


</body>
</html>
