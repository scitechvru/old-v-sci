<?php $id_teacher = $_GET['id']; ?>
<?php
session_start();
include 'headeradmin.php';
include "chksession_admin.php";
include 'navbaradmin.php';
include 'account.php';
?>

<link rel="stylesheet" type="text/css" href="css/foopicker.css">
<script type="text/javascript" src="js/foopicker.js"></script>

<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">

                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <strong>เพิ่มข้อมูลอาจารย์</strong>
                                </div>
                                <div class="card-body card-block">
                                    <form action="cont_edit_save.php?id=<?php echo $id_teacher ;?>" method="post" enctype="multipart/form-data"
                                        class="form-horizontal">
                                        <?php
                         
                         $no = 0;
                         include "connect.php";
                         $sql = "select * from contact_teacher  where id_th = $id_teacher";
                         $result = mysql_db_query($dbname, $sql);
                         $num = mysql_num_rows($result);
                         if ($num > 0) {
                             while ($r = mysql_fetch_array($result)) {
                                 $id_th = $r[id_th];
                                 $th_name = $r[th_name];
                                 $th_img = $r[th_img];
                                 $th_fb = $r[th_fb];
                                 $th_line = $r[th_line];
                                 $th_email = $r[th_email];
                                 $th_tel = $r[th_tel];
                                 $th_other = $r[th_other];
                                 $th_major = $r[th_major];
                                 $no++;
                        ?>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="text-input" class=" form-control-label">ชื่อ</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="name" name="name"  value="<?php echo "$th_name"; ?>"
                                                    class="form-control" required="required">

                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="text-input" class=" form-control-label">เบอร์</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="tel" name="tel" value="<?php echo "$th_tel"; ?>"  maxlength="10" 
                                                    class="form-control">

                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="text-input" class=" form-control-label">FB</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="fb" name="fb"  value="<?php echo "$th_fb"; ?>"
                                                    class="form-control">

                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="text-input" class=" form-control-label">LINE</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="line" name="line"  value="<?php echo "$th_line"; ?>"
                                                    class="form-control">

                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="text-input" class=" form-control-label">Other</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="other" name="other"  value="<?php echo "$th_other"; ?>"
                                                    class="form-control">

                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="email-input" class=" form-control-label">Email</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="email" id="email" name="email" value="<?php echo "$th_email"; ?>"
                                                    class="form-control">

                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="select" class=" form-control-label">สาขา</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="major" id="major" required="required" class="form-control">
                                                    <option value="<?php echo "$th_major"; ?>"><?php echo "$th_major"; ?></option>
                                                    <option value="โภชนาการและการกำหนดอาหาร">โภชนาการและการกำหนดอาหาร</option> 
                                                    <option value="คณิตศาสตร์ประยุกต์">คณิตศาสตร์ประยุกต์</option>
                                                    <option value="ฟิสิกส์ประยุกต์">ฟิสิกส์ประยุกต์</option>
                                                    <option value="เคมี">เคมี</option>
                                                    <option value="เทคโนโลยีสารสนเทศ">เทคโนโลยีสารสนเทศ</option>
                                                    <option value="วิทยาการคอมพิวเตอร์">วิทยาการคอมพิวเตอร์</option>
                                                    <option value="นวัตกรรมดิจิทัลและวิศวกรรมซอฟแวร์">
                                                        นวัตกรรมดิจิทัลและวิศวกรรมซอฟแวร์</option>
                                                    <option value="เทคโนโลยีชีวภาพ">เทคโนโลยีชีวภาพ</option>
                                                    <option value="การจัดการภัยพิบัติและบรรเทาสาธารณภัย">
                                                        การจัดการภัยพิบัติและบรรเทาสาธารณภัย</option>
                                                    <option value="วิทยาศาสตร์และนวัตกรรมเพื่อการศึกษา">
                                                        วิทยาศาสตร์และนวัตกรรมเพื่อการศึกษา</option>
                                                    <option value="อาชีวอนามัยและความปลอดภัย">อาชีวอนามัยและความปลอดภัย
                                                    </option>
                                                    <option value="วิทยาศาสตร์และเทคโนโลยีสิ่งแวดล้อม">
                                                        วิทยาศาสตร์และเทคโนโลยีสิ่งแวดล้อม</option>
                                                    <option value="คหกรรมศาสตร์">คหกรรมศาสตร์</option>
                                                    <option value="นวัตกรรมอาหารและเครื่องดื่มเพื่อสุขภาพ">
                                                        นวัตกรรมอาหารและเครื่องดื่มเพื่อสุขภาพ</option>
                                                    <option value="มาตรวิทยาอุตสาหกรรมและระบบคุณภาพ">
                                                        มาตรวิทยาอุตสาหกรรมและระบบคุณภาพ</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="file-input" class=" form-control-label">รูป</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="file" id="img" name="img"
                                                    onchange="return fileValidation()"  required="required" class="form-control-file">
                                            </div>
                                            <!-- Image preview -->
                                            <div id="imagePreview"></div>
                                        </div>
                                        <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> บันทึก
                                    </button>
                                    <?php }
                            }
                            ?>
                                </div>
                                    </form>
                                </div>
                                
                            </div>

                        </div>




                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <?php include 'footeradmin.php';?>
    <script type="text/javascript">
    //สคริปห้ามกรอกตัวหนังสือกรอกได้เฉพาะตัวเลข
    $("#tel").keydown(function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    </script>

    <script>
    function fileValidation() {
        var fileInput = document.getElementById('img');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('รองรับแค่ไฟล์นามสกุล .png .jpg .jpeg เท่านั้น.');
            fileInput.value = '';
            return false;
        } else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    document.getElementById('imagePreview').innerHTML = '<img src="' + e.target.result + '"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }
    }
    </script>