<?php
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>เพิ่มวิดีโอเผยแพร่</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="index.php">
                            <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าหลัก
                        </a>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!-- right column -->
                <div class="col-md-6">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มวิดีโอเผยแพร่</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form METHOD=POST ACTION="video_add2.php" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อวิดีโอ</label>
                                            <input type="text" class="form-control" name="name"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>โค้ดวิดีโอ</label>
                                            <input type="text" class="form-control" name="code"
                                                placeholder="https://youtu.be/โค้ดวิดีโอ">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Link</label>
                                            <input type="text" class="form-control" name="link"
                                                placeholder="กดที่แชร์แล้วcopyวางได้เลย">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- select -->
                                        <div class="form-group">
                                            <label>สังกัด</label>
                                            <select name="major" id="major" class="form-control" width="30">
                                                <?php  if($sess_major== 'OF' ){
                                                      include "connect.php";
                                                      $sql = "select * from tb_major order by id_major";
                                                      $result = mysql_db_query($dbname, $sql);
                                                      $qr = mysql_query($sql);
                                                      while ($result = mysql_fetch_array($qr)) {
                                                          $id_major = $result['id_major'];
                                                          $nameth_major = $result['nameth_major'];
                                                          $int_major = $result['int_major'];

                                                          ?>
                                                <option value="<?php echo "$int_major"; ?>">
                                                    <?php echo "$nameth_major"; ?>
                                                </option>
                                                <?php } }
                                                      else {
                                                        include "connect.php";
                                                        $sql = "select * from tb_major  WHERE int_major='$sess_major'";
                                                        $result = mysql_db_query($dbname, $sql);
                                                        $qr = mysql_query($sql);
                                                        while ($result = mysql_fetch_array($qr)) {
                                                            $id_major = $result['id_major'];
                                                            $nameth_major = $result['nameth_major'];
                                                            $int_major = $result['int_major'];
                                                            ?>
                                                <option value="<?php echo "$int_major"; ?>">
                                                    <?php echo "$nameth_major"; ?>
                                                </option>
                                                <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>วันที่เผยแพร่</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="far fa-calendar-alt"></i></span>
                                                </div>
                                                <input type="text" class="form-control" name="date_p"
                                                    data-inputmask-alias="datetime"
                                                    data-inputmask-inputformat="yyyy/mm/dd" data-mask>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
</div>
<?php include "f.php";?>