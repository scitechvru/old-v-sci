<?php include 'h.php';?>
<?php include "chksession_admin.php";?>
<!-- MENU SIDEBAR-->
<?php include 'navbar.php';?>

<style type="text/css">
.container {
    max-width: 600px;
}

label {
    color: #999;
    display: block;
    font-size: 14px;
    font-family: 'Source Sans Pro', sans-serif;
}

input[type="text"] {
    padding: 10px;
    width: 100%;
}

/* Add a gray background color and some padding to the footer */

.carousel-inner img {
    width: 100%;
    /* Set width to 100% */
    margin: auto;
    min-height: 200px;
}

/* Hide the carousel text when the screen is less than 600 pixels wide */
@media (max-width: 400px) {
    .carousel-caption {
        display: none;
    }
}


input[type=text],
select,
textarea {
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    resize: vertical;
}

label {
    padding: 12px 12px 12px 0;
    display: inline-block;
}

input[type=submit] {
    background-color: #4CAF50;
    color: white;
    padding: 12px 40px;
    border: none;
    border-radius: 12px;
    cursor: pointer;
    float: right;
}

input[type=submit]:hover {
    background-color: #45a049;
}

.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
}

.col-50 {
    float: left;
    width: 50%;
    margin-top: 6px;
}

.col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
</style>
<div class="page-container">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1">Dashboard</h2>
                            <button class="au-btn au-btn-icon au-btn--blue">
                                <i class="zmdi zmdi-plus"></i>add item</button>
                        </div>
                    </div>
                    <div class="container">
                        <FORM METHOD=POST ACTION="admin_report2.php" ENCTYPE="multipart/form-data">
                            <div align="center">
                                <H2>Report Consult</H2>
                            </div>

                            <div class="row">
                                <div class="col-25">
                                    <label>Type Report</label>
                                </div>
                                <div class="col-75">
                                    <INPUT TYPE="radio" id="type_report" NAME="type_report" value="1">
                                    มติการประชุมกรรมการบริหารคณะ
                                    <BR><INPUT TYPE="radio" id="type_report" NAME="type_report" value="2">
                                    มติการประชุมกรรมการวิชาการคณะ
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-25">
                                    <label>Number Report</label>
                                </div>
                                <div class="col-25">
                                    <SELECT NAME="num_report" id="num_report">
                                        <?php
for ($num = 1; $num <= 12; $num++) {
    echo "<OPTION VALUE='$num' >$num";
}
?>
                                    </SELECT>
                                </div>
                                <div class="col-25" align="center">
                                    <label>Year Report</label>
                                </div>
                                <div class="col-25">
                                    <SELECT NAME="year_report" id="year_report">
                                        <?php
date_default_timezone_set("Asia/Bangkok");
$y = date("Y") + 543;
//$y1 = $y - 5 ;
for ($y1 = $y - 2; $y1 <= $y; $y--) {
    echo "<OPTION VALUE='$y' >$y";
}
?>
                                    </SELECT>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-25">
                                    <label>Date Report</label>
                                    <br>
                                    <span>ปี/เดือน/วัน</span>
                                </div>
                                <div class="col-25">
                                    <?php include "inputdate.php";?>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='date' class="form-control" name="date_report"
                                            placeholder="Date Report.." />
                                            
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-25">
                                    <label for="fname">File Report</label>
                                </div>
                                <div class="col-75">
                                    <input type="file" id="file_report" name="file_report">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-50">
                                    <input type="submit" value="SAVE">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>




            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
</div>

</div>

<?php include 'f.php';?>


<!--
<BR><CENTER>
<div class="container22" >
<div align="center"  ><H2>หลักสูตร</H2> </div>
  <div class="row" bgcolor="#f2f2f2f">
<table >
  <tr>
   <th><CENTER>Num</CENTER></th>
   <th><CENTER>Title</CENTER></th>
   <th><CENTER>Detail</CENTER></th>
   <th><CENTER>Link</CENTER></th>
   <th><CENTER>End</CENTER></th>
   <th><CENTER>Status</CENTER></th>
   <th colspan ="2" ><CENTER>Manage</CENTER></th>
  </tr>

-->
<!--
<BR>
<BR>
<div class="container">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="banner/master.jpg">
        <div class="carousel-caption">
          <h3>Master Banner</h3>
          <p>Number 0</p>
        </div>
      </div>
-->
<?php
/*
$no=0;
include "connect.php";
$sql="select * from tb_banner order by id_ban";
$result=mysql_db_query($dbname,$sql);
$num=mysql_num_rows($result);
if($num>0) {
while ($r=mysql_fetch_array($result)) {
$id_ban=$r[id_ban];
$name_ban=$r[name_ban];
$title_ban=$r[title_ban];
$detail_ban=$r[detail_ban];
$link_ban=$r[link_ban];
$end_ban=$r[end_ban];
$status_ban=$r[status_ban];
$no++;
if(($no%2)==0)
{$row_color="#D8D8D8";}
else {$row_color="#FFFFFF";}

echo" <tr bgcolor=$row_color>
<td><CENTER>$no</CENTER></td>
<td>$title_ban</td>
<td>$detail_ban</td>
<td>$link_ban</td>
<td><CENTER>$end_ban</CENTER></td>
<td><CENTER>$status_ban</CENTER></td>
<td>Edit</td>
<td>Del</td>
</tr>  ";
 */
?>
<!-- Wrapper for slides
      <div class="item ">
        <img src="banner/<?php echo "$name_ban"; ?>">
        <div class="carousel-caption">
          <h3><?php echo "$detail_ban"; ?></h3>
          <p><?php echo "$no"; ?></p>
        </div>
      </div>
-->
<?php
//        } // end while
?>
<!--    </div>
    <!-- Left and right controls
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
-->

<?php
//} // end if
?>
</table>
</div>
</CENTER>