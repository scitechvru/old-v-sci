<?php include "chksession_admin.php"; ?>
<html lang="en">
<head>
  <title>SCI VRU</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link rel="stylesheet" type="text/css" href="css/foopicker.css">
  <script type="text/javascript" src="js/foopicker.js"></script>
 
  <style type="text/css">
	.container {
      max-width: 600px;
    }

    label {
      color: #999;
      display: block;
      font-size: 14px;
      font-family: 'Source Sans Pro', sans-serif;
    }

    input[type="text"] {
      padding: 10px;
      width: 100%;
    }
  /* Add a gray background color and some padding to the footer */
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 400px) {
    .carousel-caption {
      display: none; 
    }
  }
      footer {
      background-color: #ECEFF1;
      padding: 25px;
    }


input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 400px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}

  </style>
</head>
<body>
<? include "navbar.php" ?>

<BR>
<div class="container">
  <FORM METHOD=POST ACTION="ban_insert2.php" ENCTYPE="multipart/form-data">
<div align="center"><H2>BANNER</H2> </div>  

  <div class="row">
    <div class="col-25">
      <label for="fname">Name Banner</label>
    </div>
    <div class="col-75">
      <input type="file" id="nameban" name="nameban">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="lname">Title Banner</label>
    </div>
    <div class="col-75">
      <input type="text" id="titleban" name="titleban" placeholder="Title Banner...">
    </div>
  </div>
   <div class="row">
    <div class="col-25">
      <label for="subject">Detail Banner</label>
    </div>
    <div class="col-75">
      <textarea id="detailban" name="detailban" placeholder="Write something.." style="height:100px"></textarea>
    </div>
  </div> 
  
  <div class="row">
    <div class="col-25">
      <label for="lname">Link Banner</label>
    </div>
    <div class="col-75">
      <input type="text" id="linkban" name="linkban" placeholder="Link Banner...">
    </div>
  </div>

  <div class="row">
    <div class="col-25">
      <label for="lname">End Banner</label>
    </div>
    <div class="col-75">
    <input type="text" id="datepicker" name="enddate" placeholder="End Banner.."  />
	</div>
  </div>

  <script type="text/javascript">
    var foopicker = new FooPicker({
      id: 'datepicker', dateFormat: 'yyyy-MM-dd',
    });
  </script>

  <div class="row">
    <input type="submit" value="Submit">
  </div>
  </form>
</div>

<BR>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="banner/master.jpg">
        <div class="carousel-caption">
          <h3>Master Banner</h3>
          <p>Number 0</p>
        </div>     
      </div>

<?
$no=0;
include "connect.php";
$sql="select * from tb_banner order by id_ban";
$result=mysql_db_query($dbname,$sql);
$num=mysql_num_rows($result);
if($num>0) {
	while ($r=mysql_fetch_array($result)) {
		$id_ban=$r[id_ban];
		$name_ban=$r[name_ban];
		$detail_ban=$r[detail_ban];
		$no++;

?>
    <!-- Wrapper for slides -->
      <div class="item ">
        <img src="banner/<?echo"$name_ban";?>">
        <div class="carousel-caption">
          <h3><?echo"$detail_ban";?></h3>
          <p><?echo"$no";?></p>
        </div>      
      </div>

<?	
		} // end while 	
?> 
    </div>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>

<?	
} // end if 
?> 




<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>
</BODY>
</HTML>