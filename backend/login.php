<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SciTech VRU | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="apple-touch-icon" sizes="180x180" href="../images/fav-icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="../images/fav-icon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="../images/fav-icon/favicon-16x16.png" sizes="16x16">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="index2.html"><b>SciTech</b>VRU</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">คณะวิทยาศาสตร์และเทคโนโลยี</p>
      <?php
      require_once "../inc/function.php";
      session_start();
      
      if(isset($_POST["submit"])){
        adminLogin($_POST["username"],$_POST["password"],$_POST["chk"]);
        $data=adminLogin();
      ?>
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>กรุณาตรวจสอบ</strong> <?php echo "$data";?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <?php } ?>
      <form action="" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="username" name="username" required="required" placeholder="User">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" id="password" name="password" required="required"  placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <!-- select -->
            <div class="form-group">
                        <select class="form-control" id="chk" name="chk" required="required">
                          <option>โปรดเลือกสังกัด</option>
                          <?php
                          require "../inc/connect.php";
                          $sql = "select * from tb_major order by id_major";
                          $result = mysql_db_query($dbname, $sql);
                          $qr = mysql_query($sql);
                          while ($result = mysql_fetch_array($qr)) {
                              $id_major = $result['id_major'];
                              $nameth_major = $result['nameth_major'];
                              $int_major = $result['int_major'];
                              ?>
                                        <option value="<?php echo "$int_major"; ?>">
                                            ผู้ดูแล<?php echo "$nameth_major"; ?>
                                        </option>
                                        <?php }?>
                                        </select>
                      </div>
          </div>
          <!-- /.col -->
          
        </div>
        <div class="row">
          
          <div class="col">
          <button type="submit" name="submit"  class="btn btn-primary btn-block">เข้าสู่ระบบ</button>
          </div>
          <div class="col">
          <button class="btn btn-warning btn-block" onclick="window.location.href = '../index.php?index=' + this.selectedIndex;">กลับสู่หน้าหลัก</button>
          </div>
        </div>
      </form>

      
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

</body>
</html>
