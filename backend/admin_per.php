<?php 
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการบุคลากรภายในคณะ</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="admin_addper.php">
                            <i class="fas fa-plus"></i> เพิ่มบุคลากรภายในคณะ
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">จัดการบุคลากรภายในคณะ</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">ลำดับ</th>
                                        <th scope="col" class="text-center">ชื่อ - นามสกุล</th>
                                        <th scope="col" class="text-center">หลักสูตร</th>
                                        <th scope="col" class="text-center">ตำแหน่งบริหาร</th>
                                        <th scope="col" class="text-center">ตำแหน่งวิชาการ</th>
                                        <th scope="col" class="text-center">การศึกษา</th>
                                        <th scope="col" class="text-center">ประวัติ</th>
                                        <th scope="col" class="text-center">สถานะ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                        $no = 0;

                        $sql = "select * from tb_per order by major_per";
                        $result = mysql_db_query($dbname, $sql);
                        $num = mysql_num_rows($result);
                        if ($num > 0) {
                            while ($r = mysql_fetch_array($result)) {
                                $id_per = $r[id_per];
                                $idcard_per = $r[idcard_per];
                                $tname_per = $r[tname_per];
                                $fnamet_per = $r[fnamet_per];
                                $lnamet_per = $r[lnamet_per];
                                $img_per = $r[img_per];
                                $status_per = $r[status_per];
                                $type_per = $r[type_per];
                                $email=$r[email_per];
                                $major=$r[major_per];

                                $sql2 = "select * from tb_pered where idcard_per='$idcard_per' order by level_pered ASC";
                                $result2 = mysql_db_query($dbname, $sql2);
                                while ($r2 = mysql_fetch_array($result2)) {
                                    $level_pered = $r2[level_pered];
                                }
                                switch ($level_pered) {
                                    case '1': $level_ed = "ปริญญาตรี"; break;
                                    case '2': $level_ed = "ปริญญาโท"; break;
                                    case '3': $level_ed = "ปริญญาเอก"; break;
                                    
                                }

                                $sql3 = "select * from tb_peraca2 where idcard_per='$idcard_per' ";
                                $result3 = mysql_db_query($dbname, $sql3);
                                while ($r3 = mysql_fetch_array($result3)) {
                                    $level_peraca = $r3[level_peraca];
                                    //$type_peraca = $r3[type_peraca];
                                }
                                if($type_per == '1' or $type_per == '3' ){
                                    switch ($level_peraca) {
                                        case '1':$type_per2="อ."; break;
                                        case '2':$type_per2="ผศ."; break;
                                        case '3':$type_per2="รศ."; break;
                                        case '4':$type_per2="ศ."; break;                                 
                                    }//end switch
                                }elseif($type_per == '2' or $type_per == '4' ){
                                    switch($level_peraca){
                                        case '1':$type_per2="ปฎิบัติการ"; break;
                                        case '2':$type_per2="ชำนาญการ"; break;
                                        case '3':$type_per2="ชำนาญการพิเศษ"; break;
                                        case '4':$type_per2="เชี่ยวชาญ"; break;
                                        case '5':$type_per2="เชี่ยวชาญพิเศษ"; break;
                                    }  //end switch
                                }//end if
                                $level_peraca = "";
                                
                                $sql4="select * from tb_ex where id_per ='$idcard_per' order by position_ex";
                                $result4=mysql_db_query($dbname,$sql4);
                                $resultArray = array();
                                while ($r4 = mysql_fetch_array($result4)) {
                                    $position = $r4[position_ex];
                                    //$major = $r4[major_ex];

                                    switch($position){
                                        case'1': $position = "คณบดี"; break;
                                        case'2': $position = "รองคณบดี"; break;
                                        case'4': $position = "ผู้ช่วยคณบดี"; break; 
                                        case'7': $position = "ประธานหลักสูตร"; break;
                                        case'8': $position = "หัวหน้าสำนักงาน"; break;
                                    
                                    //    case'0': $position = "อื่นๆ"; break;
                                    } //end switch
                                    array_push($resultArray,$position);
                                }//end while


                                $no++;?>
                                    <tr>
                                        <th class="text-center"><?php echo "$no";?></th>
                                        <td class="text-sm-left"> <?php echo "$tname_per $fnamet_per  $lnamet_per";?>
                                        </td>
                                        <td class="text-center"> <?php echo "$major";?></td>
                                        <td class="text-center">
                                            <a class="btn btn-app"
                                                href="admin_addex.php?id_per=<?php echo "$id_per";?>">
                                                <?php 
                                            if(!$resultArray[0]){
                                                echo"<i class='fas fa-school nav-icon'></i>";

                                            }else{
                                                echo "$resultArray[0]"."<br>"."$resultArray[1]"."<br>"."$resultArray[2]";
                                            }?>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                <?php if($type_per == '1' or $type_per == '3' ){ ?>
                                        <a href="pac1.php?id_per=<?php echo "$idcard_per";?>"
                                                class="btn btn-app">
                                                
                                <?php }elseif($type_per == '2' or $type_per == '4' ){ ?>
                                        <a href="pac2.php?id_per=<?php echo "$idcard_per";?>"
                                                class="btn btn-app">
                                                
                                <?php }//end if ?>
                                            <i class="fas fa-id-card nav-icon"></i>
                                <?php echo "$type_per2"; $type_per2 = "";?>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="admin_added.php?id_per=<?php echo "$id_per";?>"
                                                class="btn btn-app"><i class="fas fa-graduation-cap nav-icon"></i>
                                                <?php echo "$level_ed";?>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="admin_addper3.php?id_per=<?php echo "$id_per";?>"
                                                class="btn btn-app"><i class="fas fa-user nav-icon"></i>
                                                ประวัติ
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <?php if ($status_per == 1) { ?>
                                            <a href="admin_addper5.php?id_edit=<?php echo "$id_per";?>&status=<?php echo "$status_per";?>"
                                                class="btn btn-app"><i class="fas fa-eye nav-icon"></i>Show

                                            </a>
                                            <?php }else{?>
                                            <a href="admin_addper5.php?id_edit=<?php echo "$id_per";?>&status=<?php echo "$status_per";?>"
                                                class="btn btn-app"><i class="fas fa-eye-slash nav-icon"></i>Hide

                                            </a>
                                            <?php  } ?>
                                        </td>
                                    </tr>
                                    <?php
                                    } // end while
                                } // end if
                            ?>
                                </tbody>
                            </table>



                        </div>
                    </div>
                </div>

            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<script>
$(function() {
    $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});
</script>
<!--
<div class="col-md-4">

    <div class="card card-widget widget-user-2">

        <div class="widget-user-header bg-warning">
            <div class="widget-user-image">

                <img class="img-circle elevation-2" src="img_resize/<?php echo "$img_per";?>"
                    alt="<?php echo "$fnamet_per  $lnamet_per";?>">
            </div>

            <h3 class="widget-user-username">
                <?php echo "$tname_per $fnamet_per  $lnamet_per";?></h3>
            <h5 class="widget-user-desc"><?php echo "$email";?></h5>
        </div>
        <div class="card-footer p-0">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="admin_addex.php?id_per=<?php echo "$id_per";?>" class="nav-link">
                        ตำแหน่งทางการบริหารภายในคณะ
                    </a>
                </li>
                <li class="nav-item">
                    <a href="admin_addap.php?id_per=<?php echo "$id_per";?>" class="nav-link">
                        ตำแหน่งทางวิชาการ
                    </a>
                </li>
                <li class="nav-item">
                    <a href="admin_added.php?id_per=<?php echo "$id_per";?>" class="nav-link">
                        ประวัติการศึกษา
                    </a>
                </li>

                <li class="nav-item">
                    <a href="admin_addper3.php?id_per=<?php echo "$id_per";?>" class="nav-link">
                        ประวัติ
                    </a>
                </li>
                <?php if ($status_per == 1) { ?>
                <li class="nav-item">
                    <a href="admin_addper5.php?id_edit=<?php echo "$id_per";?>&status=<?php echo "$status_per";?>"
                        class="nav-link">
                        สถานะ: SHOW
                    </a>
                </li>
                <?php }else{?>
                <li class="nav-item">
                    <a href="admin_addper5.php?id_edit=<?php echo "$id_per";?>&status=<?php echo "$status_per";?>"
                        class="nav-link">
                        สถานะ: HIDE
                    </a>
                </li>
                <?php }?>
            </ul>
        </div>
    </div>


</div>
-->