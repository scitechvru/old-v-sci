<?php
session_start();
include 'h.php';
include "chksession_admin.php";
include 'chksession_admin.php';
include 'navbar.php';

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการข้อมูลแบนเนอร์</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="admin_banner1.php">
                            <i class="fas fa-plus"></i> เพิ่มแบนเนอร์
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!-- /.card-header -->
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="row mb-2"></div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">จัดการข้อมูลแบนเนอร์</h3>
                            </div>
                            <div class="card-body">

                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col" class="text-center">ลำดับ</th>
                                            <th scope="col" class="text-center">แบนเนอร์</th>
                                            <th scope="col" class="text-center">สถานะ</th>
                                            <th scope="col" class="text-center">ลบ</th>

                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <?php
                         if ($sess_major == 'OF') {
                             $no = 0;
                             include 'connect.php';
                             $sql = 'select * from tb_banner order by id_ban DESC ';
                             $result = mysql_db_query($dbname, $sql);
                             $num = mysql_num_rows($result);
                             if ($num > 0) {
                                 while ($r = mysql_fetch_array($result)) {
                                     $id_ban = $r[id_ban];
                                     $name_ban = $r[name_ban];
                                     $title_ban = $r[title_ban];
                                     $detail_ban = $r[detail_ban];
                                     $link_ban = $r[link_ban];
                                     $end_ban = $r[end_ban];
                                     $status_ban = $r[status_ban];
                                     ++$no; ?>
                                            <th class="text-center" scope="row"><?php echo "$no"; ?></th>
                                            <td><?php echo "<a target='_blank' href='../backend/banner/$name_ban'>
                            <img src='img_resize_ban/$name_ban' class='img-fluid' >
                            </a> "; ?></td>
                                            <td><?php if ($status_ban == 1) {
                                         echo "<A HREF='admin_banner3.php?id_edit=$id_ban&status=$status_ban'><IMG SRC='images/show.png' WIDTH='30'></A>	";
                                     } else {
                                         echo "<A HREF='admin_banner3.php?id_edit=$id_ban&status=$status_ban'><IMG SRC='images/hide.png' WIDTH='30'></A>	";
                                     } ?></td>
                                            <td><A
                                                    HREF="admin_del.php?id_del_ban=<?php echo "$id_ban"; ?>&name_ban=<?php echo"$name_ban"; ?>"><IMG
                                                        SRC='images/del.png' WIDTH='30'></A> </td>
                                        </tr>
                                        <?php
                                 }
                             }
                         } else {
                             $no = 0;
                             include 'connect.php';
                             $sql = "select * from tb_banner WHERE major_ban='$sess_major' order by id_ban DESC ";
                             $result = mysql_db_query($dbname, $sql);
                             $num = mysql_num_rows($result);
                             if ($num > 0) {
                                 while ($r = mysql_fetch_array($result)) {
                                     $id_ban = $r[id_ban];
                                     $name_ban = $r[name_ban];
                                     $title_ban = $r[title_ban];
                                     $detail_ban = $r[detail_ban];
                                     $link_ban = $r[link_ban];
                                     $end_ban = $r[end_ban];
                                     $status_ban = $r[status_ban];
                                     ++$no; ?>
                                        <th class="text-center" scope="row"><?php echo "$no"; ?></th>
                                        <td><?php echo "$title_ban <a target='_blank' href='../backend/banner/$name_ban'>
                            <img src='img_resize_ban/$name_ban' class='img-rounded' >
                            </a> $link_ban"; ?></td>
                                        <td><?php if ($status_ban == 1) {
                                         echo "<A HREF='admin_banner3.php?id_edit=$id_ban&status=$status_ban'><IMG SRC='images/show.png' WIDTH='30'></A>	";
                                     } else {
                                         echo "<A HREF='admin_banner3.php?id_edit=$id_ban&status=$status_ban'><IMG SRC='images/hide.png' WIDTH='30'></A>	";
                                     } ?></td>
                                        <td><A
                                                HREF="admin_del.php?id_del_ban=<?php "$id_ban"; ?>&name_ban=<?php "$name_ban"; ?>"><IMG
                                                    SRC='images/del.png' WIDTH='30'></A> </td>
                                        </tr>
                                        <?php
                                 }
                             }
                         }
                            ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>

<?php include 'f.php'; ?>

<script>
$(function() {
    $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});
</script>