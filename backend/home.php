<?php 
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
include "connect.php";
include "array.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการข้อมูลผู้ดูแลระบบ</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="admin_admins1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลผู้ดูแลระบบ
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">ผู้ดูแลระบบ</h3>

                        </div>
                        <!-- /.card-header -->

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">ลำดับ</th>
                                        <th scope="col" class="text-center">ชื่อ</th>
                                        <th scope="col" class="text-center">ดูแลหลักสูตร</th>
                                        <th scope="col" class="text-center">ลบ</th>
                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php

$no = 0;
$sql = "select * from tb_admin order by id_admin";
$result = mysql_db_query($dbname, $sql);
$num = mysql_num_rows($result);
if ($num > 0) {
    while ($r = mysql_fetch_array($result)) {
        $id_admin = $r[id_admin];
        $fname_admin = $r[fname_admin];
        $lname_admin = $r[lname_admin];
        $major_admin = $r[major_admin];
        $img_admin = $r[img_admin];
        $no++;
        $sql2 = "select * from tb_major where id_major='$major_admin' ";
        $result2 = mysql_db_query($dbname, $sql2);
        $qr = mysql_fetch_array($result2);
        $id_major = $qr[id_major];
        $nameth_major = $qr[nameth_major];
        
                        ?>
                           <th class="text-center" scope="row"><?php echo "$no"?></th>
                           <th class="text-center" scope="row"><?php echo "$fname_admin  $lname_admin"?></th>
                           <td class="text-center"><?php echo "$major_admin"?></td>
                           <td class="text-center"><A HREF="admin_del.php?id_del_admin=<?php echo "$id_admin"?>&img=<?php echo "$img_admin"?>"><IMG SRC="images/del.png" WIDTH="20"></A></td>
                           
                           </tr>
                           <?php }
                           }
                           ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?include 'f.php';?>