<?php session_start();
include "h.php";
include "navbar.php";
require "../inc/connect.php";
require "../inc/function.php";


//$idcard_per = $_SESSION[sess_adminidcard_per];
//$type_per = $_SESSION[sess_admintype];
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>ประเมินสมรรถนะ </h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">รายชื่อผู้ถูกประเมิน</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">ลำดับ</th>
                                        <th class="text-center">idcard</th>
                                        <th class="text-center">รอบ</th>
                                        <th class="text-center">ปี</th>
                                        <th class="text-center">ปม.</th>
                                        <th class="text-center">ระดับ 0 </th>
                                        <th class="text-center">ระดับ 1 </th>
                                        <th class="text-center">ระดับ 2 </th>
                                        <th class="text-center">ระดับ 3 </th>
                                        <th class="text-center">ระดับ 4 </th>
                                        <th class="text-center">ระดับ 5 </th>
                                        <th class="text-center">ค่าเฉลี่ย</th>
                                        <th class="text-center">ผู้ประเมิน</th>
                                    </tr>
                                </thead>
                                <tbody>
            <?php $count = 0;$level0_pm3; $level0_pm3 = 0; $level1_pm3 = 0;$level2_pm3 = 0; $level3_pm3 = 0; $level4_pm3 = 0; $level5_pm3 = 0;

            $sql ="SELECT * FROM tb_perpm3";
            $result = mysql_query($sql,$con);
            while($r = mysql_fetch_array($result)){
                $id_pm3 = $r[id_pm3];
                $idcard_per = $r[idcard_per];
                $r_pm3 = $r[r_pm3];
                $year_pm3 = $r[year_pm3];
                $pm_pm3 = $r[pm_pm3];
                $level0_pm3 = $r[level0_pm3];
                $level1_pm3 = $r[level1_pm3];
                $level2_pm3 = $r[level2_pm3];
                $level3_pm3 = $r[level3_pm3];
                $level4_pm3 = $r[level4_pm3];
                $level5_pm3 = $r[level5_pm3];
                $avg_pm3 = $r[avg_pm3];
                $ass_pm3 = $r[ass_pm3];

                $count++;
                $r_pm3 = "2";
                $year_pm3 = "2564";
                $level0_pm3 = 0;
                if($level5_pm3){
                    $avg_pm3 =  number_format(floatval(($level1_pm3+$level2_pm3+$level3_pm3+$level4_pm3+$level5_pm3)/5),2) ;
                }else if($level4_pm3){
                    $avg_pm3 =  number_format(floatval(($level1_pm3+$level2_pm3+$level3_pm3+$level4_pm3)/4),2) ;
                }else if($level3_pm3){
                    $avg_pm3 =  number_format(floatval(($level1_pm3+$level2_pm3+$level3_pm3)/3),2) ;
                }else if($level2_pm3){
                    $avg_pm3 =  number_format(floatval(($level1_pm3+$level2_pm3)/2),2) ;
                }
                
                $sql2="update tb_perpm3 set  
                r_pm3 = '$r_pm3', year_pm3 ='$year_pm3',
                level0_pm3 = '$level0_pm3',  
                avg_pm3 = '$avg_pm3' 
                where id_pm3='$id_pm3' ";
                $result2=mysql_db_query($dbname,$sql2);
            ?>

                                    <tr>
                                        <td class="text-center"><?php echo"$count"; ?></td>
                                        <td class="text-center"><?php echo"$idcard_per"; ?></td>
                                        <td class="text-center"><?php echo"$r_pm3"; ?></td>
                                        <td class="text-center"><?php echo"$year_pm3"; ?> </td>
                                        <td class="text-center"><?php echo"$pm_pm3"; ?></td>
                                        <td class="text-center"><?php echo"$level0_pm3"; ?></td>
                                        <td class="text-center"><?php echo"$level1_pm3"; ?></td>
                                        <td class="text-center"><?php echo"$level2_pm3"; ?></td>
                                        <td class="text-center"><?php echo"$level3_pm3"; ?></td>
                                        <td class="text-center"><?php echo"$level4_pm3"; ?></td>
                                        <td class="text-center"><?php echo"$level5_pm3"; ?></td>
                                        <td class="text-center"><?php echo"$avg_pm3"; ?></td>
                                        <td class="text-center"><?php echo"$ass_pm3"; ?></td>
                                        
                                    </tr>
            <?php  }//end while ?>

                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php 
  include "f.php";
  ?>
<script>
$(function() {
    $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});
</script>