<?php 
session_start();

include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
require "../inc/connect.php";
require "../inc/educationlist.php";
require "../backend/array.php";
require_once "../inc/function.php";
$idcard_per = $_SESSION[sess_adminidcard_per];
$type_per = $_SESSION[sess_admintype];
$getData_Aca = viewAca($idcard_per);


?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h1>ตำแหน่งทางวิชาการ</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <?php if($type_per == 2){?>
                        <a class="btn btn-app" HREF="pac2.php">
                            <i class="fas fa-plus"></i> เพิ่มตำแหน่งทางวิชาการสายสนับสนุน
                        </a>
                        <?php } elseif($type_per == 4) {?>
                            <a class="btn btn-app" HREF="pac2.php">
                            <i class="fas fa-plus"></i> เพิ่มตำแหน่งทางวิชาการสายสนับสนุน
                        </a>
                        <?php } elseif($type_per == 5) {?>
                            <a class="btn btn-app" HREF="pac2.php">
                            <i class="fas fa-plus"></i> เพิ่มตำแหน่งทางวิชาการสายสนับสนุน
                        </a>
                        <?php } else {?>
                        <a class="btn btn-app" HREF="pac1.php">
                            <i class="fas fa-plus"></i> เพิ่มตำแหน่งทางวิชาการสายวิชาการ
                        </a>
                        <?php }?>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <?php if(empty($getData_Aca)){?>

                <div class="col">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>กรุณาอัพเดทข้อมูล!</strong> ตำแหน่งทางวิชาการของท่าน
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <?php }else{?>
                <?php $i=1; ?>


                <?php foreach ($getData_Aca as $data){ ?>
                <div class="col-md-3">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">ตำแหน่งทางวิชาการ</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <?php
                        if($type_per == '1' or $type_per  == '3' ){
                            switch ($data["level_peraca"] ) {
                                case '1':$level_peraca2="อาจารย์"; break;
                                case '2':$level_peraca2="ผู้ช่วยศาสตราจารย์"; break;
                                case '3':$level_peraca2="รองศาสตราจารย์"; break;
                                case '4':$level_peraca2="ศาสตราจารย์"; break;            
                            } //end switch
                        }elseif($type_per  == '2' or $type_per  == '4' ){
                            switch($data["level_peraca"] ){
                                case '1':$level_peraca2="ปฎิบัติการ"; break;
                                case '2':$level_peraca2="ชำนาญการ"; break;
                                case '3':$level_peraca2="ชำนาญการพิเศษ"; break;
                                case '4':$level_peraca2="เชี่ยวชาญ"; break;
                                case '5':$level_peraca2="เชี่ยวชาญพิเศษ"; break;
                            }  //end switch
                        }//endif
                            
                        ?>
                        <div class="card-body">
                            <p class="text-center">ตำแหน่งทางวิชาการระดับ<?php echo"$level_peraca2";?></p>
                            <p class="text-center">ในปี พ.ศ. <?php echo $data["y_peraca"];?></p>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <?php }?>
                <?php }?>
                <!-- /.col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include 'f.php';?>
<?php
 if($_GET['update']=='success'){
    echo '<script type="text/javascript">
    Swal.fire("อัพเดทสถานะสำเร็จ","","success");
    </script>';
    }elseif ($_GET['update']=='nodata'){
        echo '<script type="text/javascript">
        swal.fire("โปรดกรอกข้อมูลให้ครบ", "", "warning");
        </script>';
    }
    ?>