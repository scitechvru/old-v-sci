<?php
include "h.php";
include "navbar.php";
include "connect.php";
include "array.php";
include "chksession_admin.php"; 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>หน้าหลัก</h1>
          </div>
          
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
         <!-- Info boxes -->
         <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-comment"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">จำนวนคำถามQ&A</span>
                <?php 
                 include "connect.php";
                  $count5=0 ;
$sql = "SELECT  *
FROM
q qu
WHERE
qu.q_type = 1 ";
$result = mysql_db_query($dbname, $sql);

//$r = mysql_fetch_array($result); 
while ($r = mysql_fetch_array($result))
{
$count5++; }
                ?>
                <span class="info-box-number">
                <?php  echo number_format($count5) ?>  คำถาม  
                </span>
                <?php ?>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-comments"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">จำนวนการตอบQ&A</span>
                <?
                include "connect.php";
                $count=0;
$sql = "SELECT  *
FROM
q qu
WHERE
qu.q_type = 1 and qu.q_status = 3";
$result = mysql_db_query($dbname, $sql);

//$r = mysql_fetch_array($result); 
while ($r = mysql_fetch_array($result))
{
$count++; }?>
                <span class="info-box-number"><?php  echo number_format($count) ?>   คำตอบ</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-comment"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">จำนวนข้อเสนอแนะ</span>
                <?
                include "connect.php";
                $count=0;
$sql = "SELECT  *
FROM
q qu
WHERE
qu.q_type = 3";
$result = mysql_db_query($dbname, $sql);

//$r = mysql_fetch_array($result); 
while ($r = mysql_fetch_array($result))
{
$count++; }?>
                <span class="info-box-number"><?php  echo number_format($count) ?> ข้อเสนอแนะ</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-comment"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">จำนวนข้อร้องเรียน</span>
                <?
                  include "connect.php";
                  $count=0;
                  $sql = "SELECT  *
                  FROM
                  q qu
                  WHERE
                  qu.q_type = 4";
                  $result = mysql_db_query($dbname, $sql);

                  //$r = mysql_fetch_array($result); 
                  while ($r = mysql_fetch_array($result))
                  {
                  $count++; }?>
                <span class="info-box-number"><?php  echo number_format($count) ?> ข้อร้องเรียน</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-comment"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">จำนวนคำถามที่พบบ่อย</span>
                <?
                  include "connect.php";
                  $count3=0;
                  $sql = "SELECT  *
                  FROM
                  question_hit qh
                  WHERE
                  qh.qh_show = 1 ";
                  $result = mysql_db_query($dbname, $sql);

                  //$r = mysql_fetch_array($result); 
                  while ($r = mysql_fetch_array($result))
                  {
                  $count3++; }?>
                <span class="info-box-number"><?php  echo number_format($count3) ?> คำถาม</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            
          </div>
          <!-- /.col -->
          
        </div>
        <!-- /.row -->
        <div class="col-md-6">
            <!-- Bar chart -->
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  จำนวนผู้ถามในะระบบ
                </h3>

              </div>
              <div class="card-body">
                <div id="bar-chart" style="height: 300px;"></div>
              </div>
              <!-- /.card-body-->
            </div>
            <!-- /.card -->
            
      </div>
       <!-- /.row -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php 
  include "f.php";
  ?>

  <?php 
                 include "connect.php";
                 global $OF ;
                  $sql = "SELECT  *
                  FROM
                  q qu
                  WHERE
                  qu.q_type_p = 'บุคลากรสายสนับสนุน'";
                  $result = mysql_db_query($dbname, $sql);

                  //$r = mysql_fetch_array($result); 
                  while ($r = mysql_fetch_array($result))
                  {
                  $OF++; }
?>

  <?php 
                 include "connect.php";
                 global $STU ;
                  $sql = "SELECT  *
                  FROM
                  q qu
                  WHERE
                  qu.q_type_p = 'นักศึกษา' ";
                  $result = mysql_db_query($dbname, $sql);

                  //$r = mysql_fetch_array($result); 
                  while ($r = mysql_fetch_array($result))
                  {
                  $STU++; }
?>
  <?php 
                 include "connect.php";
                 global $AJ ;
                  $sql = "SELECT  *
                  FROM
                  q qu
                  WHERE
                  qu.q_type_p = 'อาจารย์'";
                  $result = mysql_db_query($dbname, $sql);

                  //$r = mysql_fetch_array($result); 
                  while ($r = mysql_fetch_array($result))
                  {
                  $AJ++; }
?>
  <?php 
                 include "connect.php";
                 global $Other ;
                  $sql = "SELECT  *
                  FROM
                  q qu
                  WHERE
                  qu.q_type_p = 'บุคคลภายนอก'";
                  $result = mysql_db_query($dbname, $sql);

                  //$r = mysql_fetch_array($result); 
                  while ($r = mysql_fetch_array($result))
                  {
                  $Other++; }
?>
  <script>
  /*
     * BAR CHART
     * ---------
     */
    var OF1 = "<?php echo"$OF";?>";
    var STU1 = "<?php echo"$STU";?>";
    var AJ1 = "<?php echo"$AJ";?>";
    var Other1 = "<?php echo"$Other";?>";
    var bar_data = {
      data : [[1,STU1], [2,AJ1], [3,OF1], [4,Other1]],
      bars: { show: true }
    }
    $.plot('#bar-chart', [bar_data], {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
         bars: {
          show: true, barWidth: 0.5, align: 'center',
        },
      },
      colors: ['#2a54de'],
      xaxis : {
        ticks: [[1,'นักศึกษา'], [2,'อาจารย์'], [3,'เจ้าหน้าที่'], [4,'บุคคลภายนอก']]
      }
    })
    /* END BAR CHART */
  </script>