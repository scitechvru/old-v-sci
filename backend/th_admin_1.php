<?php
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>เพิ่มข้อมูลอาจารย์</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="index.php">
                            <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าหลัก
                        </a>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!-- right column -->
                <div class="col-md-6">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มข้อมูลอาจารย์</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form METHOD=POST ACTION="th_admin_save.php" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อ</label>
                                            <input type="text" class="form-control" id="name" name="name"
                                                placeholder="Enter ..." required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>เบอร์</label>
                                            <input type="text" class="form-control" id="tel" name="tel"  data-inputmask='"mask": "(999)-999-9999"' data-mask
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>FB</label>
                                            <input type="text" class="form-control" id="fb" name="fb"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>LINE</label>
                                            <input type="text" class="form-control" id="line" name="line"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Other</label>
                                            <input type="text" class="form-control" id="other" name="other"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" id="email" name="email" name="title_news"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            <label>สาขา</label>
                                            <select name="major" id="major" required="required" class="form-control">
                                                <option value="โปรดระบุสาขา">โปรดระบุสาขา</option>
                                                <option value="โภชนาการและการกำหนดอาหาร">โภชนาการและการกำหนดอาหาร
                                                </option>
                                                <option value="คณิตศาสตร์ประยุกต์">คณิตศาสตร์ประยุกต์</option>
                                                <option value="ฟิสิกส์ประยุกต์">ฟิสิกส์ประยุกต์</option>
                                                <option value="เคมี">เคมี</option>
                                                <option value="เทคโนโลยีสารสนเทศ">เทคโนโลยีสารสนเทศ</option>
                                                <option value="วิทยาการคอมพิวเตอร์">วิทยาการคอมพิวเตอร์</option>
                                                <option value="นวัตกรรมดิจิทัลและวิศวกรรมซอฟแวร์">
                                                    นวัตกรรมดิจิทัลและวิศวกรรมซอฟแวร์</option>
                                                <option value="เทคโนโลยีชีวภาพ">เทคโนโลยีชีวภาพ</option>
                                                <option value="การจัดการภัยพิบัติและบรรเทาสาธารณภัย">
                                                    การจัดการภัยพิบัติและบรรเทาสาธารณภัย</option>
                                                <option value="วิทยาศาสตร์และนวัตกรรมเพื่อการศึกษา">
                                                    วิทยาศาสตร์และนวัตกรรมเพื่อการศึกษา</option>
                                                <option value="อาชีวอนามัยและความปลอดภัย">อาชีวอนามัยและความปลอดภัย
                                                </option>
                                                <option value="วิทยาศาสตร์และเทคโนโลยีสิ่งแวดล้อม">
                                                    วิทยาศาสตร์และเทคโนโลยีสิ่งแวดล้อม</option>
                                                <option value="คหกรรมศาสตร์">คหกรรมศาสตร์</option>
                                                <option value="นวัตกรรมอาหารและเครื่องดื่มเพื่อสุขภาพ">
                                                    นวัตกรรมอาหารและเครื่องดื่มเพื่อสุขภาพ</option>
                                                <option value="มาตรวิทยาอุตสาหกรรมและระบบคุณภาพ">
                                                    มาตรวิทยาอุตสาหกรรมและระบบคุณภาพ</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="img" name="img"
                                                    required="required" onchange="return fileValidation()"
                                                    name="file_news">
                                                <label class="custom-file-label" for="customFile">รูปอาจารย์</label>
                                            </div>
                                            <div id="imagePreview"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">บันทึก</button>
                                    
                                </div>

                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
        <?php include 'f.php';?>
        <script type="text/javascript">
        //สคริปห้ามกรอกตัวหนังสือกรอกได้เฉพาะตัวเลข
        $("#tel").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        </script>

        <script>
        function fileValidation() {
            var fileInput = document.getElementById('img');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.png|\.jpg|\.jpeg)$/i;
            if (!allowedExtensions.exec(filePath)) {
                alert('รองรับแค่ไฟล์นามสกุล .png .jpg .jpeg เท่านั้น.');
                fileInput.value = '';
                return false;
            } else {
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById('imagePreview').innerHTML = '<img src="' + e.target.result + '"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }
        </script>
        <!--
        <div class="row">
                                    <div class="col-sm-6">
                                       
                                        <div class="form-group">
                                            <label>ประเภทข่าว</label>
                                            <select class="form-control" id="type_news" name="type_news">
                                            <option value="">โปรดระบุประเภทข่าว</option>
                                                <option value="PR"> ประชาสัมพันธ์</option>
                                                <option value="ED"> ศึกษาต่อ</option>
                                                <option value="RE"> รับสมัครงาน</option>
                                                <option value="AL"> ศิษย์เก่า</option>
                                                <option value="PRM">ประชาสัมพันธ์หลักสูตร</option>
                                                <option  value="ITA"> Work@Home</option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                           
                                            <div class="form-group">
                                                <label>สังกัด</label>
                                                <select name="Major_news" id="Major_news" class="form-control"
                                                    width="30">
                                                    <option value="">โปรดระบุสังกัด</option>
                                                    <?php /* if($sess_major== 'OF' ){
                                                      include "connect.php";
                                                      $sql = "select * from tb_major order by id_major";
                                                      $result = mysql_db_query($dbname, $sql);
                                                      $qr = mysql_query($sql);
                                                      while ($result = mysql_fetch_array($qr)) {
                                                          $id_major = $result['id_major'];
                                                          $nameth_major = $result['nameth_major'];
                                                          $int_major = $result['int_major'];

                                                          ?>
                                                    <option value="<?php echo "$int_major"; ?>">
                                                        <?php echo "$nameth_major"; ?>
                                                    </option>
                                                    <?php } }
                                                      else {
                                                        include "connect.php";
                                                        $sql = "select * from tb_major  WHERE int_major='$sess_major'";
                                                        $result = mysql_db_query($dbname, $sql);
                                                        $qr = mysql_query($sql);
                                                        while ($result = mysql_fetch_array($qr)) {
                                                            $id_major = $result['id_major'];
                                                            $nameth_major = $result['nameth_major'];
                                                            $int_major = $result['int_major'];
                                                            ?>
                                                    <option value="<?php echo "$int_major"; ?>">
                                                        <?php echo "$nameth_major"; ?>
                                                    </option>
                                                    <?php } }*/?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"id="file_news" name="file_news">
                                                    <label class="custom-file-label"
                                                        for="customFile">ไฟล์ข่าวประชาสัมพันธ์</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    
-->