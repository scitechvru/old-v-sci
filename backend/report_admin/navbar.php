<?php
require_once "../../inc/function.php";
session_start();
$user = getadmin($_SESSION["sess_id"]);
if (isset($_GET['logOutadmin'])) {
  logOutadmin();
}
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="../../backend/dist/img/logo/logosci.png" alt="SciTech" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SciTech VRU</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <?php if(isset($_SESSION['sess_adminid'])) { ?>
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../backend/img_resize/<?php echo $_SESSION['sess_adminimg']; ?>"class="rounded-top" alt="<?php echo $_SESSION['sess_adminfnamet']; ?> <?php echo $_SESSION['sess_adminlnamet']; ?>">
        </div>
        <div class="info">
          <a class="d-block"><?php echo $_SESSION['sess_adminfnamet']; ?> <?php echo $_SESSION['sess_adminlnamet']; ?></a>
        </div>
        <?php } ?>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-address-card"></i>
              <p>
                รายงานการประชุม
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="reportex.php" class="nav-link active">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>มติการประชุม</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="report.php" class="nav-link active">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>วาระการประชุม</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item">
            <a href="?logOutadmin=true" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>ออกจากระบบ
                <span class="right badge badge-danger">ผู้ใช้งาน</span>
              </p>
            </a>
          </li>
            
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>