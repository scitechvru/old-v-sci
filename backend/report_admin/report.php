<?php include "h.php";
include "navbar.php";
require "../../inc/connect.php";
require "../array.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>วาระการประชุม</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="admin_meeting.php" >
                            <i class="fas fa-plus"></i> เพิ่มวาระประชุม
                        </a>
                        
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">วาระการประชุม</h3>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="text-center">ลำดับ</th>
                                        <th class="text-center">วาระการประชุม</th>
                                        <th class="text-center">ครั้ง</th>
                                        <th class="text-center">วันที่</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php 
                                    $no=0;
                                   
                          $sql = "select * from report_title order by id_title DESC ";
                          $result = mysql_db_query($dbname, $sql);
                          $num = mysql_num_rows($result);
                          if ($num > 0) {
                              while ($r = mysql_fetch_array($result)) {
                                  $id_title = $r[id_title];
                                  $report_time = $r[report_time];
                                  $year = $r[year];
                                  $location = $r[location];
                                  $report_date = $r[report_date];
                                  $report_type = $r[report_type];
                                  $Date = explode("-", $report_date );
                                        $date_report = $Date[2] . "-" . $Date[1] . "-" . $Date[0];
                                  $no++;
                                    ?>
                                        <td class="text-center"><?php echo "$no";?></td>
                                        <td class="text-center"><?php echo "$report[$report_type]";?></td>
                                        <td class="text-center"><?php echo "$report_time";?></td>
                                        <td class="text-center"><?php echo "$date_report";?></td>
                                        <td class="text-center"><a class="btn btn-app" href="add_report.php?id=<?php echo "$id_title";?>">
                                                <i class="fas fa-plus"></i>เพิ่มหัวข้อ</a>
                                                <a class="btn btn-app" href="veiw.php?id=<?php echo "$id_title";?>">
                                                <i class="fas fa-eye"></i>ดู</a></td>
                                    </tr>
                              <?php
                            }
                            }?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?include 'f.php';?>