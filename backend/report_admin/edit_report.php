<?php
session_start();
include "h.php";
include "navbar.php";
require "../../inc/connect.php";
$id=$_GET['id'];
$sql = "SELECT * from report_detail where id_detail = '$id' ";
    $result = mysql_db_query($dbname, $sql);
      $r8 = mysql_fetch_array($result);
      $id_detail=$r8[id_detail]; 
      $id_report=$r8[id_report];
      $title=$r8[title];
      $name_title=$r8[name_title];
      $file=$r8[file];
      $link=$r8[link];

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>วาระการประชุม</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="index.php">
                            <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าหลัก
                        </a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">


                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">แก้ไขหัวข้อวาระการประชุม</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form  action="report_edit.php?id=<?php echo "$id";?>" method="post" enctype="multipart/form-data" role="form">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- select -->
                                            <div class="form-group">
                                                <label>โปรดเลือกหัวข้อ</label>
                                                <select  name="title" class="form-control">
                                                <option>โปรดเลือกหัวข้อ</option>
                                                    <option value="1">ระเบียบที่ ๑ เรื่องประธานแจ้งให้ทราบ</option>
                                                    <option value="2">ระเบียบที่ ๒ เรื่องรับรองรายงานการประชุม</option>
                                                    <option value="3">ระเบียบที่ ๓ เรื่องสืบเนือง</option>
                                                    <option value="4">ระเบียบที่ ๔ เรื่องเสนอเพื่อทราบ</option>
                                                    <option value="5">ระเบียบที่ ๕ เรื่องเสนอเพื่อพิจารณา</option>
                                                    <option value="6">ระเบียบที่ ๖ เรื่องอื่นๆ</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>หัวข้อ</label>
                                                <input type="text"  name="name_title" class="form-control" value="<?php echo "$name_title";?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input"  name="file" id="file" onchange="return fileValidation()"  >
                                                <label class="custom-file-label" for="customFile">เพิ่มไฟล์</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>หรือ</label>
                                                <input type="text"  name="link" class="form-control" value="<?php echo "$link";?>">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">บันทึก</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include "f.php";?>
<script>
    function fileValidation() {
        var fileInput = document.getElementById('file');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.docx|\.doc|\.pdf)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('รองรับแค่ไฟล์นามสกุล .doc .docx .pdf เท่านั้น.');
            fileInput.value = '';
            return false;
        }
        }
   
    </script>