<?php include "h.php";
include "navbar.php";
require "../../inc/connect.php";
require "../array.php";
$id=$_GET[id];
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>วาระการประชุม</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="report.php" >
                        <i class="fas fa-arrow-alt-circle-left"></i>ย้อนกลับ
                        </a>
                        
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">วาระการประชุม</h3>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="text-center">ลำดับ</th>
                                        <th class="text-center">หัวข้อ</th>
                                        <th class="text-center">วาระ</th>
                                   
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php 
                                    $no=0;
                                    $id=$_GET[id];
                                    $sql1 = "select * from report_detail where id_report = '$id'  order by id_detail DESC ";
                                    $result1 = mysql_db_query($dbname, $sql1);
                                    $num1 = mysql_num_rows($result1);
                                    if ($num1 > 0) {
                                        while ($r1 = mysql_fetch_array($result1)) {
                                            $id_detail = $r1[id_detail];
                                            $id_report = $r1[id_report];
                                            $title = $r1[title];
                                            $name_title = $r1[name_title];
                                            $file = $r1[file];
                                            $link = $r1[link];
                                            $no++;
                                    ?>
                                        <td class="text-center"><?php echo "$no";?></td>
                                        <td class="text-center"><?php echo "$title";?></td>
                                        <td class="text-center"><?php echo "$name_title";?></td>
                                        <td class="text-center">
                                                <a class="btn btn-app" href="edit_report.php?id=<?php echo "$id_detail";?>">
                                                <i class="fas fa-edit"></i>แก้ไข</a>
                                                <a class="btn btn-app" href="del_report.php?id=<?php echo "$id_detail";?>">
                                                <i class="fas fa-trash-alt"></i>ลบ</a>
                                                
                                    </tr>
                              <?php
                            }
                            }?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?include 'f.php';?>