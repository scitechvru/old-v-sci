<?php include 'h.php';?>
<?php include "chksession_admin.php";?>
<!-- MENU SIDEBAR-->
<?php include 'navbar.php';?>
<!-- END MENU SIDEBAR-->

<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <strong>เพิ่มคำถามที่พบบ่อย</strong>
                            </div>
                            <div class="card-body card-block">
                                <form action="faq_save.php" method="post" enctype="multipart/form-data"
                                    class="form-horizontal">
                                    
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_sub" class=" form-control-label">คำถาม</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="question" name="question" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_location" class=" form-control-label">คำตอบ</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="answer" name="answer" class="form-control">

                                        </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">ประเภทคำถาม</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <select name="select" id="select" class="form-control">
                                                        <option value="0">โปรดเลือกประเภทคำถาม</option>
                                                        <option value="การศึกษา">การศึกษา</option>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        
                                    </div>
                            </div>
                            </form>
                        </div>


                    </div>
                    <!-- END MAIN CONTENT-->
                    <!-- END PAGE CONTAINER-->
                </div>
            </div>
        </div>
        <?php include 'f.php';?>