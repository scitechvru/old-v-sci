<?php
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>เพิ่มข้อมูลแบบฟอร์ม</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="index.php">
                            <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าหลัก
                        </a>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!-- right column -->
                <div class="col-md-6">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มข้อมูลแบบฟอร์ม</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form METHOD=POST ACTION="admin_files2.php" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อไฟล์</label>
                                            <input type="text" class="form-control" id="titlefile" name="titlefile"
                                                placeholder="Enter ..." required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Link</label>
                                            <input type="text" class="form-control" id="linkfile" name="linkfile"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            <label>ประเภทแบบฟอร์ม</label>
                                            <select class="form-control"id="typefile" name="typefile">
                                            <option value="">โปรดระบุประเภทแบบฟอร์ม</option>
                                                <option value="stu"> แบบฟอร์มนักศึกษา</option>
                                                <option value="man"> คู่มือนักศึกษา</option>
                                                <option value="per">แบบฟอร์มอาจารย์/เจ้าหน้าที่</option>
                                                <option value="res"> แบบฟอร์มวิจัย</option>
                                                <option value="info"> สารสนเทศ</option>
                                                <option  value="reg">ระเบียบ/ข้อบังคับ/กฎหมายที่เกี่ยวข้อง</option>
                                                <option value="qua">ประกันคุณภาพ</option>
                                                <option  value="mua">คู่มือ</option>
                                                <option  value="jr1">ระเบียบการตีพิมพ์ JRIST</option>
                                                <option  value="jr2">รูปแบบบทความวิชาการ JRIST</option>
                                                <option  value="jr3">รูปแบบบทความวิจัย JRIST</option>
                                                <option  value="jr4">ใบนำส่งบทความฯ JRIST</option>

                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="namefile" name="namefile"
                                                    required="required" onchange="return fileValidation()"
                                                    name="file_news">
                                                <label class="custom-file-label" for="customFile">ไฟล์แบบฟอร์ม</label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">บันทึก</button>

                                </div>

                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
        <?php include 'f.php';?>

        <script>
        function fileValidation() {
            var fileInput = document.getElementById('namefile');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.doc|\.docx|\.pdf|\.xls|\.xlsx)$/i;
            if (!allowedExtensions.exec(filePath)) {
                alert('รองรับแค่ไฟล์นามสกุล .pdf .doc .docx .xls .xlsx เท่านั้น.');
                fileInput.value = '';
                return false;

            }
        }
        </script>
        <!--
        <div class="row">
                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            <label>ประเภทข่าว</label>
                                            <select class="form-control" id="type_news" name="type_news">
                                            <option value="">โปรดระบุประเภทข่าว</option>
                                                <option value="PR"> ประชาสัมพันธ์</option>
                                                <option value="ED"> ศึกษาต่อ</option>
                                                <option value="RE"> รับสมัครงาน</option>
                                                <option value="AL"> ศิษย์เก่า</option>
                                                <option value="PRM">ประชาสัมพันธ์หลักสูตร</option>
                                                <option  value="ITA"> Work@Home</option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">

                                            <div class="form-group">
                                                <label>สังกัด</label>
                                                <select name="Major_news" id="Major_news" class="form-control"
                                                    width="30">
                                                    <option value="">โปรดระบุสังกัด</option>
                                                    <?php /* if($sess_major== 'OF' ){
                                                      include "connect.php";
                                                      $sql = "select * from tb_major order by id_major";
                                                      $result = mysql_db_query($dbname, $sql);
                                                      $qr = mysql_query($sql);
                                                      while ($result = mysql_fetch_array($qr)) {
                                                          $id_major = $result['id_major'];
                                                          $nameth_major = $result['nameth_major'];
                                                          $int_major = $result['int_major'];

                                                          ?>
                                                    <option value="<?php echo "$int_major"; ?>">
                                                        <?php echo "$nameth_major"; ?>
                                                    </option>
                                                    <?php } }
                                                      else {
                                                        include "connect.php";
                                                        $sql = "select * from tb_major  WHERE int_major='$sess_major'";
                                                        $result = mysql_db_query($dbname, $sql);
                                                        $qr = mysql_query($sql);
                                                        while ($result = mysql_fetch_array($qr)) {
                                                            $id_major = $result['id_major'];
                                                            $nameth_major = $result['nameth_major'];
                                                            $int_major = $result['int_major'];
                                                            ?>
                                                    <option value="<?php echo "$int_major"; ?>">
                                                        <?php echo "$nameth_major"; ?>
                                                    </option>
                                                    <?php } }*/?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">

                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"id="file_news" name="file_news">
                                                    <label class="custom-file-label"
                                                        for="customFile">ไฟล์ข่าวประชาสัมพันธ์</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

-->
