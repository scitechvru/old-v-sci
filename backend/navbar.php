<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <?php if($_SESSION["sess_adminmajor"] == "OF"){ ?>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index.php" class="nav-link">หน้าหลัก</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="admin_banner.php" class="nav-link">จัดการแบนเนอร์</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="admin_image.php" class="nav-link">จัดการกิจกรรม</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="admin_news.php" class="nav-link">จัดการข่าวประชาสัมพันธ์</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="contect_t_admin.php" class="nav-link">จัดการช่องทางติดต่ออาจารย์</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="admin_files.php" class="nav-link">จัดการข้อมูลแบบฟอร์ม</a>
      </li>
      <?php }else{ ?>
        <li class="nav-item d-none d-sm-inline-block">
        <a href="admin_banner.php" class="nav-link">จัดการแบนเนอร์</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="admin_image.php" class="nav-link">จัดการกิจกรรม</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="admin_news.php" class="nav-link">จัดการข่าวประชาสัมพันธ์</a>
      </li>
      <?php }?>
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/logo/logologin.png" alt="SciTech" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SciTech VRU</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <?php if(isset($_SESSION['sess_adminid'])) { ?>
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="./img_resize/<?php echo $_SESSION['sess_adminimg']; ?>"class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION['sess_adminfname']; ?> <?php echo $_SESSION['sess_adminlanme']; ?></a>
        </div>
        <?php } ?>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <?php if($_SESSION["sess_adminmajor"] == "OF"){ ?>
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item has-treeview menu-open">
            <a href="index.php" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashbord
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <!--<ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="index.php" class="nav-link active">
                  <i class="fas fa-chart-bar nav-icon"></i>
                  <p>สรุปข้อมูล</p>
                </a>
              </li>
            </ul>-->
          </li>
          <li class="nav-item has-treeview">
            <a href="official.php" class="nav-link ">
              <i class="nav-icon fas fa-rss"></i>
              <p>
                สื่อเผยแพร่
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="book.php" class="nav-link active">
                  <i class="fas fa-journal-whills nav-icon"></i>
                  <p>หนังสือ</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="video.php" class="nav-link active">
                  <i class="fab fa-youtube nav-icon"></i>
                  <p>วิดีโอ</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview ">
            <a href="admin_banner.php" class="nav-link ">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
                จัดการแบนเนอร์
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_banner.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มแบนเนอร์</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview ">
            <a href="admin_image.php" class="nav-link">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
                จัดการกิจกรรม
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_image.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มกิจกรรม</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview ">
            <a href="admin_news.php" class="nav-link ">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                จัดการข่าวประชาสัมพันธ์
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_news.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มข่าวประชาสัมพันธ์</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview ">
            <a href="admin_major.php" class="nav-link ">
              <i class="nav-icon fas fa-building"></i>
              <p>
                จัดการข้อมูลหน่วยงาน
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_major.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มหน่วยงาน</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview ">
            <a href="admin_per.php" class="nav-link ">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                จัดการข้อมูลบุคคลากร
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_per.php" class="nav-link ">
                  <i class="fas fa-user-plus nav-icon"></i>
                  <p>เพิ่มข้อมูลบุคลากร</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="ita.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                จัดการข้อมูล ITA
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="ita.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มข้อมูล ITA</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="ita_detail.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>เพิ่มรายละเอียด ข้อมูล ITA</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="contect_t_admin.php" class="nav-link">
              <i class="nav-icon fas fa-address-card"></i>
              <p>
                จัดการช่องติดต่ออาจารย์
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="contect_t_admin.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มข้อมูลการติดต่ออาจารย์</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="admin_files.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                จัดการข้อมูลแบบฟอร์ม
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_files.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มข้อมูลแบบฟอร์ม</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="admin_report.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                จัดการข้อมูลวาระการประชุม
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_reportex.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มข้อมูลวาระการประชุม</p>
                </a>
                <a href="admin_report.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มข้อมูลมติการประชุม</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="admin_admins.php" class="nav-link">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                จัดการข้อมูลAdmin
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_admins.php" class="nav-link ">
                  <i class="fas fa-user-plus nav-icon"></i>
                  <p>เพิ่มข้อมูลAdmin</p>
                </a>
              </li>
            </ul>
          </li>
          <?php if($_SESSION[sess_admintype] =="1" or $_SESSION[sess_admintype] == "2"){  ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                ระบบประเมินพฤติกรรมสมรรถนะ
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pa4.php" class="nav-link ">
                  <i class="fas fa-user-plus nav-icon"></i>
                  <p>ตรวจสอบผลประเมินพฤติกรรมสมรรถนะ</p>
                </a>
              </li>
            </ul>
          </li>
          <?php }?>
          <li class="nav-item">
            <a href="logout.php" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>ออกจากระบบ
                <span class="right badge badge-danger">admin</span>
              </p>
            </a>
          </li>
          <?php }else{ ?>
            <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashbord
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link active">
                  <i class="fas fa-chart-bar nav-icon"></i>
                  <p>สรุปข้อมูล</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="official.php" class="nav-link ">
              <i class="nav-icon fas fa-rss"></i>
              <p>
                สื่อเผยแพร่
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="book.php" class="nav-link active">
                  <i class="fas fa-journal-whills nav-icon"></i>
                  <p>หนังสือ</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="video.php" class="nav-link active">
                  <i class="fab fa-youtube nav-icon"></i>
                  <p>วิดีโอ</p>
                </a>
              </li>
            </ul>
          </li>
            <li class="nav-item has-treeview ">
            <a href="admin_banner.php" class="nav-link ">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
                จัดการแบนเนอร์
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_banner1.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มแบนเนอร์</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview ">
            <a href="admin_image.php" class="nav-link">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
                จัดการกิจกรรม
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_image.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มกิจกรรม</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview ">
            <a href="admin_news.php" class="nav-link ">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                จัดการข่าวประชาสัมพันธ์
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_news.php" class="nav-link ">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>เพิ่มข่าวประชาสัมพันธ์</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="logout.php" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>ออกจากระบบ
                <span class="right badge badge-danger">admin</span>
              </p>
            </a>
          </li>
            <?php } ?>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>