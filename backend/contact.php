<?php include 'h.php';?>
<?php include "chksession_admin.php"; ?>
<!-- MENU SIDEBAR-->
<?php include 'navbar.php';?>
<!-- END MENU SIDEBAR-->

<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1">จัดการข้อมูลการติดต่อ</h2><A HREF="admin_contact.php">
                            <button class="au-btn au-btn-icon au-btn--blue">
                                <i class="zmdi zmdi-plus"></i>เพิ่มข้อมูลการติดต่อ</button>
                        </div>
                    </div>
                </div>
                <br><br>
                <!-- DATA TABLE -->

                <div class="table-responsive table-responsive-data2">
                    <table class="table table-data2">
                        <thead>
                            <tr>
                                <th class="text-center">ลำดับ</th>
                                <th class="text-center" colspan="2">ชื่อ</th>
                                <th class="text-center" colspan="2">ที่อยู่</th>
                                <th class="text-center">เบอร์ติดต่อ</th>

                                <th class="text-center">E-mail</th>
                                <th class="text-center">FB</th>
                                <!-- <th>สถานะ</th>-->
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                            $no = 0;
                                            include "connect.php";
                                            $sql = "select * from contact order by c_id DESC";
                            $result = mysql_db_query($dbname, $sql);
                            $num = mysql_num_rows($result);
                            if ($num > 0) {
                                while ($r = mysql_fetch_array($result)) {
                                    $c_id = $r[c_id];
                                    $c_sub = $r[c_sub];
                                    $c_location = $r[c_location];
                                    $c_tel1 = $r[c_tel1];
                                    $c_tel2 = $r[c_tel2];
                                    $c_fax = $r[c_fax];
                                    $c_email = $r[c_email];
                                    $c_major = $r[c_major];
                                    $c_fb = $r[c_fb];
                                    $c_status = $r[c_status];
                                    $no++;
                                            ?>
                            <tr class="tr-shadow">
                                <td class="text-center"><?php echo "$no";?></td>
                                <td class="text-center" colspan="2"><?php echo "$c_sub";?></td>
                                <td  class="text-center" colspan="2"><?php echo "$c_location";?></td>
                                <td class="text-center"><div class="table-data-feature"><button class="item" data-toggle="tooltip" data-placement="top" title="<?php echo "$c_tel1";?>">
                                            <i class="zmdi zmdi-phone"></i>
                                        </button><button class="item" data-toggle="tooltip" data-placement="top" title="<?php echo "$c_tel2";?>">
                                            <i class="zmdi zmdi-phone"></i>
                                        </button><button class="item" data-toggle="tooltip" data-placement="top" title="<?php echo "$c_fax";?>">
                                            <i class="zmdi zmdi-phone"></i>
                                        </button></td>

                             <td class="text-center"><div class="table-data-feature"><button class="item" data-toggle="tooltip" data-placement="top" title="<?php echo "$c_email";?>">
                                            <i class="zmdi zmdi-email"></i>
                                        </button></div>
                                </td>
                                <td class="text-center"><div class="table-data-feature"><button class="item" data-toggle="tooltip" data-placement="top" title="<?php echo "$c_fb";?>">
                                            <i class="zmdi zmdi-facebook-box"></i>
                                        </button></div></td>
                                <td>
                                    <div class="table-data-feature">
                                       
                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <?php }
                                }                           
                            ?>

                        </tbody>
                    </table>
                </div>
                <!-- END DATA TABLE -->
            </div>



        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->
<!-- END PAGE CONTAINER-->
</div>

</div>

<?php include 'f.php';?>