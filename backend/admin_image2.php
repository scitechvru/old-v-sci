<?php include "chksession_admin.php";?>
<?php
date_default_timezone_set("Asia/Bangkok");

function uppic_only($img, $index, $imglocate, $limit_size = 9000000000, $limit_width = 0, $limit_height = 0)
{
    $allowed_types = array("JPG", "JPEG", "jpg", "jpeg", "gif", "png");
    if ($img["name"][$index] != "") {
        $fileupload1 = $img["tmp_name"][$index];
        $data_Img = @getimagesize($fileupload1);
        $g_img = explode(".", $img["name"][$index]);
        $ext = strtolower(array_pop($g_img));
        $file_up = time() . "_" . $index . "." . $ext;
        $canUpload = 0;
        if (isset($data_Img) && $data_Img[0] > 0 && $data_Img[1] > 0) {
            if ($img["size"][$index] <= $limit_size) {
                if ($limit_width > 0 && $limit_height > 0) {
                    if ($data_Img[0] <= $limit_width && $data_Img[1] <= $limit_height) {
                        $canUpload = 1;
                    }
                } elseif ($limit_width > 0 && $limit_height == 0) {
                    if ($data_Img[0] <= $limit_width) {
                        $canUpload = 1;
                    }
                } elseif ($limit_width == 0 && $limit_height > 0) {
                    if ($data_Img[1] <= $limit_height) {
                        $canUpload = 1;
                    }
                } else {
                    $canUpload = 1;
                }
            } else {

            }
        }
        if ($fileupload1 != "" && @in_array($ext, $allowed_types) && $canUpload == 1) {
            @copy($fileupload1, $imglocate . $file_up);
            //    @chmod($imglocate.$file_up,0777);
        } else {
            $file_up = "";
        }
    }
    return $file_up; // ส่งกลับชื่อไฟล์
    //return $Title_image;
}
//////  END UPPIC FUNCTION

$Title_image = $_POST['Title_image'];
$Major_image = $_POST['Major_image'];
$place_image = $_POST['place_image'];
$detail_img =$_POST['detail_img'];
$pic_type =$_POST['pic_type'];
$date_image = $_POST['date_image'];
$Date = explode("/", $date_image);
$date_image = $Date[2] . $Date[1] . $Date[0];

$picco_image = $_FILES[picco_image][tmp_name];
$picco_image_name = $_FILES[picco_image][name];
echo "Cover: $picco_image_name<BR>";

if ($Title_image = '') {
    echo "<h3>ERROR : กรุณาป้อนชื่อกิจกรรมด้วยครับ </h3>";
    exit();
}
include "connect.php";
$sql = "select * from tb_picup where title_img='$Title_image' ";
$result = mysql_db_query($dbname, $sql);
$num = mysql_num_rows($result);
if ($num) {
    echo "<h3><CENTER>ERROR : มีข้อมูลซ้ำกับในระบบครับ </h3></CENTER>";exit();
} //end if

if ($picco_image) {
    $array_last = explode(".", $picco_image_name);
    $c = count($array_last) - 1;
    $lastname = strtolower($array_last[$c]);
    if ($lastname == "jpg" or $lastname == "jpeg" or $lastname == "png") {
        $picco_image_name1 = time() . "_" . 'Cover_.' . $lastname;
        copy($picco_image, "pic_upload/" . $picco_image_name1);
    } else {
        echo "<h3>ERROR : ไม่สามารถ Upload Image Cover ได้ครับ </h3>";exit();
    } //end if
    unlink($picco_image);
} //end if

include "connect.php";

$num_pic = count($_FILES['file_upload']['name']); // นับว่ามีการส่งรูปภาพมาเพื่ออัพหรือไม่
echo "num: $num_pic<BR>";
if ($num_pic > 0 && $_FILES['file_upload']['name'][0] != "") { // ถ้ามีอย่างน้อยหนึ่งรูป
    for ($i = 0; $i < $num_pic; $i++) { // วนลูปอัพรูป
        $upfile_name = uppic_only($_FILES['file_upload'], $i, "pic_upload/"); // อัพรูปไปไว้ที่โหลเดอร์ uppic
        $Title_image = $_POST['Title_image'];
        echo "$Title_image<BR>$Major_image<BR>$place_image<BR>$date_image<BR>$picco_image_name1<BR>$pic_type<BR>";
        echo "$upfile_name.<br>"; // จะได้ชื่อไฟล์รูป ที่อัพเสร็จแล้วมาใช้งาน
        // วนลูปคำสั่งบันทึกลงฐานข้อมูลหรืออื่นๆ ตามต้องการ
        $sql = "insert into tb_picup values('','$Title_image','$detail_img','$Major_image','$date_image','$place_image','$picco_image_name1','$upfile_name','$pic_type','201')";
        $result = mysql_db_query($dbname, $sql);
        if (!$result) {
            echo "<h3>ไม่สามารถบันทึกข้อมูลได้ครับ</h3>";
        } //endIf

    } //endFor
    $name = $_SESSION[sess_adminuser];
    $major = $_SESSION[sess_adminmajor];
    $status = "Admin_" . "$major" . "_AddPicUp_" . "$Title_image" . "  \r\n";
    include "savelog.php";
    savelog($name, $status);
    echo "<script type='text/javascript'>window.location.href = 'admin_image.php';</script> ";
} //endIf
