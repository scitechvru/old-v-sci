<?php include 'h.php';?>
<?php include "chksession_admin.php"; ?>
<!-- MENU SIDEBAR-->
<?php include 'navbar.php';?>
<!-- END MENU SIDEBAR-->

<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1">จัดการข้อมูลคำถามQ&A</h2><A HREF="faq1.php">
                            <button class="au-btn au-btn-icon au-btn--blue">
                                <i class="zmdi zmdi-plus"></i>เพิ่มคำถามที่พบบ่อย</button>
                        </div>
                    </div>
                </div>
                <br><br>
                <!--<div class="card">
                    <div class="card-header">
                        <h4>จัดการข้อมูลคำถาม</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">Q&A</a>
                            </li>
                            <li>
                                <a href="#">คำถามที่พบบ่อย</a>
                            </li>
                            <li>
                                <a href="#">ข้อเสนอแนะ</a>
                            </li>
                            <li>
                                <a href="#">ข้อร้องเรียน</a>
                            </li>
                        </ul>

                    </div>
                </div>-->
                <div class="row m-t-30">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table class="table table-borderless table-data3">
                                <thead>
                                    <tr>
                                        <th class="text-center">ลำดับ</th>
                                        <th class="text-center">ประเภทคำถาม</th>
                                        <th class="text-center">วันที่ถาม</th>
                                        <th class="text-center">สถานะ</th>
                                        <th class="text-center" colspan="2">คำถาม</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php 
                    include "connect.php";
                    include "../ITA/array.php";
                    $no = 0;
                    $sql = "SELECT * FROM q qu ORDER by qu.q_date DESC";
                    $result = mysql_db_query($dbname, $sql);
                    $num = mysql_num_rows($result);
                    if ($num > 0) {
                        while ($r = mysql_fetch_array($result)) {
                            $id =$r[q_id];
                            $title =$r[q_title];
                            $name =$r[q_name];
                            $detail =$r[q_detail];
                            $email =$r[q_email];
                            $tel =$r[q_tel];
                            $date =$r[q_date];
                            $status =$r[q_status];
                            $status2 =$r[q_show];
                            $type =$r[q_type_p];
                            $type2 =$r[q_type];
                            $admin =$r[a_name];
                            $a_detail =$r[a_detail];
                            $a_date =$r[a_date];
                            $Date = explode("-", $date);
                            $date_q = $Date[2] . "/" . $thaimonth1[$Date[1]] . "/" . ($Date[0]+543);
                            $Date = explode("-", $a_date);
                            $date_a = $Date[2] . "/" . $thaimonth1[$Date[1]] . "/" . ($Date[0]+543);
                            $no++
                    ?>
                                        <td class="text-center"><?php echo "$no";?></td>
                                        <td class="text-center"><?php echo "$type1[$type2]";?></td>
                                        <td class="text-center"><?php echo "$date_q";?></td>
                                        <td class="text-center"><?php echo "$status1[$status]";?></td>
                                        <td class="text-center" colspan="2"><?php echo "$title".">>"."$detail";?></td>


                                        <td class="text-center">
                                            <div class="table-data-feature">
                                            <?php if ($type2==1){?>
                                                <?php if($status==1){?>
                                                <button class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                    data-placement="top" title="อ่านคำถามทั้งหมด"
                                                    onclick="window.location.href='view_question.php?q_id=<?php echo $id ?>'">
                                                    อ่านคำถาม
                                                </button>
                                                <?php }?>
                                                <?php if($status==2){?>ุ
                                                <button class="btn btn-sm btn-success " data-toggle="tooltip"
                                                    data-placement="top" title="ตอบคำถาม"
                                                    onclick="window.location.href='answer.php?q_id=<?php echo $id ?>'">
                                                    ตอบคำถาม
                                                </button>&nbsp;
                                                

                                                <?php }?>
                                                <?php if ($status2 == 1 || 2) {?>
                                                <?php if($status2 == 1){?>
                                                    <button class="btn btn-sm btn-success " data-toggle="tooltip"
                                                        data-placement="top" title="อนุมัติให้แสดงหน้าบ้าน">
                                                        แสดง
                                                    </button>
                                                <?php } elseif($status2==2) {?>&nbsp;
                                                    <button class="btn btn-sm btn-danger " data-toggle="tooltip"
                                                        data-placement="top" title="ไม่อนุมัติการแสดงหน้าบ้าน">
                                                        ไม่แสดง
                                                    </button>
                                                <?php }else{?>
                                                <?php if($status == 3){?>
                                                <button class="btn btn-sm btn-success " data-toggle="tooltip"
                                                    data-placement="top" title="อนุมัติให้แสดงหน้าบ้าน"
                                                    onclick="window.location.href='Q&A.php?q_id=<?php echo $id ?>&ap_status=1'">
                                                    อนุมัติ
                                                </button>&nbsp;
                                                <button class="btn btn-sm btn-danger " data-toggle="tooltip"
                                                    data-placement="top" title="ไม่อนุมัติการแสดงหน้าบ้าน"
                                                    onclick="window.location.href='Q&A.php?q_id=<?php echo $id ?>&ap_status=2'">
                                                    ไม่อนุมัติ
                                                </button>
                                                <?php }?>
                                                <?php }?>
                                                <?php }?>
                                                <?php } elseif($type2== 3 || 4){?>
                                                <?php if($type2==3){?>
                                                    <button class="btn btn-sm btn-success " data-toggle="tooltip"
                                                    data-placement="top" title="อนุมัติให้แสดงหน้าบ้าน"
                                                    onclick="window.location.href='check.php?q_id=<?php echo $id ?>&ap_status=4'">
                                                    รับทราบข้อเสนอแนะ
                                                </button>
                                               
                                                <?} elseif($type2==4){?>
                                                    <button class="btn btn-sm btn-success " data-toggle="tooltip"
                                                    data-placement="top" title="อนุมัติให้แสดงหน้าบ้าน"
                                                    onclick="window.location.href='check.php?q_id=<?php echo $id ?>&ap_status=5'">
                                                    รับทราบข้อร้องเรียน
                                                </button>
                                                
                                                <?php }?>
                                                <?php }?>
                                            </div>
                                    </tr>
                                    <?php 
                        }
                        }
                        ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE-->
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
</div>

</div>

<?php include 'f.php';?>