<?php 
session_start();
include "h.php";
include "navbar.php";
include "chksession_admin.php";
require "connect.php";

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการข้อมูลรายละเอียด ITA</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">

                        <a class="btn btn-app" HREF="ita1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลรายละเอียด ITA
                        </a>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">ข้อมูลรายละเอียด ITA</h3>

                        </div>
                        <!-- /.card-header -->

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="text-center">ลำดับ</th>
                                        <th colspan="2" class="text-center" scope="col">หัวข้อ ITA</th>
                                        <th colspan="2" class="text-center" scope="col">ชื่อไฟล์</th>
                                        <th colspan="2" class="text-center" scope="col"> ไฟล์</th>
                                        <th scope="col" class="text-center">จัดการ</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php
                                $no=0;
                                $sql = "SELECT * FROM ita_detail id INNER JOIN ita_head ih ON id.id_ita=ih.id_ita WHERE id.id_ita=ih.id_ita ORDER BY id.id_ita ASC ";
                                $result = mysql_db_query($dbname, $sql);
                                $num = mysql_num_rows($result);
                                if ($num > 0) {
                                    while ($r = mysql_fetch_array($result)) {
                                        $id_detail = $r[ita_detail];
                                        $id_ita = $r[id_ita];
                                        $sub_ita = $r[sub_ita];
                                        $detail = $r[detail];
                                        $name = $r[name];
                                        $file = $r[file];
                                        $link = $r[link];
                                        $no++
                               ?>
                                        <td class="text-center"><?php echo "O$no";?></td>
                                        <td class="text-center" colspan="2"><?php echo "$sub_ita";?></td>
                                        <td class="text-center" colspan="2"><?php echo "$name ";?></td>
                                        <td class="text-center" colspan="2">
                                            <?php
                                             if(!$link){?>
                                            <li><a href="<?php echo "$link";?>"><?php echo "$name";?></a></li>

                                            <?php }else{?>

                                            <li><a href="../ita/file_ita/<?php echo "$file";?>"><?php echo "$name";?></a></li>
                                            <?php }?>
                                        </td>
                                        <td class="text-center">
                                            <a class="btn btn-app" HREF="edit_ita_detail.php?id_edit=<?php echo "$id_detail"; ?>">
                                                <i class="fas fa-edit"></i> แก้ไขข้อมูล
                                            </a><a class="btn btn-app"
                                                HREF="delete_ita_detail.php?id_delete=<?php echo "$id_detail"; ?>">
                                                <i class="fas fa-trash-alt"></i> ลบข้อมูล
                                            </a>
                                        </td>
                                    </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include "f.php";?>