<?php include 'h.php';?>
<?php include "chksession_admin.php";?>
 <!-- MENU SIDEBAR-->
 <?php include 'navbar.php';?>
 <!-- END MENU SIDEBAR-->

<!-- PAGE CONTAINER-->
        <div class="page-container">
           <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Mangement</h2>
                                    <button class="au-btn au-btn-icon au-btn--blue">
                                        <i class="zmdi zmdi-plus"></i>add item</button>
                                </div>
                               <br><br>
                            </div>
                            <div class="row">
<!--
    <div class="col-sm-3">
      <p>อาจารย์</p>
		<a href="#"><div align="center"><img src="images/logo_teacher.png" class="img-responsive" style="width:150" alt="Image"></div></a>
	</div>
-->
	<div class="col-sm-3">
      <p>หน่วยงาน</p>
      <a href="admin_major.php"><div align="center"><img src="images/logo_major.png" class="img-responsive" style="width:150" alt="Image"></div></a>
    </div>
	<div class="col-sm-3">
      <p>บุคลากรเจ้าหน้าที่</p>
      <a href="admin_per.php"><div align="center"><img src="images/logo_staff.png" class="img-responsive" style="width:150" alt="Image"></div></a>
    </div>
    <div class="col-sm-3">
      <p>คู่มือ แบบฟอร์ม </p>
      <a href="admin_files.php"><div align="center"><img src="images/logo_download.png" class="img-responsive" style="width:150" alt="Image"></div></a>
    </div>
    <div class="col-sm-3">
      <p>วาระการประชุม</p>
      <a href="admin_report.php"><div align="center"><img src="images/logo_meeting.png" class="img-responsive" style="width:150" alt="Image"></div></a>
    </div>
  </div>
</div><br><br><br>
<div class="container-fluid bg-3 text-center">
  <div class="row">
	<div class="col-sm-3">
      <p>Banner</p>
      <a href="admin_banner.php"><div align="center"><img src="images/logo_banner.png" class="img-responsive" style="width:200" alt="Image"></div></a>
    </div>    <div class="col-sm-3">
      <p>ภาพกิจกรรม</p>
      <a href="admin_image.php"><div align="center"><img src="images/logo_activity.png" class="img-responsive" style="width:150"  alt="Image"></div></a>
    </div>
    <div class="col-sm-3">
      <p>News</p>
      <a href="admin_news.php"><div align="center"><img src="images/logo_news.png" class="img-responsive" style="width:180" alt="Image"></div></a>
    </div>
    <div class="col-sm-3">
      <p>Admin</p>
      <a href="admin_admins.php"><div align="center"><img src="images/logo_admin.png" class="img-responsive" style="width:180" alt="Image"></div></a>
    </div>
  </div>
</div><br><br>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

<?php include 'f.php';?>
