<?php
session_start();
include "h.php";
include "navbar.php";
include "chksession_admin.php";
include "connect.php";
$pic_type1=array(
    "0"=>"กิจกรรมทั่วไป",
    "1"=>"กิจกรรมด้านศิลปวัฒนธรรม",
);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการกิจกรรม</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="admin_image1.php">
                            <i class="fas fa-plus"></i> เพิ่มรูปกิจกรรม
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-2"></div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">ข้อมูลการส่งบทความ</h3>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">ลำดับ</th>
                                <th class="text-center">หัวข้อกิจกรรม</th>
                                <th class="text-center">หลักสูตร</th>
                                <th class="text-center">ประเภทกิจกรรม</th>
                                <th class="text-center">วันที่</th>
                                <th class="text-center">สถานที่</th>
                                <th class="text-center">ดูรูป</th>
                                <th class="text-center">จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <?php if($sess_major== 'OF' ){
                            $no = 0;
                            $sql = "SELECT * from tb_picup order by id_img DESC ";
                            $result = mysql_db_query($dbname, $sql);
                            $num = mysql_num_rows($result);
                            if ($num > 0) {
                                while ($r = mysql_fetch_array($result)) {
                                    if ($title_img1 != $r[title_img]) {
                                        if (($no % 2) == 0) {$row_color = "#D8D8D8";} else { $row_color = "#FFFFFF";}
                                        $title_img1 = $r[title_img];
                                        $pic_type = $r[pic_type];
                                        $date_img = $r[date_img];
                                        $place_img = $r[place_img];
                                        $cover_img = $r[cover_img];
                                        $major_img1 = $r[major_img];

                                        $sql1 = "select * from tb_major where int_major='$major_img1' ";
                                        $result1 = mysql_db_query($dbname, $sql1);
                                        $qr1 = mysql_query($sql1);
                                        while ($result1 = mysql_fetch_array($qr1)) {
                                            $nameth_major = $result1['nameth_major'];
                                        } // end while
                                        $Date = explode("-", $date_img);
                                        $date_img = $Date[2] . "-" . $Date[1] . "-" . $Date[0];
                                        $no++;
?>
                            <tr>
                                <td>
                                    <div align='center'><?php echo "$no";?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$title_img1";?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$nameth_major";?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$pic_type1[$pic_type]"; ?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$date_img";?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$place_img";?></div>
                                </td>
                                

                                <td>
                                    <?php
                                           
                                                echo " <div align='right'><A HREF='admin_view.php?title_img=$title_img1' target='_blank'>More...</A></div></td>";
                                                echo "<td><div align='center'><A HREF='admin_del.php?id_del_img=$title_img1'><IMG SRC='images/del.png' WIDTH='20'></A></div></td></tr> ";
                                         
                                    }
                                } //end while

                            } //end if
                                } else {
                                    $no = 1;
                                    $sql = "select * from tb_picup WHERE major_img='$sess_major' order by id_img DESC ";
                                    $result = mysql_db_query($dbname, $sql);
                                    $num = mysql_num_rows($result);
                                    if ($num > 0) {
                                        while ($r = mysql_fetch_array($result)) {
                                            if ($title_img1 != $r[title_img]) {
                                                if (($no % 2) == 0) {$row_color = "#D8D8D8";} else { $row_color = "#FFFFFF";}
                                                $title_img1 = $r[title_img];
                                                $pic_type = $r[pic_type];
                                                $date_img = $r[date_img];
                                                $place_img = $r[place_img];
                                                $cover_img = $r[cover_img];
                                                $major_img1 = $r[major_img];
                                    
                                                $sql1 = "select * from tb_major where int_major='$major_img1' ";
                                                $result1 = mysql_db_query($dbname, $sql1);
                                                $qr1 = mysql_query($sql1);
                                                while ($result1 = mysql_fetch_array($qr1)) {
                                                    $nameth_major = $result1['nameth_major'];
                                                } // end while
                                                $Date = explode("-", $date_img);
                                                $date_img = $Date[2] . "-" . $Date[1] . "-" . $Date[0];
                                                $no++;
                                    ?>
                            <tr>
                            <td>
                                    <div align='center'><?php echo "$no";?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$title_img1";?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$nameth_major";?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$pic_type1[$pic_type]"; ?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$date_img";?></div>
                                </td>
                                <td>
                                    <div align='center'><?php echo "$place_img";?></div>
                                </td>

                                <td><?php
                                                    echo " <div align='right'><A HREF='admin_view.php?title_img=$title_img1' target='_blank'>More...</A></div></td>";
                                                    echo "<td><div align='center'><A HREF='admin_del.php?id_del_img=$title_img1'><IMG SRC='images/del.png' WIDTH='20'></A></div></td></tr> ";
                                                
                                            }
                                        } //end while
                                    
                                    } //end if

                                }
                                ?>
                            </tr>


                        </tbody>

                    </table>


                </div>
            </div>
        


        <!-- /.end content -->
</div>
</section>
<!-- /.end content -->

</div>
<!-- /.content-wrapper -->
<?php include "f.php";?>

<script>
$(function() {
    $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});
</script>