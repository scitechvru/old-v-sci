<?php
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>เพิ่มกิจกรรม</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="index.php">
                            <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าหลัก
                        </a>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!-- right column -->
                <div class="col-md-6">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มกิจกรรม</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form METHOD=POST ACTION="admin_image2.php" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อกิจกรรม</label>
                                            <input type="text" class="form-control" name="Title_image" id=""
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>รายละเอียดกิจกรรม</label>
                                            <textarea type="text" class="form-control" name="detail_img"
                                                placeholder="Enter ...">
                                                </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>สถานที่</label>
                                            <input type="text" class="form-control" name="place_image"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ประเภทกิจกรรม</label>
                                            <select  class="form-control" name="pic_type">
                                            <option value="0">กิจกรรมทั่วไป</option>
                                            <option value="1">ด้านศิลปวัฒนธรรมและความเป็นไทย</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- select -->
                                        <div class="form-group">
                                            <label>สังกัด</label>
                                            <select name="Major_image" id="Major_image" class="form-control" width="30">
                                                <?php  if($sess_major== 'OF' ){
                                                      include "connect.php";
                                                      $sql = "select * from tb_major order by id_major";
                                                      $result = mysql_db_query($dbname, $sql);
                                                      $qr = mysql_query($sql);
                                                      while ($result = mysql_fetch_array($qr)) {
                                                          $id_major = $result['id_major'];
                                                          $nameth_major = $result['nameth_major'];
                                                          $int_major = $result['int_major'];

                                                          ?>
                                                <option value="<?php echo "$int_major"; ?>">
                                                    <?php echo "$nameth_major"; ?>
                                                </option>
                                                <?php } }
                                                      else {
                                                        include "connect.php";
                                                        $sql = "select * from tb_major  WHERE int_major='$sess_major'";
                                                        $result = mysql_db_query($dbname, $sql);
                                                        $qr = mysql_query($sql);
                                                        while ($result = mysql_fetch_array($qr)) {
                                                            $id_major = $result['id_major'];
                                                            $nameth_major = $result['nameth_major'];
                                                            $int_major = $result['int_major'];
                                                            ?>
                                                <option value="<?php echo "$int_major"; ?>">
                                                    <?php echo "$nameth_major"; ?>
                                                </option>
                                                <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <!-- <label for="customFile">Custom File</label> -->
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="picco_image">
                                                <label class="custom-file-label" for="customFile">รูปปกกิจกรรม</label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <!-- <label for="customFile">Custom File</label> -->
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="upload">
                                                <label class="custom-file-label" for="customFile">รูปกิจกรรม</label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                        </div>

                        <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>วันที่เผยแพร่</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="far fa-calendar-alt"></i></span>
                                                </div>
                                                <input type="text" class="form-control" name="date_image"
                                                    data-inputmask-alias="datetime"
                                                    data-inputmask-inputformat="yyyy/mm/dd" data-mask>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                            <!-- Image preview -->
                            <div id="thumbnail"></div>
                        </div>
                        
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
</div>
<?php include 'f.php';?>


<script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>
<script type="text/javascript">
$(function() {
    $("#upload").on("click", function(e) {
        var lastFile = $(".file_upload:last").length;
        if (lastFile >= 0) {
            if (lastFile == 0 || $(".file_upload:last").val() != "") {
                var objFile = $("<input>", {
                    "class": "file_upload",
                    "type": "file",
                    "multiple": "true",
                    "name": "file_upload[]",
                    "style": "display:none",
                    change: function(e) {
                        var files = this.files
                        showThumbnail(files)
                    }
                });
                $(this).before(objFile);
                $(".file_upload:last").show().click().hide();
            } else {
                $(".file_upload:last").show().click().hide();
            }
        }
        e.preventDefault();
    });

    function showThumbnail(files) {

        //    $("#thumbnail").html("");
        for (var i = 0; i < files.length; i++) {
            var file = files[i]
            var imageType = /image.*/
            if (!file.type.match(imageType)) {
                console.log("Not an Image");
                continue;
            }

            var image = document.createElement("img");
            var thumbnail = document.getElementById("thumbnail");
            image.file = file;
            thumbnail.appendChild(image);

            var reader = new FileReader();
            reader.onload = (function(aImg) {
                return function(e) {
                    aImg.src = e.target.result;
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload = function() {
                ctx.drawImage(image, 100, 100)
            }
        } // end for loop

    } // end showThumbnail

});
</script>