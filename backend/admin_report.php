<?include "h.php";
include "navbar.php";
include "connect.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>มติการประชุม</h1>    
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="admin_report1.php" >
                            <i class="fas fa-plus"></i> เพิ่มมติการประชุม
                        </a>
                        
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
 <!-- /.row -->
 <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">มติการประชุม</h3>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>มติการประชุม</th>
                                        <th>ครั้ง</th>
                                        <th>วันที่</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php
$no = 0;
$sql = "select * from tb_report order by type_report ";
$result = mysql_db_query($dbname, $sql);
$num = mysql_num_rows($result);
if ($num > 0) {
    while ($r = mysql_fetch_array($result)) {
        $no++;
        if (($no % 2) == 0) {$row_color = "#D8D8D8";} else { $row_color = "#FFFFFF";}
        $id_report = $r[id_report];
        $type_report = $r[type_report];
        $num_report = $r[num_report];
        $year_report = $r[year_report];
        $date_report = $r[date_report];
        $file_report = $r[file_report];
        $status_report = $r[status_report];
        switch ($type_report) {
            case '1':$type_report = "มติกรรมการบริหารคณะ";
                break;
            case '2':$type_report = "มติกรรมการวิชาการคณะ";
                break;
        }
        $year_report = $year_report;
        $date1 = explode("-", $date_report);
        $date1[0] = $date1[0];
        $date_report1 = $date1[2] . "/" . $date1[1] . "/" . $date1[0];
        ?>
                                        <td><?php echo "$no";?></td>
                                        <td><?php echo "$type_report";?></td>
                                        <td><?php echo "$num_report";?></td>
                                        <td><span class="tag tag-success"><?php echo "$date_report";?></span></td>
                                        <td><a class="btn btn-app">
                                                <i class="fas fa-edit"></i>แก้ไข
                                            </a>
                                            </td>
                                            
                                    </tr>
                                    <?
                                          } //end if
} //end while

?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?include "f.php";?>