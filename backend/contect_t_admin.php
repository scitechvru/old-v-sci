<?php 
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
include "connect.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการช่องทางการติดต่ออาจารย์</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="th_admin_1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลการติดต่ออาจารย์
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">ข้อมูลการติดต่ออาจารย์</h3>

                        </div>
                        <!-- /.card-header -->

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">ลำดับ</th>
                                        <th scope="col" class="text-center">ชื่อ</th>
                                        <th scope="col" class="text-center">หลักสูตร</th>
                                        <th scope="col" class="text-center">เบอร์</th>
                                        <th scope="col" class="text-center">FB</th>
                                        <th scope="col" class="text-center">Line</th>
                                        <th scope="col" class="text-center">E-mail</th>
                                        <th scope="col" class="text-center">อื่น</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php
                         
                         $no = 0;
                         include "connect.php";
                         $sql = "select * from contact_teacher order by id_th DESC ";
                         $result = mysql_db_query($dbname, $sql);
                         $num = mysql_num_rows($result);
                         if ($num > 0) {
                             while ($r = mysql_fetch_array($result)) {
                                 $id_th = $r[id_th];
                                 $th_name = $r[th_name];
                                 $th_img = $r[th_img];
                                 $th_fb = $r[th_fb];
                                 $th_line = $r[th_line];
                                 $th_email = $r[th_email];
                                 $th_tel = $r[th_tel];
                                 $th_other = $r[th_other];
                                 $th_major = $r[th_major];
                                 $no++;
                        ?>
                           <th class="text-center" scope="row"><?php echo "$no"?></th>
                           <td class="text-center"><?php echo "$th_name"?></td>
                           <td class="text-center"><?php echo "$th_major"?></td>
                           <td class="text-center"><button class="item" data-toggle="tooltip" data-placement="top"  title="<?php echo "$th_tel"?>">
                           <i class="fas fa-mobile-alt"></i>
                           <td class="text-center"><button class="item" data-toggle="tooltip" data-placement="top"  title="<?php echo "$th_fb"?>">
                           <i class="fab fa-facebook-f"></i>
                                      </button></td>
                           <td class="text-center"><button class="item" data-toggle="tooltip" data-placement="top"  title="<?php echo "$th_line"?>">
                           <i class="fab fa-line"></i>
                                      </button></td>
                           <td class="text-center"><button class="item" data-toggle="tooltip" data-placement="top"  title="<?php echo "$th_email"?>">
                           <i class="fas fa-envelope"></i>
                                      </button></td>
                           <td class="text-center"><button class="item" data-toggle="tooltip" data-placement="top"  title="<?php echo "$th_other"?>">
                           <i class="fas fa-ellipsis-h"></i>
                                      </button></td>
                           <td> <div class="table-data-feature">
                                      
                                      <button class="item" data-toggle="tooltip" data-placement="top" onclick="window.location.href='cont_edit.php?id=<?php echo $id_th ;?>'" title="Edit">
                                      <i class="fas fa-edit"></i>
                                      </button>
                                      <button class="item" data-toggle="tooltip" data-placement="top" onclick="window.location.href='cont_delete.php?id=<?php echo $id_th ;?>'" title="Delete">
                                      <i class="fas fa-trash-alt"></i>
                                      </button>
                                  </div></td>
                           </tr>
                           <?php }
                           }
                           ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?include 'f.php';?>