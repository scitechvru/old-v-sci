<?php include 'headerccc.php';?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<?php include 'menubarccc.php';?>
<br>
<div class="inner-banner text-center">
    <div class="container">

        <div class="breadcumb-wrapper">
            <div class="clearfix">
                <div class="pull-left">
                    <ul class="list-inline link-list">
                        <li><i class="fa fa-picture-o"></i> <a href="#">ข้อมูลการติดต่ออาจารย์</a></li>

                    </ul>
                </div>

            </div><!-- /.container -->
        </div>
    </div><!-- /.container -->
</div>




<section class="blog-section sec-padd" style="margin-top:-70px;">
    <div class="container">

        <div class="row">

            <div class="table-responsive -xl">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="text-center">ลำดับ</th>
                            <th scope="col" class="text-center">ชื่ออาจารย์</th>
                            <th scope="col" class="text-center">สาขา</th>
                            <th scope="col" class="text-center">ข้อมูลเพิ่มเติม</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
include "connect.php";
$no = 0;
$sql = "select * from contact_teacher order by th_major desc";
$result = mysql_db_query($dbname, $sql);
$num = mysql_num_rows($result);
if ($num > 0) {
    while ($r = mysql_fetch_array($result)) {
        if ($th_img!= $r[th_img]) {
            $id_th = $r[id_th];
                                  $th_name = $r[th_name];
                                  $th_img = $r[th_img];
                                  $th_fb = $r[th_fb];
                                  $th_line = $r[th_line];
                                  $th_email = $r[th_email];
                                  $th_tel = $r[th_tel];
                                  $th_other = $r[th_other];
                                  $th_major = $r[th_major];
            $no++;
            
            ?>
                            <th class="text-center" scope="row"><?php echo "$no"; ?></th>
                            <td class="text-center"><?php echo "$th_name"; ?></td>
                            <td class="text-center">สาขา <?php echo "$th_major"; ?></td>
                            <td class="text-center"><a
                                    href="view_contect.php?id_cover=<?php echo "$id_th"; ?>">ข้อมูลเพิ่มเติม</a></td>
                        </tr>
                        <?php } //end if
    } //endwhile
} //end if

?>
                    </tbody>
                </table>
            </div>






        </div>


    </div>
</section>




<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
<?include 'footerccc.php';?>