<?php 
session_start();
include "h.php";
include "navbar.php";
include "chksession_admin.php";
require "connect.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการข้อมูลITA</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="ita_h.php">
                            <i class="fas fa-plus"></i> เพิ่มหัวข้อ ITA
                        </a>
                        <a class="btn btn-app" HREF="ita1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูล ITA
                        </a>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">ข้อมูล ITA</h3>

                        </div>
                        <!-- /.card-header -->

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="text-center">ลำดับ</th>
                                        <th colspan="2" class="text-center" scope="col">หัวข้อ ITA</th>
                                        <th scope="col" class="text-center">จัดการ</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php
                                $no=0;
                                $sql = "select * from ita_head ORDER BY id_ita ASC ";
                                $result = mysql_db_query($dbname, $sql);
                                $num = mysql_num_rows($result);
                                if ($num > 0) {
                                    while ($r = mysql_fetch_array($result)) {
                                        $id_ita = $r[id_ita];
                                        $sub_ita = $r[sub_ita];
                                        $detail = $r[detail];
                                        $no++
                               ?>
                                        <td class="text-center"><?php echo "O$no";?></td>
                                        <td class="text-center" colspan="2"><?php echo "$sub_ita";?></td>
                                        <td class="text-center">
                                            <a class="btn btn-app" HREF="edit_ita.php?id_edit=<?php echo "$id_ita"; ?>">
                                                <i class="fas fa-edit"></i> แก้ไขข้อมูล
                                            </a><a class="btn btn-app" HREF="delete_ita.php?id_delete=<?php echo "$id_ita"; ?>">
                                                <i class="fas fa-trash-alt"></i> ลบข้อมูล
                                            </a>
                                        </td>
                                    </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include "f.php";?>