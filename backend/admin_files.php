<?php 
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
include "connect.php";
include "array.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการข้อมูลแบบฟอร์ม</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="admin_files1.php">
                            <i class="fas fa-plus"></i> เพิ่มข้อมูลแบบฟอร์ม
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">แบบฟอร์ม</h3>

                        </div>
                        <!-- /.card-header -->

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">ลำดับ</th>
                                        <th scope="col" class="text-center">ชื่อแบบฟอร์ม</th>
                                        <th scope="col" class="text-center">ประเภทแบบฟอร์ม</th>
                                        <th scope="col" class="text-center">สถานะ</th>
                                        <th scope="col" class="text-center">ลบ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php
                                    $no=0;
$sql = "SELECT * from tb_files order by type_files DESC  ";
$result = mysql_db_query($dbname, $sql);
$num = mysql_num_rows($result);
if ($num > 0) {
    while ($r = mysql_fetch_array($result)) {
        $id_files = $r[id_files];
        $name_files = $r[name_files];
        $title_files = $r[title_files];
        $status_files = $r[status_files];
        $type=$r[type_files];
        $no++
                        ?>
                           <th class="text-center" scope="row"><?php echo "$no"?></th>
                           <td class="text-center"><?php echo "$name_files"?></td>
                           <td class="text-center"><?php echo " $typefiles1[$type]"?></td>
                           <?php if ($status_files == '1') { ?>
                           <td><A HREF="admin_files3.php?id_edit=<?php echo "$id_files";?>&status=<?php echo "$status_files";?>"><IMG SRC="images/show.png" WIDTH="28" ></A></td>
                           <?php }else{?>
                           <td><A HREF="admin_files3.php?id_edit=<?php echo "$id_files";?>&status=<?php echo "$status_files";?>"><IMG SRC="images/hide.png" WIDTH="28" ></A></td>
                           <?php }?>
                           <td><A HREF="admin_del.php?id_del_files=<?php echo "$id_files";?>"><IMG SRC="images/del.png" WIDTH="20" ></td>
                           </tr>
                           <?php }
                           }
                           ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?include 'f.php';?>