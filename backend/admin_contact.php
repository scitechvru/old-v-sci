<?php include 'h.php';?>
<?php include "chksession_admin.php";?>
<!-- MENU SIDEBAR-->
<?php include 'navbar.php';?>
<!-- END MENU SIDEBAR-->

<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <strong>เพิ่มข้อมูลที่ติดต่อ </strong>
                            </div>
                            <div class="card-body card-block">
                                <form action="admin_contact1.php" method="post" enctype="multipart/form-data"
                                    class="form-horizontal">

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_sub" class=" form-control-label">ชื่อหน่วยงาน</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="c_sub" name="c_sub" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_location" class=" form-control-label">ที่อยู่</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="c_location" name="c_location" class="form-control">

                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_tel1" class=" form-control-label">เบอร์ติดต่อ</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="c_tel1" name="c_tel1" class="form-control">

                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_tel2" class=" form-control-label">เบอร์ติดต่อสำรอง</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="c_tel2" name="c_tel2" class="form-control">

                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_fax" class=" form-control-label">Fax</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="c_fax" name="c_fax" class="form-control">

                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_email" class=" form-control-label">E-mail</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="email" id="c_email" name="c_email" class="form-control">

                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_fb" class=" form-control-label">Facebook</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="c_fb" name="c_fb" class="form-control">

                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="c_major" class=" form-control-label">สังกัด</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <select name="c_major" id="c_major" class="form-control-sm form-control">
                                                <?php  if($sess_major== 'OF' ){
include "connect.php";
$sql = "select * from tb_major order by id_major";
$result = mysql_db_query($dbname, $sql);
$qr = mysql_query($sql);
while ($result = mysql_fetch_array($qr)) {
    $id_major = $result['id_major'];
    $nameth_major = $result['nameth_major'];
    $int_major = $result['int_major'];

    ?>
                                                <option value="<?php echo "$int_major"; ?>">
                                                    <?php echo "$nameth_major"; ?></option>
                                                <?php } }
else {
  include "connect.php";
  $sql = "select * from tb_major  WHERE int_major='$sess_major'";
  $result = mysql_db_query($dbname, $sql);
  $qr = mysql_query($sql);
  while ($result = mysql_fetch_array($qr)) {
      $id_major = $result['id_major'];
      $nameth_major = $result['nameth_major'];
      $int_major = $result['int_major'];

      
      ?>
                                                <option value="<?php echo "$int_major"; ?>">
                                                    <?php echo "$nameth_major"; ?></option>
                                                <?php } }?>
                                            </select>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                            </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>
    <?php include 'f.php';?>