<?php 
session_start();
include "h.php";
include "navbar.php";
include "chksession_admin.php";
require "connect.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>ข้อมูลรายละเอียด ITA</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="index.php">
                            <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าหลัก
                        </a>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!-- right column -->
                <div class="col-md-6">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">ข้อมูลรายละเอียด ITA</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form METHOD=POST ACTION="ita_1_save.php" ENCTYPE="multipart/form-data" role="form">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>หัวข้อ</label>
                                            
                                            <select name="id_ita" id="id_ita" class="form-control-sm form-control">
                                            <option value="0">โปรดระบุหัวข้อ</option>
                                            <?php
                                             $sql = "select * from ita_head ORDER BY id_ita ASC ";
                                             $result = mysql_db_query($dbname, $sql);
                                             $num = mysql_num_rows($result);
                                             if ($num > 0) {
                                                 while ($r = mysql_fetch_array($result)) {
                                                     $id_ita = $r[id_ita];
                                                     $sub_ita = $r[sub_ita];
                                                     $detail = $r[detail];
                                            ?>
                                                <option value="<?php echo "$id_ita";?>"><?php echo "$sub_ita";?>
                                                </option>
                                                <?php }}?>
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อ</label>
                                            <input type="text" class="form-control" name="name" placeholder="ชื่อไฟล์">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ลิงค์</label>
                                            <input type="text" class="form-control" name="link" placeholder="ลิงค์ข้อมูล">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>เอกสาร</label>
                                            <div class="custom-file">
                                            <label class="custom-file-label" for="customFile">เอกสาร</label>
                                                <input class="custom-file-input" type="file" id="file" name="file"
                                                    onchange="return fileValidation2()">
                                                
                                            </div>
                                            <small style="color:#FF0000 ;">รองรับแค่ไฟล์ PDF เท่านั้น</small>
                                        </div>
                                    </div>
                                </div>




                                <div class="row">

                                </div>



                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
</div>
<!-- /.card -->
</div>
<!--/.col (right) -->
</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</section>

</div>
<!-- /.content-wrapper -->
<?php include "f.php";?>

<script type="text/javascript">
//สคริปห้ามกรอกตัวหนังสือกรอกได้เฉพาะตัวเลข
$("#year").keydown(function(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
</script>

<script>
function fileValidation() {
    var fileInput = document.getElementById('img');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.png)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('รองรับแค่ไฟล์นามสกุล .png เท่านั้น.');
        fileInput.value = '';
        return false;
    } else {
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview').innerHTML = '<img src="' + e.target.result + '"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}
</script>
<script>
function fileValidation2() {
    var fileInput = document.getElementById('file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.pdf)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('รองรับแค่ไฟล์นามสกุล .pdf เท่านั้น.');
        fileInput.value = '';
        return false;
    }
}
</script>