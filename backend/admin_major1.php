<?php
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>เพิ่มหลักสูตร</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="index.php">
                            <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าหลัก
                        </a>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!-- right column -->
                <div class="col-md-6">
                    <!-- general form elements disabled -->
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มหลักสูตร</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form METHOD=POST ACTION="admin_major2.php" ENCTYPE="multipart/form-data" role="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อหลักสูตรภาษาไทย</label>
                                            <input type="text" class="form-control"id="name_thmaj" name="name_thmaj"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อหลักสูตรภาษาอังกฤษ</label>
                                            <input type="text" class="form-control" id="name_enmaj" name="name_enmaj"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ชื่อย่อหลักสูตร</label>
                                            <input type="text" class="form-control" id="int_maj" name="int_maj"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Email หลักสูตร</label>
                                            <input type="text" class="form-control" id="email_maj" name="email_maj"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>ลิงค์หลักสูตร</label>
                                            <input type="text" class="form-control"id="link_maj" name="link_maj"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Facebook หลักสูตร</label>
                                            <input type="text" class="form-control" id="facebook_maj" name="facebook_maj"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Line หลักสูตร</label>
                                            <input type="text" class="form-control" id="line_maj" name="line_maj"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">บันทึก</button>
                                   
                                </div>

                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
        <?php include 'f.php';?>
        <!--
        <div class="row">
                                    <div class="col-sm-6">
                                       
                                        <div class="form-group">
                                            <label>ประเภทข่าว</label>
                                            <select class="form-control" id="type_news" name="type_news">
                                            <option value="">โปรดระบุประเภทข่าว</option>
                                                <option value="PR"> ประชาสัมพันธ์</option>
                                                <option value="ED"> ศึกษาต่อ</option>
                                                <option value="RE"> รับสมัครงาน</option>
                                                <option value="AL"> ศิษย์เก่า</option>
                                                <option value="PRM">ประชาสัมพันธ์หลักสูตร</option>
                                                <option  value="ITA"> Work@Home</option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                           
                                            <div class="form-group">
                                                <label>สังกัด</label>
                                                <select name="Major_news" id="Major_news" class="form-control"
                                                    width="30">
                                                    <option value="">โปรดระบุสังกัด</option>
                                                    <?php /* if($sess_major== 'OF' ){
                                                      include "connect.php";
                                                      $sql = "select * from tb_major order by id_major";
                                                      $result = mysql_db_query($dbname, $sql);
                                                      $qr = mysql_query($sql);
                                                      while ($result = mysql_fetch_array($qr)) {
                                                          $id_major = $result['id_major'];
                                                          $nameth_major = $result['nameth_major'];
                                                          $int_major = $result['int_major'];

                                                          ?>
                                                    <option value="<?php echo "$int_major"; ?>">
                                                        <?php echo "$nameth_major"; ?>
                                                    </option>
                                                    <?php } }
                                                      else {
                                                        include "connect.php";
                                                        $sql = "select * from tb_major  WHERE int_major='$sess_major'";
                                                        $result = mysql_db_query($dbname, $sql);
                                                        $qr = mysql_query($sql);
                                                        while ($result = mysql_fetch_array($qr)) {
                                                            $id_major = $result['id_major'];
                                                            $nameth_major = $result['nameth_major'];
                                                            $int_major = $result['int_major'];
                                                            ?>
                                                    <option value="<?php echo "$int_major"; ?>">
                                                        <?php echo "$nameth_major"; ?>
                                                    </option>
                                                    <?php } }*/?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"id="file_news" name="file_news">
                                                    <label class="custom-file-label"
                                                        for="customFile">ไฟล์ข่าวประชาสัมพันธ์</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    
-->