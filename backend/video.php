<?php 
session_start();
include "h.php";
include "chksession_admin.php";
include "navbar.php";
include "connect.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการสื่อเผยแพร่วิดีโอ</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="video_add.php">
                            <i class="fas fa-plus"></i> เพิ่มสื่อเผยแพร่วิดีโอ
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">ข้อมูลวิดีโอที่เผยแพร่</h3>

                        </div>
                        <!-- /.card-header -->

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">ลำดับ</th>
                                        <th scope="col" class="text-center">ชื่อ</th>
                                        <th scope="col" class="text-center">ลิงค์</th>
                                        <th scope="col" class="text-center">สาขา</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php
                         
                         $no = 0;
                         include "connect.php";
                         $sql = "select * from tb_youtube order by id_youtube DESC ";
                         $result = mysql_db_query($dbname, $sql);
                         $num = mysql_num_rows($result);
                         if ($num > 0) {
                             while ($r = mysql_fetch_array($result)) {
                                 $id_youtube = $r[id_youtube];
                                 $title_youtube = $r[title_youtube];
                                 $youtube_code = $r[youtube_code];
                                 $date_youtube = $r[date_youtube];
                                 $link_youtube = $r[link_youtube];
                                 $major_youtube = $r[major_youtube];
                                 $code_u = $r[code_u];
                                
                                 $no++;
                        ?>
                           <th class="text-center" scope="row"><?php echo "$no"?></th>
                           <td class="text-center"><?php echo "$title_youtube"?></td>
                           <td class="text-center"><?php echo "$link_youtube"?></td>
                           <td class="text-center"><?php echo "$major_youtube"?></td>
                          <!-- <td> <div class="table-data-feature">
                                      
                                      <button class="item" data-toggle="tooltip" data-placement="top" onclick="window.location.href='cont_edit.php?id=<?php echo $id_th ;?>'" title="Edit">
                                      <i class="fas fa-edit"></i>
                                      </button>
                                      <button class="item" data-toggle="tooltip" data-placement="top" onclick="window.location.href='cont_delete.php?id=<?php echo $id_th ;?>'" title="Delete">
                                      <i class="fas fa-trash-alt"></i>
                                      </button>
                                  </div></td>-->
                           </tr>
                           <?php }
                           }
                           ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?php include "f.php";?>