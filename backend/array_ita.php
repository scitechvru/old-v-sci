<?php
$type1=array("0"=>"","1"=>"Q&A","2"=>"คำถามพบบ่อย","3"=>"ข้อเสนอแนะ","4"=>"ข้อร้องเรียน");
$status1=array("0"=>"","1"=>"บันทึกคำถาม","2"=>"อนุมัติคำถาม","3"=>"ตอบกลับเรียบร้อย","4"=>"รับทราบคำร้องเรียน","5"=>"รับทราบข้อเสนอแนะ");
$thaimonth=array(
    "00"=>"",
"01"=>"มกราคม",
"02"=>"กุมภาพันธ์",
"03"=>"มีนาคม",
"04"=>"เมษายน",
"05"=>"พฤษภาคม",
"06"=>"มิถุนายน",
"07"=>"กรกฎาคม",
"08"=>"สิงหาคม",
"09"=>"กันยายน",
"10"=>"ตุลาคม",
"11"=>"พฤศจิกายน",
"12"=>"ธันวาคม");
$thaimonth1=array(
    "00"=>"",
    "01"=>"ม.ค.",
    "02"=>"ก.พ.",
    "03"=>"มี.ค.",
    "04"=>"เม.ย.",
    "05"=>"พ.ค.",
    "06"=>"มิ.ย.",
    "07"=>"ก.ค.",
    "08"=>"ส.ค.",
    "09"=>"ก.ย.",
    "10"=>"ต.ค.",
    "11"=>"พ.ย.",
    "12"=>"ธ.ค.");

    $type=array(
        "0"=>"กรุณาระบุตำแหน่ง",
        "1"=>"นักวิชาการคอมพิวเตอร์",
        "2"=>"เจ้าหน้าที่บริหารงานทั่วไป",
        "3"=>"นักวิทยาศาสตร์",
        "4"=>"นักวิชาการศึกษา",
        "5"=>"นักวิชาการพัสดุ");
        $ita=array(
            "0"=>"กรุณาระบุข้อมูลที่ต้องการเพิ่ม",
            "1"=>"โครงสร้างหน่วยงาน",
            "2"=>"อำนาจหน้าที่",
            "3"=>"แผนยุทศาสตร์หรือแผนพัฒนาหน่วยงาน",
            "4"=>"กฏหมายที่เกี่ยวข้อง",
            "5"=>"แผนดำเนินงานประจำปี",
            "6"=>"คู่มือหรือมาตรฐานการปฏิบัติงาน",
            "7"=>"รายงานการกำกับติดตามการกำเนินงานประจำปีรอบ 6 เดือน",
            "8"=>"รายงานผลการดำเนินงานประจำปี",
            "9"=>"คู่มือหรือมาตรฐานการให้บริการ",
            "10"=>"ข้อมูลเชิงสถิติการให้บริการ",
            "11"=>"รายงานผลการสำรวจความพึงพอใจการให้บริการ",
            "12"=>"แผนการใช้จ่ายงบประมาณประจำปี",
            "13"=>"รายงานการกำกับติดตามการใช้จ่ายงบประมาณประจำปีรอบ6เดือน",
            "14"=>"รายงานผลการใช้จ่ายงบประมาณ ประจำปี",
            "15"=>"ประกาศต่างๆเกี่ยวกับการจัดซื้อจัดจ้างหรือการจัดหาพัสดุ",
            "16"=>"สรุปผลการจัดซื้อจัดจ้างหรือการจัดหาพัสดุรายเดือน",
            "17"=>"รายงานผลการจัดซื้อจัดจ้างหรือการจัดหาพัสดุประจำปี",
            "18"=>"นโยบายการบริหารทรัพยากรบุคคล",
            "19"=>"การดำเนินการตามนโยบายการบริหารทรัพยากรบุคคล",
            "20"=>"หลักเกณฑ์การบริหารและพัฒนาทรัพยากรบุคคล",
            "21"=>"รายงานผลการบริหารและพัฒนาทรัพยากรบุคคลประจำปี",
            "22"=>"แนวปฏิบัติการจัดการเรื่องร้องเรียนการทุจริต",
            "23"=>"ช่องทางแจ้งเรื่องร้องเรียนการทุจริต",
            "24"=>"ข้อมูลเชิงสถิติเรื่องร้องเรียนการทุจริตประจำปี",
            "25"=>"ช่องทางการรับฟังความคิดเห็น",
            "26"=>"การเปิดโอกาสให้เกิดการมีส่วนร่วม",
            "27"=>"เจตจำนงสุจริตของผู้บริหาร",
            "28"=>"การมีส่วนร่วมของผู้บริหาร",
            "29"=>"การประเมินความเสี่ยงการทุจริตประจำปี",
            "30"=>"การดำเนินการเพื่อการจัดการความเสี่ยงการทุจริต",
            "31"=>"การเสริมสร้างวัฒนธรรมองค์กร",
            "32"=>"แผนปฏิบัติการป้องกันการทุจริตประจำปี",
            "33"=>"รายงานการกำกับติดตามการดำเนินการป้องกันการทุจริตประจำปีรอบ6เดือน",
            "34"=>"รายงานผลการดำเนินการป้องกันการทุจริตประจำปี",
            "35"=>"มาตรการส่งเสริมคุรธรรมและความโปร่งใส่ภายในหน่วยงาน",
            "36"=>"การดำเนินการตามมาตรการส่งเสริมคุณธรรมและความโปร่งใส่ภายในหน่วยงาน");
        
    
?>