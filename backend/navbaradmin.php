<body class="animsition">
    <div class="page-wrapper">
        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="images/icon/logo1.png" alt="Cool Admin" />
                </a>
            </div>
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav ">
                        <?php if($_SESSION["sess_adminmajor"] == "OF"){ ?>
                            <li><a href="admin_home.php">HOME</a></li>
                            <li><a href="admin_major.php">หน่วยงาน</a></li>
                            <li><a href="admin_per.php">บุคลากร</a></li>
                            <li><a href="admin_files.php">แบบฟอร์ม</a></li>
                            <li><a href="admin_report.php">วาระการประชุม</a></li>
                            <li><a href="admin_banner.php">Banner</a></li>
                            <li><a href="admin_image.php">กิจกรรม</a></li>
                            <li><a href="admin_news.php">News</a></li>
                            <li><a href="admin_admins.php">Admin</a></li>
                            <?php }else{ ?>
                                <li><a href="admin_banner.php">Banner</a></li>
                            <li><a href="admin_image.php">กิจกรรม</a></li>
                            <li><a href="admin_news.php">News</a></li>
                            <?php } ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="admin_logout.php"> Logout <span class="glyphicon glyphicon-log-in"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <?php if($_SESSION["sess_adminmajor"] == "OF"){ ?>
                        <li class="active has-sub">
                            <a class="js-arrow" href="ita.php">
                                <i class="fas fa-tachometer-alt"></i>ITA</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">

                            </ul>
                        </li>
                        <li><a href="contect_t_admin.php"><i class="far fa-id-badge"></i>ช่องทางติดต่ออาจารย์</a></li>
                        <li><a href="admin_home.php"><i class="fas fa-home"></i>HOME</a></li>
                        <li><a href="admin_major.php"><i class="fas fa-building"></i>หน่วยงาน</a></li>
                        <li><a href="admin_per.php"><i class="fas fa-group (alias)"></i>บุคลากร</a></li>
                        <li><a href="admin_files.php"><i class="fas fa-edit"></i>แบบฟอร์ม</a></li>
                        <li><a href="admin_report.php"><i class="fas fa-bookmark"></i>วาระการประชุม</a></li>
                        <li><a href="admin_banner.php"><i class="fas fa-bullhorn"></i>Banner</a></li>
                        <li><a href="admin_image.php"><i class="fas fa-calendar-alt"></i>กิจกรรม</a></li>
                        <li><a href="admin_news.php"><i class="fas fa-newspaper"></i>News</a></li>
                        <li><a href="admin_admins.php"><i class="fas fa-user"></i>Admin</a></li>
                        <?php }else{ ?>
                        <li><a href="admin_banner.php"><i class="fas fa-bullhorn"></i>Banner</a></li>
                        <li><a href="admin_image.php"><i class="fas fa-calendar-alt"></i>กิจกรรม</a></li>
                        <li><a href="admin_news.php"><i class="fas fa-newspaper"></i>News</a></li>
                        <?php } ?>
                        </li>
                    </ul>
                </nav>
            </div>

        </aside>
        <!-- END MENU SIDEBAR-->