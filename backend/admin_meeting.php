<?include 'h.php';
include 'navbar.php';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>วาระการประชุม</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" href="index.php">
                            <i class="fas fa-arrow-alt-circle-left"></i> กลับหน้าหลัก
                        </a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">


                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">เพิ่มวาระการประชุม</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form  action="meeting_save.php" method="post" enctype="multipart/form-data" role="form">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>ครั้ง</label>
                                                <select name="report_time" class="form-control">
                                                <option >โปรดระบุครั้งที่ประชุม</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>ปี</label>
                                                <select  name="year" class="form-control">
                                                <option >โปรดเลือกปี</option>
                                                <? $year=date(Y)+541;
                                                $i=0;
                                                while ( $i <= 3 ){
echo "<option value='$year'>$year</option>";
$year++;
$i++;
}                 ?>
                                             
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- Date dd/mm/yyyy -->
                                            <div class="form-group">
                                                <label>ในวันที่:</label>

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i
                                                                class="far fa-calendar-alt"></i></span>
                                                    </div>
                                                    <input  name="report_date" type="text" class="form-control"
                                                        data-inputmask-alias="datetime"
                                                        data-inputmask-inputformat="yyyy/mm/dd" data-mask>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>สถานที่</label>
                                                <input type="text"  name="location" class="form-control" placeholder="ณ .......">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- select -->
                                            <div class="form-group">
                                                <label>ประเภทวาระการประชุมคณะกรรม</label>
                                                <select  name="report_type" class="form-control">
                                                <option >โปรดระบุประเภทการประชุม</option>
                                                    <option value="1">ระเบียบวาระการประชุมคณะกรรมการวิชาการคณะ</option>
                                                    <option value="2">ระเบียบวาระการประชุมคณะกรรมการบริหารคณะ
                                                    </option>
                                                   
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">บันทึก</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?include 'f.php';?>