<?php 
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
include "connect.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการหลักสูตร</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="admin_major1.php">
                            <i class="fas fa-plus"></i> เพิ่มหลักสูตร
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">หลักสูตร</h3>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>

                                        <th colspan="2" scope="col">ชื่อหลักสูตร</th>
                                        <th scope="col">ชื่อย่อ</th>
                                        <th scope="col">Link</th>
                                        <th colspan="3" scope="col"></th>
                                        <th scope="col">Manage</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php
$no = 0;
include "connect.php";
$sql = "select * from tb_major order by nameen_major";
$result = mysql_db_query($dbname, $sql);
$num = mysql_num_rows($result);
if ($num > 0) {
    while ($r = mysql_fetch_array($result)) {
        $id_major = $r[id_major];
        $nameth_major = $r[nameth_major];
        $nameen_major = $r[nameen_major];
        $int_major = $r[int_major];
        $email_major = $r[email_major];
        $facebook_major = $r[facebook_major];
        $line_major = $r[line_major];
        $link_major = $r[link_major];
        $no++;
        if (($no % 2) == 0) {$row_color = "#D8D8D8";} else { $row_color = "#FFFFFF";}

        echo "  <tr bgcolor=$row_color>
					<td>$nameth_major</td>
					<td>$nameen_major</td>
					<td>$int_major</td>
					<td>$link_major</td>";
        if ($email_major == "") {
            echo "<td><CENTER><IMG SRC='images/no2.png' WIDTH='20'></CENTER></td>";
        } else {
            echo "<td><CENTER><A HREF='$email_major' target='_blank'><IMG SRC='images/email.png' WIDTH='25'></A></CENTER></td>";
        }
        if ($facebook_major == "") {
            echo "<td><CENTER><IMG SRC='images/no2.png' WIDTH='20'></CENTER></td>";
        } else {
            echo "	<td><CENTER><A HREF='$facebook_major' target='_blank'><IMG SRC='images/facebook.png' WIDTH='25'></A></CENTER></td>";
        }
        if ($line_major == "") {
            echo "<td><CENTER><A HREF='$line_major' target='_blank'><IMG SRC='images/no2.png' WIDTH='20'></A></CENTER></td>";
        } else {
            echo "	<td><CENTER><IMG SRC='images/line.png' WIDTH='25'></CENTER></td>";
        }
        echo "	<td><CENTER>
					<A HREF='admin_major3.php?id_edit=$id_major'><IMG SRC='images/edit.png' WIDTH='20'></A>
					<A HREF='admin_del.php?id_del_major=$id_major&int_major=$int_major'><IMG SRC='images/del.png' WIDTH='20'></A></CENTER></td>
				</tr>";
    } // end while
} // end if
?>

                            </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?include 'f.php';?>