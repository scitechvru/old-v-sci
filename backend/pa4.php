<?php session_start();
include "h.php";
include "navbar.php";
require "../inc/connect.php";
require "../inc/function.php";


//$idcard_per = $_SESSION[sess_adminidcard_per];
//$type_per = $_SESSION[sess_admintype];
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>ประเมินสมรรถนะ </h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">รายชื่อผู้ถูกประเมิน</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">ลำดับ</th>
                                        <th class="text-center">ชื่อ - นามสกุล</th>
                                        <th class="text-center">สาขา</th>
                                        <th class="text-center">ระดับ</th>
                                        <th class="text-center">ปม31</th>
                                        <th class="text-center">ปม32</th>
                                        <th class="text-center">ปม33</th>
                                        <th class="text-center">ปม34</th>
                                        <th class="text-center">ปม35</th>
                                        <th class="text-center">ปม2</th>
                                        <th class="text-center">ปม4</th>
                                        <th class="text-center">รายละเอียด</th>
                                    </tr>
                                </thead>
                                <tbody>
            <?php $count = 0;
$avg = 0; $cot_pm = 0; $sum31 = 0;   
            $sql ="SELECT * FROM tb_peraca2 inner join tb_per on tb_peraca2.idcard_per = tb_per.idcard_per";           
            $result = mysql_query($sql,$con);
            while($r = mysql_fetch_array($result)){
                $id_per = $r[id_per];
                $idcard_per = $r[idcard_per];
                $position_ex = $r[position_ex];
                //$id_per = $r[id_per]; 
                $tname2_per = $r[tname2_per];
                $tname1_per = $r[tname1_per];
                $fnamet_per = $r[fnamet_per];
                $lnamet_per = $r[lnamet_per];
                $major_per = $r[major_per];   
                $type_per = $r[type_per];      
                $level_peraca = $r[level_peraca];    

                switch($level_peraca){
                    case '1':$level="2"; break;
                    case '2':$level="3"; break;
                    case '3':$level="4"; break;
                    case '4':$level="5"; break;

                }
                switch ($tname2_per) {
                    case 'อ.':$type_per2="2"; break;
                    case 'ผศ.':$type_per2="3"; break;
                    case 'รศ.':$type_per2="4"; break;
                    case 'ศ.':$type_per2="5"; break;  
                    case 'ปฎิบัติการ':$type_per2="2"; break;
                    case 'ชำนาญการ':$type_per2="3"; break;
                    case 'ชำนาญการพิเศษ':$type_per2="3"; break;
                    case 'เชี่ยวชาญ':$type_per2="4"; break;
                    case 'เชี่ยวชาญพิเศษ':$type_per2="5"; break;

                    default : $type_per2="0"; break;                            
                }//end switch
                if($type_per == 2 or $type_per == 4){
                    $tname2_per = $r[tname_per];
                } 

            //  1/2/5/7/8  รวม 7 คน
                $avg = 0; $cot_pm31 = 0; $sum31 = 0; $avg31 = 0;
                $sql31 ="SELECT * FROM tb_perpm3  WHERE idcard_per = '$idcard_per' and pm_pm3 = '31' ";
                $result31 = mysql_query($sql31,$con);
                while($r31 = mysql_fetch_array($result31)){
                    $avg_pm31 = $r31[avg_pm3];
                    $avg31   = $avg31 + $avg_pm31;
                    $cot_pm31 ++;
                }
                $sum31 = ($avg31 / $cot_pm31);
                $sum31 = number_format(floatval($sum31),2);

                $avg = 0; $cot_pm32 = 0; $sum32 = 0; $avg32 = 0;
                $sql32 ="SELECT * FROM tb_perpm3  WHERE idcard_per = '$idcard_per' and pm_pm3 = '32' ";
                $result32 = mysql_query($sql32,$con);
                while($r32 = mysql_fetch_array($result32)){
                    $avg_pm32 = $r32[avg_pm3];
                    $avg32   = $avg32 + $avg_pm32;
                    $cot_pm32 ++;
                }
                $sum32 = $avg32 / $cot_pm32 ;                
                $sum32 = number_format(floatval($sum32),2);

                $avg = 0; $cot_pm33 = 0; $sum33 = 0; $avg33 = 0;
                $sql33 ="SELECT * FROM tb_perpm3  WHERE idcard_per = '$idcard_per' and pm_pm3 = '33' ";
                $result33 = mysql_query($sql33,$con);
                while($r33 = mysql_fetch_array($result33)){
                    $avg_pm33 = $r33[avg_pm3];
                    $avg33   = $avg33 + $avg_pm33;
                    $cot_pm33 ++;
                }
                $sum33 = $avg33 / $cot_pm33 ;
                $sum33 = number_format(floatval($sum33),2);

                $avg = 0; $cot_pm34 = 0; $sum34 = 0; $avg34 = 0;
                $sql34 ="SELECT * FROM tb_perpm3  WHERE idcard_per = '$idcard_per' and pm_pm3 = '34' ";
                $result34 = mysql_query($sql34,$con);
                while($r34 = mysql_fetch_array($result34)){
                    $avg_pm34 = $r34[avg_pm3];
                    $avg34   = $avg34 + $avg_pm34;
                    $cot_pm34 ++;
                }
                $sum34 = $avg34 / $cot_pm34 ; 
                $sum34 = number_format(floatval($sum34),2);     

                $avg = 0; $cot_pm35 = 0; $sum35 = 0; $avg35 = 0;
                $sql35 ="SELECT * FROM tb_perpm3  WHERE idcard_per = '$idcard_per' and pm_pm3 = '35' ";
                $result35 = mysql_query($sql35,$con);
                while($r35 = mysql_fetch_array($result35)){
                    $avg_pm35 = $r35[avg_pm3];
                    $avg35   = $avg35 + $avg_pm35;
                    $cot_pm35 ++;
                }
                $sum35 = $avg35 / $cot_pm35 ;   
                $sum35 = number_format(floatval($sum35),2);   
           
                $sum_pm2 = 0; $sum2 = 0; $sum4 = 0;
                
                $sum_pm2 = (($sum31 * 20) / 100) + (($sum32 * 20) / 100) + (($sum33 * 20) / 100) + (($sum34 * 20) / 100) + (($sum35 * 20) / 100)   ;

                $sum2 = number_format(floatval(($sum_pm2* 100)/ 5),2);
                $sum_pm2 = number_format(floatval($sum_pm2),2);

                $sum4 =  number_format(floatval(($sum2* 20)/100),2);

            $fullname=$tname2_per.$tname1_per." ".$fnamet_per."  ".$lnamet_per;
            $count++;    
           

            ?>

                                    <tr>
                                        <td class="text-center"><?php echo"$count"; ?></td>
                                        <td ><?php echo"$fullname"; ?>
                                        </td>
                                        <td class="text-center"><?php echo"$major_per"; ?></td>
                                        <td class="text-center"><?php echo"$level"; ?> </td>
                                        <td class="text-center"><?php echo"$sum31"; ?> </td>
                                        <td class="text-center"><?php echo"$sum32"; ?> </td>
                                        <td class="text-center"><?php echo"$sum33"; ?> </td>
                                        <td class="text-center"><?php echo"$sum34"; ?> </td>
                                        <td class="text-center"><?php echo"$sum35"; ?> </td>
                                        <td class="text-center"><?php echo"$sum_pm2 / $sum2"; ?> </td>
                                        <td class="text-center"><?php echo"$sum4"; ?> </td>
                                        <td class="text-center">
                                            <a class="btn btn-success" href="pa2.php?idcard_per=<?php echo $idcard_per ?>&level=<?php echo $level ?>&rou=1"> <?php echo"$cot_pm31"; ?>
                                                <i class="fas fa-folder"></i> 
                                            </a>
                                        </td>
                                        
                                    </tr>
            <?php     }//end while ?>

                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php 
  include "f.php";
  ?>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>