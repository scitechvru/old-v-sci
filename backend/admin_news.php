<?php 
session_start();
include 'h.php';
include "chksession_admin.php";
include 'navbar.php';
include "connect.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>จัดการข่าวประชาสัมพันธ์</h1>
                </div>
                <!--ปุ่ม-->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a class="btn btn-app" HREF="admin_news1.php">
                            <i class="fas fa-plus"></i> เพิ่มข่าวประชาสัมพันธ์
                        </a>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!--เริ่มเนื้อหา-->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">ข่าวประชาสัมพันธ์</h3>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">ลำดับ</th>
                                        <th scope="col" class="text-center">หัวข้อข่าว</th>
                                        <th scope="col" class="text-center">ประเภทข่าว</th>
                                        <th scope="col" class="text-center">วันที่ลงข่าว</th>
                                        <th scope="col" colspan="2" class="text-center">แสดง</th>
                                        <th scope="col" colspan="2" class="text-center">ลบ</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <?php if($sess_major== 'OF' ){
                        $thaimonth=array(
                            "00"=>"",
                            "01"=>"ม.ค.",
                            "02"=>"ก.พ.",
                            "03"=>"มี.ค.",
                            "04"=>"เม.ย.",
                            "05"=>"พ.ค.",
                            "06"=>"มิ.ย.",
                            "07"=>"ก.ค.",
                            "08"=>"ส.ค.",
                            "09"=>"ก.ย",
                            "10"=>"ต.ค.",
                            "11"=>"พ.ย.",
                            "12"=>"ธ.ค.");
                            $news_type=array(
                                "PR"=>"ประชาสัมพันธ์",
                                "ED"=>"ศึกษาต่อ",
                                "RE"=>"สมัคงาน",
                                "PRM"=>"ประชาสัมพันธ์หลักสูตร",
                                "AL"=>"ศิษย์เก่า",
                                "ITA"=>"Work@Home",
                                );
                        $no=0;
                        $sql = "select * from tb_news ORDER BY date_news DESC ";
                        $result = mysql_db_query($dbname, $sql);
                        $num = mysql_num_rows($result);
                        if ($num > 0) {
                            while ($r = mysql_fetch_array($result)) {
                                $id_news = $r[id_news];
                                $major_news = $r[major_news];
                                $title_news = $r[title_news];
                                $name_news = $r[name_news];
                                $status_news = $r[status_news];
                                $type_news = $r[type_news];
                                $date_news = $r[date_news];
                                $Date = explode("-", $date_news);
                                $date_news2 = $Date[2] . "/" . $thaimonth[$Date[1]] . "/" . ($Date[0]+543);
                                $no++
                                //<A HREF='files/$name_files'>$title_files</A>
                                ?>
                            <th scope="row" class="text-center"><?php echo "$no" ?></th>
                            <td ><?php echo "$title_news" ?></td>
                            <td class="text-center"><?php echo "$news_type[$type_news]" ?></td>
                            <td ><?php echo "$date_news2" ?></td>
                            <td class="text-center" colspan="2"><?php  if ($status_news == '1') {
            echo "<A HREF='admin_news3.php?id_edit=$id_news&status=$status_news'><IMG SRC='images/show.png' WIDTH='28' ></A></td> ";
          
        } else {
            echo "<A HREF='admin_news3.php?id_edit=$id_news&status=$status_news'><IMG SRC='images/hide.png' WIDTH='28' ></A></td> ";
           
        }  ?></td>
        <td><A HREF="admin_del.php?id_del_news='<?php echo "$id_news";?>'&file_news='<?php echo "$name_news";?>'"><IMG SRC='../backend/images/delete.png' WIDTH='20'></A></td>
                            
                        </tr>
                        <?php
                            }
                        }
                    }else{
                        $thaimonth=array(
                            "00"=>"",
                            "01"=>"ม.ค.",
                            "02"=>"ก.พ.",
                            "03"=>"มี.ค.",
                            "04"=>"เม.ย.",
                            "05"=>"พ.ค.",
                            "06"=>"มิ.ย.",
                            "07"=>"ก.ค.",
                            "08"=>"ส.ค.",
                            "09"=>"ก.ย",
                            "10"=>"ต.ค.",
                            "11"=>"พ.ย.",
                            "12"=>"ธ.ค.");
                            $news_type=array(
                                "PR"=>"ประชาสัมพันธ์",
                                "ED"=>"ศึกษาต่อ",
                                "RE"=>"สมัคงาน",
                                "PRM"=>"ประชาสัมพันธ์หลักสูตร",
                                "AL"=>"ศิษย์เก่า",
                                );
                        $no=0;
                        $sql = "select * from tb_news WHERE `major_news` = '$sess_major' ORDER BY date_news DESC ";
                        $result = mysql_db_query($dbname, $sql);
                        $num = mysql_num_rows($result);
                        if ($num > 0) {
                            while ($r = mysql_fetch_array($result)) {
                                $id_news = $r[id_news];
                                $major_news = $r[major_news];
                                $title_news = $r[title_news];
                                $name_news = $r[name_news];
                                $status_news = $r[status_news];
                                $type_news = $r[type_news];
                                $date_news = $r[date_news];
                                $Date = explode("-", $date_news);
                                $date_news2 = $Date[2] . "/" . $thaimonth[$Date[1]] . "/" . ($Date[0]+543);
                                $no++
                                //<A HREF='files/$name_files'>$title_files</A>
                    
                            ?>
                             <th scope="row" class="text-center"><?php echo "$no" ?></th>
                            <td ><?php echo "$title_news" ?></td>
                            <td class="text-center"><?php echo "$news_type[$type_news]" ?></td>
                            <td ><?php echo "$date_news2" ?></td>
                            <td class="text-center" colspan="2"><?php  if ($status_news == '1') {
            echo "<A HREF='admin_news3.php?id_edit=$id_news&status=$status_news'><IMG SRC='images/show.png' WIDTH='28' ></A></td> ";
          
        } else {
            echo "<A HREF='admin_news3.php?id_edit=$id_news&status=$status_news'><IMG SRC='images/hide.png' WIDTH='28' ></A></td> ";
           
        }  ?></td>
        <td><A HREF="admin_del.php?id_del_news='<?php echo "$id_news";?>'&file_news='<?php echo "$name_news";?>'"><IMG SRC="images/del.png" WIDTH='20'></A></td>
                            
                        </tr>
                        <?php
                            }
                        }
                    }
                        ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.row -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.end content -->
</div>
<!-- /.content-wrapper -->
<?include 'f.php';?>