<?php include 'headerccc.php';?>
<?php include 'menubarccc.php';?>
<br>
<div class="inner-banner text-center">
    <div class="container">

        <div class="breadcumb-wrapper">
            <div class="clearfix">
                <div class="pull-left">
                    <ul class="list-inline link-list">
                        <li><i class="fa fa-picture-o"></i> <a href="#">สื่อความปลอดภัย</a></li>

                    </ul>
                </div>

            </div><!-- /.container -->
        </div>
    </div><!-- /.container -->
</div>




<section class="blog-section sec-padd" style="margin-top:-70px;">
    <div class="container">

        <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/lbrKKF0Lwso"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </figure>
                    <div class="content">
                        <h4 style="font-size:14px; margin-top:10px; line-height:25px;">การป้องกันตนเอง ในสถานการณ์
                            Covid-19</h4>
                            <div class="post-meta" align="right" style="font-size:14px; margin-top:10px; line-height:25px;">
                        <i class="fa fa-link"></i><a href="https://www.youtube.com/watch?v=lbrKKF0Lwso&feature=youtu.be" target="_blank"
                                rel="noopener noreferrer">ดูเพิ่มเติม</a></div>
                    </div>
                </div>

            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/rhdhRslMqSc"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </figure>
                    <div class="content">
                        <h4 style="font-size:14px; margin-top:10px; line-height:25px;">มาตรการป้องกัน Covid-19
                            ในสถานศึกษา
                        </h4>
                        <div class="post-meta" align="right" style="font-size:14px; margin-top:10px; line-height:25px;">
                        <i class="fa fa-link"></i><a href="https://www.youtube.com/watch?v=rhdhRslMqSc" target="_blank"
                                rel="noopener noreferrer">ดูเพิ่มเติม</a></div>
                    </div>
                </div>

            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/PsEAc8GPRlw"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </figure>
                    <div class="content">
                        <h4 style="font-size:14px; margin-top:10px; line-height:25px;">ระบบการคัดกรอง Covid-19
                            หอพักในมหาวิทยาลัย</h4>
                        <div class="post-meta" align="right" style="font-size:14px; margin-top:10px; line-height:25px;">
                        <i class="fa fa-link"></i><a href="https://www.youtube.com/watch?v=PsEAc8GPRlw" target="_blank"
                                rel="noopener noreferrer">ดูเพิ่มเติม</a></div>
                    </div>
                </div>

            </div>


        </div>


    </div>
</section>





<?include 'footerccc.php';?>