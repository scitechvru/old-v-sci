
<body>
    <section class="container">
        <div class="page-header">
            <h1>คณะวิทยาศาสตร์และเทคโนโลยี<br>
                <small>ขอแสดงความยินดีกับคณาจารย์ เนื่องในโอกาสที่รับการแต่งตั้งให้ดำรงตำแหน่งทางวิชาการ
                    <b>"ผู้ช่วยศาสตราจารย์"<b></small></h1>
        </div>
        <br>
        <div class="row">
            <!--style="height: 400px;width: 100%;"-->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder" >
                        <a><img src="images/3422.jpg"  style="height: 300px;width: 100%;" alt=""></a>  
                    </figure>
                    <div class="lower-content">
                        <div class="date">1</div>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">อาจารย์ ดร.เยาวภา แสงพยับ</a>
                        </h4>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">สาขา ฟิสิกส์</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder" >
                        <a><img src="images/07.jpg" style="height: 300px;width: 100%;" alt=""></a>  
                    </figure>
                    <div class="lower-content">
                        <div class="date">2</div>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">อาจารย์ กนกวรรณ ปุณณะตระกูล</a>
                        </h4>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">สาขา คหกรรมศาสตร์</a></h4>
                    </div>
                </div>
            </div><div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder" >
                        <a><img src="images/08.jpg" style="height: 300px;width: 100%;" alt=""></a>  
                    </figure>
                    <div class="lower-content">
                        <div class="date">3</div>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">อาจารย์ ณฐพงศ์ เมธินธรังสรรค์</a>
                        </h4>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">สาขา สัตววิทยา</a></h4>
                    </div>
                </div>
            </div><div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder" >
                        <a><img src="images/04.jpg" style="height: 300px;width: 100%;" alt=""></a>  
                    </figure>
                    <div class="lower-content">
                        <div class="date">4</div>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">อาจารย์ ดวงเดือน วัฏฏานุรักษ์</a>
                        </h4>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">สาขา เทคโนโลยีชีวภาพ</a></h4>
                    </div>
                </div>
            </div><div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder" >
                        <a><img src="images/02.jpg" style="height: 300px;width: 100%;" alt=""></a>  
                    </figure>
                    <div class="lower-content">
                        <div class="date">5</div>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">อาจารย์ มณทิพย์ จันทร์แก้ว</a>
                        </h4>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">สาขา วิทยาศาสตร์สิ่งแวดล้อม</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder" >
                        <a><img src="images/03.jpg" style="height: 300px;width: 100%;" alt=""></a>  
                    </figure>
                    <div class="lower-content">
                        <div class="date">6</div>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">อาจารย์ ดร.ตีรณรรถ ศรีสุนน์</a>
                        </h4>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">สาขา วิทยาศาสตร์สิ่งแวดล้อม</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder" >
                        <a><img src="images/98220.jpg" style="height: 300px;width: 100%;" alt=""></a>  
                    </figure>
                    <div class="lower-content">
                        <div class="date">7</div>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">อาจารย์ ดร.วีระวัฒน์ อุ่นเสน่หา</a>
                        </h4>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">สาขา เทคโนโลยีสิ่งแวดล้อม</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder" >
                        <a><img src="images/98219.jpg" style="height: 300px;width: 100%;" alt=""></a>  
                    </figure>
                    <div class="lower-content">
                        <div class="date">8</div>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">อาจารย์ ดร.ณัฐสิมา โทขันธ์</a>
                        </h4>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">สาขา เทคโนโลยีสิ่งแวดล้อม</a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated"
                    style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder" >
                        <a><img src="images/98218.jpg" style="height: 300px;width: 100%;" alt=""></a>  
                    </figure>
                    <div class="lower-content">
                        <div class="date">9</div>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">อาจารย์ ดร.ขนิษฐา ภมรพล</a>
                        </h4>
                        <h4><a style="font-size:14px; margin-top:10px; line-height:25px;">สาขา เทคโนโลยีสิ่งแวดล้อม</a></h4>
                    </div>
                </div>
            </div>
        </div>

            </div>
    </section>
</body>