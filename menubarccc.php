<section class="theme_menu stricky">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 menu-column">
                <nav class="menuzord" id="main_menu">
                    <ul class="menuzord-menu">
                        <li><a href="index.php">หน้าแรก</a></li>
                        <li><a href="#">เกี่ยวกับคณะ</a>
                            <ul class="dropdown">
                                <li><a href="history.php">ประวัติ</a></li>
                                <li><a href="vision.php">ปรัชญา วิสัยทัศน์ พันธกิจ</a></li>
                                <li><a href="tree.php">ค่านิยมหลัก</a></li>
                                <li><a href="#">โครงสร้างการบริหาร</a>
                                    <ul class="dropdown">
                                        <li><a href="structure.php">โครงสร้างองค์กร</a></li>
                                        <li><a href="board.php">ผู้บริหาร</a></li>

                                        <li><a href="per_aca.php">บุคลากรสายวิชาการ</a></li>
                                        <li><a href="per_sup.php">บุคลากรสายสนับสนุน</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">หน่วยงานภายใน</a>
                            <ul class="dropdown">
                                <?php
                                    $no = 0;
                                    require_once "inc/connect.php";
                                    $sql = "select * from tb_major where int_major != 'OF' and status_major='1' order by nameen_major";
                                    $result = $conn->query($sql) or die($conn->error);
                                   /*  print_r($result);
                                    die(); */
                                    if ($result->num_rows > 0){
                                        while ($row = $result->fetch_assoc()){
                                    /* $result = mysql_db_query($dbname, $sql);
                                    $num = mysql_num_rows($result);
                                    if ($num > 0) {
                                        while ($r = mysql_fetch_array($result)) {
                                            $nameth_major = $r[nameth_major];
                                            $link_major = $r[link_major];
                                            $no++; */
                                            ?>
                                <li><a href="<?php echo $row['link_major']; ?>"
                                        target="_blank"><?php echo $row['nameth_major']; ?></a></li>
                                <?php } //end while ?>

                                <?php } //end if ?>
                            </ul>
                        </li>
                                <li><a href="#">สถานศึกษาปลอดภัย</a>
                            <ul class="dropdown">
                                <li><a href="policysafety.php">นโยบายความปลอดภัยในสถานศึกษา</a></li>
                                <li><a href="valuessafety.php">ค่านิยมหลัก</a></li>
                                <li><a href="https://drive.google.com/file/d/1BDa-PRHmEk7Jb_FcW87hs-BgAboPDjQ_/view?usp=sharing" target="_blank" rel="noopener noreferrer">คู่มือด้านความปลอดภัยในสถานศึกษา</a></li>
                                <li><a href="#">ประกาศที่เกี่ยวข้อง</a>
                                    <ul class="dropdown">
                                        <li><a href="#">แผนงานและงบประมาณ ดำเนินการด้านความปลอดภัยในสถานศึกษา</a></li>
                                        <li><a href="https://drive.google.com/file/d/1pvlePNfymb0Cbl2RI7IkHT5tFCpKuHsq/view?usp=sharing" target="_blank" rel="noopener noreferrer">บุคลากรผู้ดูแลรับผิดชอบด้านความปลอดภัยในสถานศึกษา</a></li>
                                        <li><a href="https://drive.google.com/file/d/1ZkHmKXUyPQEv43KY2QA-1r5snJnlwsmn/view?usp=sharing" target="_blank" rel="noopener noreferrer">คณะกรรมการความปลอดภัยในสถานศึกษา</a></li>
                                    </ul>
                                </li>

                                <li><a href="https://drive.google.com/drive/folders/1joPTBu61VGL78MI_P9BUqHuoIcfPlu9B?usp=sharing" target="_blank" rel="noopener noreferrer">แผนการป้องกันและระงับอัคคีภัย</a></li>
                                <li><a href="#">กิจกรรมรณรงค์ ส่งเสริมด้านความปลอดภัย</a>
                                    <ul class="dropdown">
                                        <li><a href="#">นิทรรศการเทิดพระเกียรติสถาบันพระมหากษัตริย์</a></li>
                                        <li><a
                                                href="#">กิจกรรมส่งเสริมความสะอาดและความเป็นระเบียบเรียบร้อยของสถานศึกษา</a>
                                        </li>
                                        <li><a href="#">กิจกรรมรับฟังความคิดเห็นและข้อเสนอแนะด้านความปลอดภัย</a></li>
                                        <li><a href="#">กิจกรรมชมรมจป.น้อย</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">หลักสูตรอบรมด้านความปลอดภัย อาชีวอนามัยและสภาพแวดล้อมในการทำงาน</a>
                                    <ul class="dropdown">
                                        <li><a
                                                href="#">หลักสูตรทักษะการโค้ชเพื่อพัฒนาศักยภาพภายในตนเองและการสื่อสารเพื่อสร้างความสัมพันธ์และจิตสำนึกความปลอดภัย</a>
                                        </li>
                                        <li><a href="#">หลักสูตรการป้องกันอัคคีภัยในสถานศึกษา</a></li>
                                        <li><a href="#">หลักสูตรการส่งเสริมพฤติกรรมความปลอดภัยในสถานศึกษา (BBS)</a></li>
                                        <li><a href="#">หลักสูตรความปลอดภัยในห้องปฏิบัติการ</a></li>
                                        <li><a href="#">หลักสูตรอบรมปฐมพยาบาลเบื้องต้น</a></li>
                                        <li><a href="#">หลักสูตรอบรมดับเพลิงขั้นต้นและฝึกซ้อมอพยพหนีไฟ</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">การตรวจความปลอดภัย</a></li>
                                <li><a href="#">สถิติการเกิดอุบัติเหตุและการเจ็บป่วยในสถานศึกษา</a></li>
                                <li><a href="videosafety.php">สื่อความปลอดภัย</a></li>
                            </ul>
                        </li>
                        <li><a href="#">ติดต่อ</a>
                            <ul class="dropdown">
                                <li><a href="contect_t.php">ช่องทางการติดต่ออาจารย์</a></li>
                                <li><a href="hotline.php">สายตรงผู้บริหาร</a></li>
                                <li><a href="contact.php">ติดต่อคณะ</a></li>
                            </ul>
                        </li>
                            </ul>
                           
                        </li>
                        
                        <li><a href="#">แบบฟอร์ม / คู่มือ</a>
                            <ul class="dropdown">
                                <li><a href="form_student.php">นักศึกษา</a></li>
                                <li><a href="manual_students.php">คู่มือนักศึกษา</a></li>
                                <li><a href="form_personal.php">อาจารย์/เจ้าหน้าที่</a></li>
                                <li><a href="form_reg.php">ระเบียบ/ข้อบังคับ/กฎหมาย</a></li>
                                <li><a href="form_research.php">วิจัย</a></li>
                                <li><a href="form_information.php">สารสนเทศ</a></li>
                                <li><a href="form_qua.php">ประกันคุณภาพ</a></li>
                                <li><a href="#">วารสารJRIST</a>
                                <ul class="dropdown">
                                <li><a href="from_print.php">ระเบียบการตีพิมพ์</a></li>
                                <li><a href="from_aa.php">รูปแบบบทความวิชาการ</a></li>
                                <li><a href="from_ra.php">รูปแบบบทความวิจัย</a></li>
                                <!--<li><a href="submid_from.php">ใบนำส่งบทความฯ</a></li>-->
                                </ul>
                                </li>
                                <!-- <li><a href="form_mua.php">คู่มือการใช้งานทั้งหมด</a></li>-->
                            </ul>
                        </li>


                        <li><a href="#">วาระการประชุม</a>
                        
                        <ul class="dropdown">
                        <li><a href="report.php?type=1">กรรมการวิชาการคณะ</a></li>
                        <li><a href="report.php?type=2">กรรมการบริหารคณะ</a></li>
                        <li><a href="report.php?type=3">คณบดีสัญจร</a></li>
                        </ul></li>
                        <li><a href="ita.php">ITA</a></li>
                        <li><a href="#">จัดซื้อจัดจ้าง</a>
                            <ul class="dropdown">
                                <li><a href="http://procurement.vru.ac.th/index.php?type=1">จัดซื้อจัดจ้าง</a></li>
                                <li><a href="http://procurement.vru.ac.th/index.php?type=3">ประกาศผู้ชนะ</a></li>
                                <li><a href="http://www.vru.ac.th/shownewsType5.php">ประกาศประกวดราคา</a></li>
                            </ul>
                        </li>
                        <li><a href="#">แผนยุทธศาสตร์</a>
                            <ul class="dropdown">
                                <li><a
                                        href="http://plan.vru.ac.th/?page_id=3809">ปฏิทินการดำเนินงานการจัดทำแผนปฏิบัติการ</a>
                                </li>
                                <li><a href="http://plan.vru.ac.th/?page_id=5960">แผนกลยุทธ์ Strategy Plan</a></li>
                                <li><a href="http://plan.vru.ac.th/?page_id=3302">แผนปฏิทินราชการ Action Plan</a></li>
                                <li><a href="http://plan.vru.ac.th/?page_id=2756">ตัวชี้วัดแผลกลยุทธ์และปฏิบัติการ</a>
                                </li>
                                <li><a href="http://plan.vru.ac.th/?page_id=3710">ยุทธศาสตร์
                                        มหาวิทยาลัยราชภัฎเพื่อการพัฒนาท้องถิ่น ระยะ 20 ปี</a></li>
                                <li><a href="http://plan.vru.ac.th/?page_id=3642">แผนยุทธศาสตร์
                                        มหาวิทยาลัยราชภัฎวไลยอลงกรณ์ ระยะ 20 ปี</a></li>
                                <li><a href="http://plan.vru.ac.th/?page_id=5864">แผนยุทธศาสตร์ระดับคณะและหน่วยงาน</a>
                                </li>
                            </ul>
                        </li>

                       <!-- <li><a href="#">งานศิลปวัฒนธรรม</a>
                            <ul class="dropdown">
                                <li><a href="artfile.php?type_news=ART">แผนงานด้านศิลปวัฒนธรรมและความเป็นไทย</a></li>
                                <li><a href="artactive.php">ภาพกิจกรรม</a></li>
                            </ul>
                        </li>-->
                        
                        <li><a href="https://bit.ly/33VSqhl"  target="_blank" rel="noopener noreferrer">วารสารJRIST</a></li>
                     <li><a href="http://www.astcconference.com"  target="_blank" rel="noopener noreferrer">ASTC ครั้งที่ 8</a></li>
                    </ul><!-- End of  -->
                    <ul class="menuzord-menu menuzord-right">
                    <li><a href="#">ระบบออนไลน์</a>
                            <ul class="dropdown">
                              <li><a href="https://forms.gle/kWA2SFtkCJCLyQsEA" target="_blank" rel="noopener noreferrer">จองห้องประชุมออนไลน์</a></li>
                              <li><a href="https://docs.google.com/forms/d/e/1FAIpQLSdoKghxi2EAQWhmLJtin7C3rlJHGX-4bQMrNiJtc6aZe84Fig/viewform" target="_blank" rel="noopener noreferrer">จองห้องปฏิบัติการทักษะกระบวนการทางวิทยาศาสตร์ (ห้อง 5103)</a></li>
                              
                            </ul>
                        </li>
                        <!--<li><a href="https://forms.gle/Vo9ZvxxnxKcfQFfq7">แบบฟอร์มขอข้อมูลหลักสูตร</a></li>-->
                        <li><a href="https://forms.gle/4WYGkwpKAhX387Du6">สมัครเรียน</a></li>

                        <!--<li><a href="#">E-Service</a>
                            <ul class="dropdown">
                                <li><a href="registercourse/index.php">Ecoz</a></li>
                                <li><a href="ITA/index.php">Q&A</a></li>
                                <li><a href="register_sm.php">Register</a></li>
                               <li><a href="item/login.php">ระบบยืม-คืนอุปกรณ์ Online</a></li>
                            </ul>
                        </li>-->
                        <li><a href="#">เข้าสู่ระบบ</a>
                            <ul class="dropdown">
                              <li><a href="user/login.php">อาจารย์และเจ้าหน้าที่</a></li>
                                <li><a href="backend/index.php">ผู้ดูแลระบบ</a></li>
                                <li><a href="backend/report_admin/login.php">รายงานการประชุม</a></li>
                            </ul>
                        </li>

                    </ul>


                </nav> <!-- End of #main_menu -->
            </div>
        </div>
    </div> <!-- End of .conatiner -->
</section>