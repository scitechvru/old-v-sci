<?php include 'headerccc.php';?>
<?php include 'menubarccc.php';?>
<div class="inner-banner text-center">
    <div class="container">
        <!--  <div class="box">
            <img src=""  alt="">
        </div>
      -->
        <!-- /.box -->
        <div class="breadcumb-wrapper">
            <div class="clearfix">
                <div class="pull-left">
                    <ul class="list-inline link-list">
                        <li><i class="fa fa-lightbulb-o"></i> ITA</li>

                    </ul>
                </div>

            </div><!-- /.container -->
        </div>
    </div><!-- /.container -->
</div>
<section class="service">
    <div class="container">
        <div class="section-title">
            <h2>ITA นโยบายการป้องกันการทุจริตของมหาวิทยาลัยราชภัฏวไลยอลงกรณ์</h2>
        </div>
        <p><strong>ตารางการดำเนินงานคณะกรรมการดำเนินงาน รับการประเมินคุณธรรมและความโปร่งใส<br>
                คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์</strong></p>
        <div class="table-responsive-xl">
            <table class="table">
                <thead>
                    <tr>
                    <th class="text-center">ลำดับ</th>
                        <th colspan="2" class="text-center">ตัวชี้วัด/ประเด็นคำถาม</th>
                        <th colspan="2" class="text-center">องค์ประกอบด้านข้อมูล</th>
                        <th  colspan="2"  class="text-center">ลิงค์หรือ URL ข้อมูล</th>

                    </tr>
                </thead>
                <tbody>
                <?php
                                $no=0;
                                $sql = "SELECT * FROM ita_head ih  ORDER BY ih.id_ita ASC";
                                $result = mysql_db_query($dbname, $sql);
                                $num = mysql_num_rows($result);
                                if ($num > 0) {
                                    while ($r = mysql_fetch_array($result)) {
                                        $id_ita = $r[id_ita];
                                        $sub_ita = $r[sub_ita];
                                        $detail = $r[detail];
                                        $no++
                               ?>
                        <tr>

                            <td class="text-center"><?php echo "O$id_ita";?></td>
                            <td class="text-left" colspan="2"><?php echo "$sub_ita";?></td>
                            <td class="text-left" colspan="2"><?php echo "$detail";?></td>
                            

                            <td colspan="2"  class="text-left">
                         <?php
                           $sql2 = "SELECT * FROM ita_detail  WHERE id_ita='$id_ita' ";
                           $result2 = mysql_db_query($dbname, $sql2);
                           $num2 = mysql_num_rows($result2);
                           if ($num2 > 0) {
                               while ($r2 = mysql_fetch_array($result2)) {
                                   $name =$r2[name];
                                   $file =$r2[file];
                                   $link =$r2[link];
                                   $no++
                         ?>
                                <?php
                                if($link){?>
                                <li><a href="<?php echo "$link";?>" target="_blank"><?php echo "$name";?></a></li>
                                <?php }else{?> 
                                <li><a href="../backend/ita/file_ita/<?php echo "$file";?>"
                                        target="_blank"><?php echo "$name";?></a></li>
                                <?php }?>
                                <?php  }}?>
                            </td>
                            
                        </tr>
                        <?php  }}?>


                    </tbody>
                </table>
        </div>
</section>

</div>
<?php include 'footerccc.php';?>