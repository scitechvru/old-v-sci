<?php /* include "home.php";  */

header( "refresh: 0; url=ScitechVRU/index.php" );
exit(0);

?>
<!--
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SCI</title>


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="images/fav-icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="images/fav-icon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/fav-icon/favicon-16x16.png" sizes="16x16">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <style>
    body {
        text-align: center
    }

    figure {
        display: table;
        margin: 0 auto;
    }

    img {
        width: 100%;
        height: auto;
    }

    html,
    body {
        margin: 0;
        font: 400 16px/1.5 exo, ubuntu, "segoe ui", helvetica, arial, sans-serif;
        padding: 0;
        width: 100%;
        height: 100vh;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        background: #ffff;
        text-align: center;
    }

    .glow-on-hover {
        width: 220px;
        height: 50px;
        border: none;
        outline: none;
        color: #fff;
        background: #e70000;
        cursor: pointer;
        position: relative;
        z-index: 0;
        border-radius: 10px;
    }

    .glow-on-hover:before {
        content: '';
        background: linear-gradient(45deg, #ff0000, #ff7300, #fffb00, #48ff00, #00ffd5, #002bff, #7a00ff, #ff00c8, #ff0000);
        position: absolute;
        top: -2px;
        left: -2px;
        background-size: 400%;
        z-index: -1;
        filter: blur(5px);
        width: calc(100% + 4px);
        height: calc(100% + 4px);
        animation: glowing 20s linear infinite;
        opacity: 0;
        transition: opacity .3s ease-in-out;
        border-radius: 10px;
    }

    .glow-on-hover:active {
        color: #000
    }

    .glow-on-hover:active:after {
        background: transparent;
    }

    .glow-on-hover:hover:before {
        opacity: 1;
    }

    .glow-on-hover:after {
        z-index: -1;
        content: '';
        position: absolute;
        width: 100%;
        height: 100%;
        background: #111;
        left: 0;
        top: 0;
        border-radius: 10px;
    }

    @keyframes glowing {
        0% {
            background-position: 0 0;
        }

        50% {
            background-position: 400% 0;
        }

        100% {
            background-position: 0 0;
        }
    }
    </style>
</head>

<body>

    <div class="container-fluid shadow-lg p-3 mb-5 bg-white rounded ">
        <br><br>
        <img src="introweb/qu.jpg" class="img-fluid" alt="Responsive image">
        <br>
        <div class="container">
            <br>

            <p class="text-justify text-center">
                เนื่องในโอกาสวันคล้ายวันประสูติ
                สมเด็จพระเจ้าลูกเธอ เจ้าฟ้าสิริวัณณวรี นารีรัตนราชกัญญา
                ๘ มกราคม ๒๕๖๔ ทรงเจริญพระชันษา ๓๔ ปี
                ขอพระองค์ทรงพระเกษมสำราญ พระพลานามัยสมบูรณ์แข็งแรง<br>ด้วยเกล้าด้วยกระหม่อม ขอเดชะ ข้าพระพุทธเจ้า
                คณะผู้บริหาร บุคลากรและนักศึกษา คณะวิทยาศาสตร์และเทคโนโลยี <br> มหาวิทยาลัยราชภัฏวไลยอลงกรณ์
                ในพระบรมราชูปถัมภ์
                จังหวัดปทุมธานี</p>

            <br>
            <button class="glow-on-hover" type="button"
                onclick="window.location.href='home.php'">เข้าสู่เว็บไซต์</button>
        </div>




    </div>
-->
    <!--<p>เนื่องในโอกาสวันคล้ายวันพระราชสมภพ สมเด็จพระกนิษฐาธิราชเจ้า กรมสมเด็จพระเทพรัตนราชสุดาฯ สยามบรมราชกุมารี วันที่ 2 เมษายน 2563 
คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ ขอน้อมเกล้าฯ ถวายพระพร ขอพระองค์ทรงพระเจริญ ยิ่งยืนนาน</p>
<img src="introweb/index.jpg" class="img-fluid" alt="Responsive image">-->
<!--
</body>

</html>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>

-->





