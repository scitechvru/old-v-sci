
<?php
include 'headerccc.php';
include 'menubarccc.php';
require 'connect.php';
//require 'inc/connect.php';
include 'backend/educationlist.php';
include "backend/array.php";
?>

<style>
.our-team {
    border: 1px solid #d3d3d3;
    position: relative;
    overflow: hidden;
}

.our-team img {
    width: 100%;
    height: 100%;
}

.our-team .team-content {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    padding: 45px 18px;
    background: rgba(0, 0, 0, 0.7);
    transform: translateX(-100%);
    transition: all 0.20s ease 0s;
}

.our-team:hover .team-content {
    transform: translateX(0);
}

.our-team .team-content .post-title {
    font-size: 18px;
    color: #fff;
    text-transform: uppercase;
}

.our-team .team-content .post {
    font-size: 14px;
    color: #cb95e1;
    display: block;
    margin-bottom: 20px;
}

.our-team .description {
    font-size: 14px;
    line-height: 25px;
    color: #fff;
    margin-bottom: 20px;
}

.our-team .team_social {
    margin: 0;
    padding: 0;
    list-style: none;
}

.our-team .team_social li {
    display: inline-block;
    margin-right: 5px;
}

.our-team .team_social li a {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    border: 2px solid #f5f5f5;
    font-size: 17px;
    color: #f5f5f5;
    line-height: 40px;
    text-align: center;
    display: inline-block;
    transition: border 0.3s ease 0s;
}

.our-team .team_social li a:hover {
    border-color: transparent;
}

.our-team .team-prof {
    width: 100%;
    position: absolute;
    bottom: 0;
    text-align: right;
    padding: 20px 16px;
    background: rgba(0, 0, 0, 0.7);
    transform: translateX(0);
    transition: all 0.20s ease 0s;
}

.our-team:hover .team-prof {
    transform: translateX(100%);
}

.our-team .team-prof .post-title {
    font-size: 14px;
    color: #fff;
    margin: 0 0 8px 0;
    text-transform: uppercase;
}

.our-team .team-prof .post {
    font-size: 14px;
    color: #cb95e1;
    margin-bottom: 0;
}

@media only screen and (max-width: 990px) {
    .our-team {
        margin-bottom: 20px;
    }
}
</style>
<div class="container">
    <div class="row">
	<h1 class="text-center">บุคลากรสายสนับสนุน</h1>
	<h3 class="text-center">สำนักงานคณบดี</h3>
        <?php $no = 0;
$sql2 = "SELECT * from tb_per where (type_per='2' or type_per='4' or type_per='5')and major_per='OF' order by fnamet_per ASC";
$result2 = mysql_db_query($dbname, $sql2);
$num2 = mysql_num_rows($result2);
if ($num2 > 0) {
    while ($r2 = mysql_fetch_array($result2)) {
        $id_per = $r2[id_per];
        $idcard_per = $r2[idcard_per];
        $tname2_per = $r2[tname2_per];
        $tname1_per = $r2[tname1_per];
        $tname_per = $r2[tname_per];
        $fnamet_per = $r2[fnamet_per];
        $email_per = $r2[email_per];
        $lnamet_per = $r2[lnamet_per];
        $major_per = $r2[major_per];
        $type_per = $r2[type_per];
        $img_per = $r2[img_per];
        $status_per = $r2[status_per];

        if ($tname2_per or $tname1_per) {
            $tname_per = $tname2_per.$tname1_per;
        }

        ++$no;
        if ($img_per) { ?>

        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <img src="./backend/img_resize/<?php echo "$img_per"; ?>"
                style="width:300px;height:350px;" alt="<?php echo "$tname_per $fnamet_per  $lnamet_per"; ?>">
                <div class="team-content">
                    <h3 class="post-title"><?php echo "$tname_per $fnamet_per  $lnamet_per"; ?></h3>
                    <span class="post"><?php echo "$major[$major_per]"; ?></span>
                    <span class="post"><?php echo "($email_per)"; ?></span>
                    
                </div>
                <div class="team-prof">
                    <h3 class="post-title"><?php echo "$tname_per $fnamet_per  $lnamet_per"; ?></h3><br>
                    <span class="post"><?php echo "$major[$major_per]"; ?></span><br>
                    <span class="post"><?php echo "($email_per)"; ?></span>
                </div>
            </div>
        </div>
        


        <?php         } // end while
    }// end while
}// end if

?>
    </div>
</div>
<div class="container">
	<div class="row">
	
	<h3 class="text-center">ศูนย์วิทยาศาสตร์</h3>
        <?php $no = 0;
$sql2 = "SELECT * from tb_per where (type_per='2' or type_per='4' or type_per='5')and major_per='SCC' order by fnamet_per ASC";
$result2 = mysql_db_query($dbname, $sql2);
$num2 = mysql_num_rows($result2);
if ($num2 > 0) {
    while ($r2 = mysql_fetch_array($result2)) {
        $id_per = $r2[id_per];
        $idcard_per = $r2[idcard_per];
        $tname2_per = $r2[tname2_per];
        $tname1_per = $r2[tname1_per];
        $tname_per = $r2[tname_per];
        $fnamet_per = $r2[fnamet_per];
        $email_per = $r2[email_per];
        $lnamet_per = $r2[lnamet_per];
        $major_per = $r2[major_per];
        $type_per = $r2[type_per];
        $img_per = $r2[img_per];
        $status_per = $r2[status_per];

        if ($tname2_per or $tname1_per) {
            $tname_per = $tname2_per.$tname1_per;
        }

        ++$no;
        if ($img_per) { ?>

        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <img src="./backend/img_resize/<?php echo "$img_per"; ?>"
                style="width:300px;height:350px;" alt="<?php echo "$tname_per $fnamet_per  $lnamet_per"; ?>">
                <div class="team-content">
                    <h3 class="post-title"><?php echo "$tname_per $fnamet_per  $lnamet_per"; ?></h3>
                    <span class="post"><?php echo "$major[$major_per]"; ?></span>
                    <span class="post"><?php echo "($email_per)"; ?></span>
                    
                </div>
                <div class="team-prof">
                    <h3 class="post-title"><?php echo "$tname_per $fnamet_per  $lnamet_per"; ?></h3><br>
                    <span class="post"><?php echo "$major[$major_per]"; ?></span><br>
                    <span class="post"><?php echo "($email_per)"; ?></span>
                </div>
            </div>
        </div>
        


        <?php         } // end while
    }// end while
}// end if

?>
	</div>
</div>
<div class="container">
	<div class="row">
		
	<h3 class="text-center">เจ้าหน้าที่ประจำหลักสูตร</h3>
        <?php $no = 0;
$sql2 = "SELECT * from tb_per where (type_per='2' and (major_per='IT' or major_per='CS' or major_per='HE' or major_per='BIOT'or major_per='NU'or major_per='SET'or major_per='OHS')) ";
$result2 = mysql_db_query($dbname, $sql2);
$num2 = mysql_num_rows($result2);
if ($num2 > 0) {
    while ($r2 = mysql_fetch_array($result2)) {
        $id_per = $r2[id_per];
        $idcard_per = $r2[idcard_per];
        $tname2_per = $r2[tname2_per];
        $tname1_per = $r2[tname1_per];
        $tname_per = $r2[tname_per];
        $fnamet_per = $r2[fnamet_per];
        $email_per = $r2[email_per];
        $lnamet_per = $r2[lnamet_per];
        $major_per = $r2[major_per];
        $type_per = $r2[type_per];
        $img_per = $r2[img_per];
        $status_per = $r2[status_per];

        if ($tname2_per or $tname1_per) {
            $tname_per = $tname2_per.$tname1_per;
        }

        ++$no;
        if ($img_per) { ?>

        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <img src="./backend/img_resize/<?php echo "$img_per"; ?>"
                style="width:300px;height:350px;" alt="<?php echo "$tname_per $fnamet_per  $lnamet_per"; ?>">
                <div class="team-content">
                    <h3 class="post-title"><?php echo "$tname_per $fnamet_per  $lnamet_per"; ?></h3>
                    <span class="post"><?php echo "$major[$major_per]"; ?></span>
                    <span class="post"><?php echo "($email_per)"; ?></span>
                    
                </div>
                <div class="team-prof">
                    <h3 class="post-title"><?php echo "$tname_per $fnamet_per  $lnamet_per"; ?></h3><br>
                    <span class="post"><?php echo "$major[$major_per]"; ?></span><br>
                    <span class="post"><?php echo "($email_per)"; ?></span>
                </div>
            </div>
        </div>
        


        <?php         } // end while
    }// end while
}// end if

?>
	</div>
</div>

<?php include 'footerccc.php'; ?>
