<?php include "headerccc.php"?>
<?php include "menubarccc.php"?>
<br>
<style>
.main-timeline {
    font-family: 'Roboto', sans-serif;
}

.main-timeline:after {
    content: '';
    display: block;
    clear: both;
}

.main-timeline .timeline {
    width: 50%;
    padding: 95px 0 0 0;
    margin: 0 50px 20px 0;
    float: left;
    position: relative;
}

.main-timeline .timeline:after {
    content: '';
    background-color: #fff;
    height: 110px;
    width: 110px;
    border: 1px solid #333;
    border-radius: 50%;
    position: absolute;
    right: -56px;
    top: 20px;
}

.main-timeline .timeline-content {
    padding: 10px 60px 10px 10px;
    border-top: 1px solid #555;
    display: block;
    position: relative;
}

.main-timeline .timeline-content:hover {
    text-decoration: none;
}

.main-timeline .timeline-year {
    color: #fff;
    background-color: #02A89E;
    font-size: 32px;
    font-weight: 900;
    text-align: center;
    line-height: 98px;
    height: 100px;
    width: 100px;
    border-radius: 50%;
    box-shadow: 10px 10px 0 #019187, 20px 20px 0 #019187;
    position: absolute;
    right: -31px;
    top: -92px;
    z-index: 1;
}

.main-timeline .timeline-icon {
    color: #555;
    font-size: 35px;
    position: absolute;
    bottom: 0;
    right: 10px;
}

.main-timeline .title {
    color: #02A89E;
    font-size: 20px;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 1px;
    margin: 0 0 7px 0;
}

.main-timeline .description {
    color: #555;
    font-size: 15px;
    letter-spacing: 1px;
    text-align: justify;
    margin: 0 0 5px;
}

.main-timeline .timeline:nth-child(even) {
    padding: 95px 0 0;
    margin: 0 0 20px 50px;
    float: right;
}

.main-timeline .timeline:nth-child(even):after {
    right: auto;
    left: -56px;
}

.main-timeline .timeline:nth-child(even) .timeline-content {
    padding: 10px 10px 10px 60px;
}

.main-timeline .timeline:nth-child(even) .timeline-year {
    right: auto;
    left: -71px;
    top: -91px;
}

.main-timeline .timeline:nth-child(even) .timeline-icon {
    right: auto;
    left: 7px;
}

.timeline:nth-child(4n+2) .timeline-year {
    background-color: #9F005D;
    box-shadow: 10px 10px 0 #8C0057, 20px 20px 0 #8C0057;
}

.timeline:nth-child(4n+2) .title {
    color: #9F005D;
}

.timeline:nth-child(4n+3) .timeline-year {
    background-color: #28A9E2;
    box-shadow: 10px 10px 0 #1594C1, 20px 20px 0 #1594C1;
}

.timeline:nth-child(4n+3) .title {
    color: #28A9E2;
}

.timeline:nth-child(4n+4) .timeline-year {
    background-color: #23B574;
    box-shadow: 10px 10px 0 #1B9C63, 20px 20px 0 #1B9C63;
}

.timeline:nth-child(4n+4) .title {
    color: #23B574;
}

@media screen and (max-width:767px) {

    .main-timeline .timeline,
    .main-timeline .timeline:nth-child(even) {
        width: 100%;
        padding: 60px 0 0;
        margin-bottom: 20px;
    }
}
</style>
<!--///////////////////////////////////////////////////////////////////////////////////////////////-->
<div class="container">
    <h1 class="text-center font-weight-bold">History</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="main-timeline">
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>2475</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <h3 class="title">เมื่อปี พ.ศ. 2475</h3>
                        <p class="description">
                            คณะวิทยาศาสตร์และเทคโนโลยีมีบทบาทหน้าที่ในการจัดการศึกษาด้านวิทยาศาสตร์และเทคโนโลยีมาอย่างต่อเนื่องตั้งแต่เริ่มก่อตั้งเป็นโรงเรียนฝึกหัดครูเพชรบุรีวิทยาลงกรณ์โดยคณะวิทยาศาสตร์และเทคโนโลยีมีฐานะเป็นหมวดวิชาวิทยาศาสตร์ในวิทยาลัยครูเพชรบุรีวิทยาลงกรณ์
                        </p>
                    </a>
                </div>
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>2513</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <h3 class="title">เมื่อปี พ.ศ. 2513</h3>
                        <p class="description">
                            ได้จัดการศึกษาด้านวิทยาศาสตร์
                            ตั้งแต่ระดับประกาศนียบัตรวิชาการศึกษาและระดับประกาศนียบัตรวิชาการศึกษาชั้นสูง
                        </p>
                    </a>
                </div>
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>2520</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <h3 class="title">ปี พ.ศ. 2520</h3>
                        <p class="description">
                            หมวดวิชาวิทยาศาสตร์ได้เปลี่ยนฐานะเป็นคณะวิชาวิทยาศาสตร์<br>และเทคโนโลยี&nbsp;ตามข้อบังคับของพระราชบัญญัติวิทยาลัยครู
                            พ.ศ. 2518 ได้รวมหมวดวิชาต่างๆที่มีอยู่เดิมเข้าไว้ด้วยกันได้แก่<br>&nbsp;&nbsp;&nbsp;-
                            หมวดวิชาวิทยาศาสตร์<br>
                            &nbsp;&nbsp;&nbsp;- หมวดวิชาคณิตศาสตร์<br>&nbsp;&nbsp;&nbsp;-
                            หมวดวิชาพลศึกษาและสุขศึกษา<br>&nbsp;&nbsp;&nbsp;- หมวดวิชาคหกรรมศาสตร์<br>
                            &nbsp;&nbsp;&nbsp;- หมวดวิชาหัตถศึกษา<br>&nbsp;&nbsp;&nbsp;-
                            หมวดวิชาเกษตรศาสตร์<br>โครงสร้างการบริหารมีหัวหน้าคณะเป็นหัวหน้าหน่วยงาน
                            ส่วนภายในคณะประกอบด้วยภาควิชาซึ่งบริหารโดยหัวหน้าภาควิชา ซึ่งมีจำนวนทั้งสิ้น 9
                            ภาควิชาได้แก่<br>
                            &nbsp;&nbsp;&nbsp;- ภาควิชาเกษตรศาสตร์<br>&nbsp;&nbsp;&nbsp;-
                            ภาควิชาคณิตศาสตร์<br>&nbsp;&nbsp;&nbsp;- ภาควิชาเคมี<br>&nbsp;&nbsp;&nbsp;-
                            ภาควิชาชีววิทยา<br>
                            &nbsp;&nbsp;&nbsp;- ภาควิชาฟิสิกส์และวิทยาศาสตร์ทั่วไป<br>&nbsp;&nbsp;&nbsp;-
                            ภาควิชาคหกรรมศาสตร์<br>&nbsp;&nbsp;&nbsp;- ภาควิชาพลศึกษา<br>&nbsp;&nbsp;&nbsp;-
                            ภาควิชาสุขศึกษา<br>&nbsp;&nbsp;&nbsp;- ภาควิชาอุตสาหกรรมศิลป์<br>
                            คณะวิชาวิทยาศาสตร์และเทคโนโลยีได้จัดการศึกษาโดยผลิตบัณฑิตครูวิทยาศาสตร์และคณิตศาสตร์ในระดับประกาศนียบัตรวิชาการศึกษา
                            (ป.กศ.) ประกาศนียบัตรวิชาการศึกษาชั้นสูง (ป.กศ.สูง) และปริญญาตรีครุศาสตรบัณฑิต
                        </p>
                    </a>
                </div>
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>2523</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <h3 class="title">ปี พ.ศ. 2523</h3>
                        <p class="description">
                            ได้ยกเลิกการผลิตสาขาการศึกษาระดับประกาศนียบัตรวิชาการศึกษา
                            แต่ยังคงจัดการศึกษาเฉพาะระดับประกาศนียบัตรวิชาการศึกษาชั้นสูงและระดับปริญญาตรี
                        </p>
                    </a>
                </div>
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>2535</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <h3 class="title">เมื่อวันที่ 14 กุมภาพันธ์ 2535</h3>
                        <p class="description">
                            พระบาทสมเด็จพระเจ้าอยู่หัว ทรงพระกรุณาโปรดเกล้าโปรดกระหม่อมพระราชทานนาม
                            สถาบันราชภัฏแก่วิทยาลัยครูทั่วประเทศดังนั้นวิทยาลัยครูเพชรบุรีวิทยาลงกรณ์ในพระบรมราชูปถัมภ์
                            จึงใช้นามว่า สถาบันราชภัฏเพชรบุรีวิทยาลงกรณ์ในพระบรมราชูปถัมภ์และในปี พ.ศ.2538
                            พระราชบัญญัติสถาบันราชภัฏได้ปรับเปลี่ยนคณะวิชาวิทยาศาสตร์และเทคโนโลยีเป็นคณะวิทยาศาสตร์และเทคโนโลยี
                            และผู้บริหารคณะเปลี่ยนจากตำแหน่งหัวหน้าคณะวิชาเป็นตำแหน่งคณบดี
                        </p>
                    </a>
                </div>
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>2540</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <h3 class="title">ปี พ.ศ. 2540</h3>
                        <p class="description">
                            การบริหารงานเปลี่ยนแปลงจากรูปแบบภาควิชามาเป็นการบริหารแบบโปรแกรมวิชา
                        </p>
                    </a>
                </div>
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>2547</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <h3 class="title">วันที่ 14 มิถุนายน 2547</h3>
                        <p class="description">
                            ได้มีการประกาศใช้พระราชบัญญัติมหาวิทยาลัยราชภัฏพุทธศักราช 2547 โดยประกาศในราชกิจจานุเบกษา
                            ลงวันที่ 14 มิถุนายน 2547
                            เป็นผลให้สถาบันราชภัฏวไลยอลงกรณ์ในพระบรมราชูปถัมภ์เปลี่ยนเป็นมหาวิทยาลัยราชภัฏวไลยอลงกรณ์
                            ในพระบรมราชูปถัมภ์ จังหวัดปทุมธานี
                        </p>
                    </a>
                </div>
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>2548</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <h3 class="title">ปี พ.ศ. 2548</h3>
                        <p class="description">
                            ในภาคเรียนที่ 2 ปีการศึกษา 2548
                            ได้เปลี่ยนแปลงการบริหารจากโปรแกรมวิชามาเป็นการบริหารแบบสาขาวิชาและหลักสูตรโดยมีคณะกรรมการบริหารหลักสูตรและสาขาวิชาเป็นผู้บริหารจัดการศึกษาภายในหลักสูตรและสาขาวิชาจวบจนถึงปัจจุบัน
                        </p>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>
<hr>

<?include "history2.php"?>
</div>
</div>

<BR><BR>
<?php include "footerccc.php" ?>