<?php 

$file_name = basename($_SERVER['SCRIPT_FILENAME'],".php");

?>

<!-- Section Navbar -->
<section>
        <nav id="navbar" class="navbar navbar-expand-lg fixed-top navbar-dark bg-alpha">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="assets/image/logo.png" width="35" height="35" class="d-inline-block align-top" alt="ScitechVRU"> Scitech VRU
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarKey" aria-controls="navbarKey" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarKey">
                    <ul class="navbar-nav ml-auto text-center">
                        <li class="nav-item <?php echo $file_name == 'index' ? 'active': '' ?>">
                            <a class="nav-link" href="index.php">หน้าหลัก</a>
                        </li>
                        <li class="nav-item <?php echo $file_name == 'about' ? 'active': '' ?>">
                            <a class="nav-link" href="about.php">เกี่ยวกับเรา</a>
                        </li>
                        <li class="nav-item <?php echo $file_name == 'blog' || $file_name == 'blog-detail' ? 'active': '' ?>">
                            <a class="nav-link" href="blog.php">ข่าวสารและกิจกรรม</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://forms.gle/4WYGkwpKAhX387Du6" target="_blank" rel="noopener noreferrer">สมัครเรียน</a>
                        </li>
                        <li class="nav-item <?php echo $file_name == 'contact' ? 'active': '' ?>">
                            <a class="nav-link" href="contact.php">ติดต่อ</a>
                        </li>

                    </ul>

                </div>
            </div>
        </nav>

    </section>