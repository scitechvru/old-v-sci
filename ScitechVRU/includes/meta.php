    <!-- Search Engine -->
    <meta name="description" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="image" content="https://s-courses.com/sclass1/assets/images/bg.jpg">
    <meta name="keywords" content="สอนเขียนเว็บไซต์, html, css, javascript, php, mysql">
    <meta name="robots" content="index, nofollow">
    <meta name="web_author" content="AppzStory">

    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta property="fb:app_id" content="620851126015605" /> 
    <meta property="og:title"  content="ScitechVRU" /> 
    <meta property="og:description" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta property="og:image" content="https://s-courses.com/sclass1/assets/images/bg.jpg">
    <meta property="og:url" content="https://s-courses.com/sclass1/">
    <meta property="og:site_name" content="https://s-courses.com/">
    <meta property="og:type" content="website">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">