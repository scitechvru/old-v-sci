<!DOCTYPE html>
<html lang="en">

<head>
    <title>ScitechVRU About</title>
    <meta charset="UTF-8">
    <meta name="google-site-verification" content="9PR-_y1nMZeXzVEDJgarF2uOc0qzhM1JBMVILFCl5nA" />
    <!-- ทำให้บนมือถือซูมไม่ได้ (เฉพาะบางเบราเซอร์)-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <!-- CSS img gallery Ekko Lightbox-->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="node_modules/ekko-lightbox/dist/ekko-lightbox.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Search Engine -->
    <meta name="description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <meta name="keywords"
        content="คณะวิทย์ฯ, scitech, sci,vru,techno, information, computer, physics, chemistry, bio,ราชการ, สมัครงาน, joomla, จัดซื้อจัดจ้าง, บันทึกข้อความ, เรียนต่อ, โควตา, รับตรง, ภาคี,เทคโน, เทคคอม, มัลติ, มัลติมีเดีย, มีเดีย, การจัดการคอม, ห้องเรียนคอม, เฟรชชี่, น้องใหม่, นักศึกษา, วิทยาศาสตร์, สำนักงานคณะวิทย์, วิทย์, วิทยฯ,มหาวิทยาลัยราชภัฏ, ศูนย์วิทย์, ฟิสิกส์, ฟิสิก, ชีววะ, เคมี, สิ่งแวดล้อม, เทคโนโลยีสารสนเทศ, มหาวิทยาลัย,ราชภัฏ,วไลยอลงกรณ์,ในพระบรมราชูปถัมภ์,ปริญญาตรี,ปริญญาโท,เรียนต่อปริญญาโท,เรียนต่อปริญญาเอก"> 
    <meta name="robots" content="index, nofollow">
    <meta name="language" content="English">
    <meta name="author" content="ScitechVRU">
    <!-- Schema.org for Google -->
    <meta name="name" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์">
    <meta name="description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="image"
        content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="fb:app_id" content="620851126015605">
    <meta name="og:title" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์">
    <meta name="og:description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="og:image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <meta name="og:url" content="http://sci.vru.ac.th/">
    <meta name="og:type" content="website">
</head>

<body>

 <!-- Section Navbar -->
 <?php include_once('includes/navbar.php');?>
<!-- End Section Navbar -->

    <!-- Section Page-title -->

    <header class="jarallax" data-jarallax='{ "speed": 0.6 }' style="background-image: url( assets/image/img/1.jpg); ">
        <div class="page-image ">

            <h1 class="display-4 font-weight-bold">เกี่ยวกับเรา</h1>
            <p class="lead">ปรัชญา วิสัยทัศน์ พันธกิจ</p>

        </div>
    </header>

    <!-- Section Todo -->
    <section class="container py-5">

        <div class="row">
            <div class="col-lg-6 py-3 p-lg-0">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BpAB-pbNoL8" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-lg-6 ">
                <h2>ปรัชญา</h2>
                <p>วิทยาศาสตร์ก้าวล้ำ คุณธรรมเด่น เน้นการพัฒนา ด้วยภูมิปัญญาและเทคโนโลยี</p><br>
                <h3>วิสัยทัศน์</h3>
                <p>ร่วมแรงร่วมใจในการยกระดับคุณภาพและคุณค่าขององค์กรอย่างต่อเนื่อง ผ่านการผลิตบัณฑิตที่รับผิดชอบต่อสังคม สร้างนวันกรรมเชิงบูรณาการที่ตอบสนองต่อความต้องการของผู้มีส่วนได้ส่วนเสีย และการเสริมสร้างความเข้มแข็งให้กับชุมชนภาคีเครือข่ายผ่านงานพันธกิจสัมพันธ์</p>

            </div>
        </div>
        <div class="row py-3 p-lg-5">
            <div class="col ">
                <h3>พันธกิจ</h3>
                <p>1. รับผิดชอบต่อสังคมในการผลิตบัณฑิตที่มีความรู้คู่คุณธรรม ส่งเสริมการเรียนรู้ตลอดชีวิตในชุมชน ด้วยวิทยาศาสตร์และเทคโนโลยี โดยการจัดการเรียนรู้เชิงผลิตภาพ (Productive Learning) ด้วยรูปแบบ ABCD เพื่อช่วยให้คนในท้องถิ่นรู้เท่าทันการเปลี่ยนแปลง
                    <br> 2. ศึกษา วิจัย แสวงหาความจริง บนพื้นฐานของภูมิปัญญาท้องถิ่น ภูมิปัญญาไทยและภูมิปัญญาสากล รวมถึงการแสวงหาแนวทางเพื่อส่งเสริมให้เกิดการจัดการ การบำรุงรักษา และการใช้ประโยชน์จากทรัพยากรธรรมชาติและสิ่งแวดล้อมอย่างสมดุลและยั่งยืน สู่การบูรณาการที่ตอบสนองความต้องการในการพัฒนาชุมชนและท้องถิ่น
                    <br> 3. บูรณาการภูมิปัญญาพื้นบ้านกับเทคโนโลยีสมัยใหม่ที่เหมาะสมกับการดำรงชีวิต และการประกอบอาชีพของคนในท้องถิ่น เสริมสร้างงานพันธกิจสัมพันธ์ด้วยการประสานความร่วมมือระหว่างหน่วยงานและผู้มีส่วนได้ส่วนเสีย เพื่อความเข้มแข็งของคนในชุมชน
                    <br> 4.เสริมสร้างความรู้ความเข้าใจในคุณค่า ความสำนึก และความภูมิใจในวัฒนธรรมและภูมิปัญญาของท้องถิ่น มีการบูรณาการเข้ากับงานด้านพันธกิจสัมพันธ์และวิจัย และมีการเชิดชูเผยแพร่ จนได้รับการยกย่องจากชุมชน ภาคีเครือข่าย ผู้มีส่วนได้ส่วนเสียในภาคต่างๆ
                    อย่างต่อเนื่อง
                    <br> 5. ประสานความร่วมมือและช่วยเหลือเกื้อกูลของบุคลากรในองค์กร เพื่อการยกระดับการบริหารจัดการภายในที่ดี เป็นแบบอย่างให้กับหน่วยงานภายนอกได้ <br> 6. เสริมสร้างผู้ประกอบการรายใหม่จากศิษย์ปัจจุบัน ศิษย์เก่า ชุมชน ผู้มีส่วนได้ส่วนเสีย
                    ผ่านระบบบริหารจัดการ และการเสริมสร้างองค์ความรู้ที่เหมาะสม</p>
                <h3>เอกลักษณ์</h3>
                <p>เป็นคณะที่น้อมนำแนวทางการดำเนินชีวิตตามหลักเศรษฐกิจพอเพียง</p>
                <h3>อัตลักษณ์</h3>
                <p>บัณฑิตจิตอาสา พัฒนาท้องถิ่น</p>
            </div>
        </div>

    </section>


    <!-- Section Timeline -->
    <section class="jarallax" data-jarallax='{ "speed": 0.6 }' style="background-image: url( assets/image/img/1.jpg); ">
        <div class="page-image ">

            <!-- <img class="img-fluid" src="assets/image/logo.png" width="150" alt=" "> -->
            <h1 class="text-white display-4 font-weight-bold ">ประวัติความเป็นมา</h1>

        </div>
    </section>

    <section class="container py-5">
        <div class="row">
            <div class="col-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-badge">
                            <p class="text-info">2475</p>
                        </div>
                        <div class="timeline-card">
                            <h5>เมื่อปี พ.ศ. 2475</h5>
                            <p class="text-muted">
                                คณะวิทยาศาสตร์และเทคโนโลยีมีบทบาทหน้าที่ในการจัดการศึกษาด้านวิทยาศาสตร์และเทคโนโลยีมาอย่างต่อเนื่องตั้งแต่เริ่มก่อตั้งเป็นโรงเรียนฝึกหัดครูเพชรบุรีวิทยาลงกรณ์โดยคณะวิทยาศาสตร์และเทคโนโลยีมีฐานะเป็นหมวดวิชาวิทยาศาสตร์ในวิทยาลัยครูเพชรบุรีวิทยาลงกรณ์
                            </p>
                        </div>
                    </li>
                    <li class="inverted">
                        <div class="timeline-badge">
                            <p class="text-info">2513</p>
                        </div>
                        <div class="timeline-card">
                            <h5>เมื่อปี พ.ศ. 2513</h5>
                            <p class="text-muted">ได้จัดการศึกษาด้านวิทยาศาสตร์ ตั้งแต่ระดับประกาศนียบัตรวิชาการศึกษาและระดับประกาศนียบัตรวิชาการศึกษาชั้นสูง
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge">
                            <p class="text-info">2520</p>
                        </div>
                        <div class="timeline-card">
                            <h5>ปี พ.ศ. 2520</h5>
                            <p class="text-muted">
                                หมวดวิชาวิทยาศาสตร์ได้เปลี่ยนฐานะเป็นคณะวิชาวิทยาศาสตร์<br>และเทคโนโลยี&nbsp;ตามข้อบังคับของพระราชบัญญัติวิทยาลัยครู พ.ศ. 2518 ได้รวมหมวดวิชาต่างๆที่มีอยู่เดิมเข้าไว้ด้วยกันได้แก่<br>- หมวดวิชาวิทยาศาสตร์
                                <br> - หมวดวิชาคณิตศาสตร์<br>- หมวดวิชาพลศึกษาและสุขศึกษา
                                <br>- หมวดวิชาคหกรรมศาสตร์<br> - หมวดวิชาหัตถศึกษา<br>- หมวดวิชาเกษตรศาสตร์
                                <br>โครงสร้างการบริหารมีหัวหน้าคณะเป็นหัวหน้าหน่วยงาน ส่วนภายในคณะประกอบด้วยภาควิชาซึ่งบริหารโดยหัวหน้าภาควิชา ซึ่งมีจำนวนทั้งสิ้น 9 ภาควิชาได้แก่
                                <br> - ภาควิชาเกษตรศาสตร์<br>- ภาควิชาคณิตศาสตร์
                                <br>- ภาควิชาเคมี<br>- ภาควิชาชีววิทยา
                                <br> - ภาควิชาฟิสิกส์และวิทยาศาสตร์ทั่วไป<br>- ภาควิชาคหกรรมศาสตร์
                                <br>- ภาควิชาพลศึกษา<br>- ภาควิชาสุขศึกษา
                                <br>- ภาควิชาอุตสาหกรรมศิลป์<br> คณะวิชาวิทยาศาสตร์และเทคโนโลยีได้จัดการศึกษาโดยผลิตบัณฑิตครูวิทยาศาสตร์และคณิตศาสตร์ในระดับประกาศนียบัตรวิชาการศึกษา (ป.กศ.) ประกาศนียบัตรวิชาการศึกษาชั้นสูง (ป.กศ.สูง) และปริญญาตรีครุศาสตรบัณฑิต
                            </p>
                        </div>
                    </li>
                    <li class="inverted">
                        <div class="timeline-badge">
                            <p class="text-info">2523</p>
                        </div>
                        <div class="timeline-card">
                            <h5>ปี พ.ศ. 2523</h5>
                            <p class="text-muted"> ได้ยกเลิกการผลิตสาขาการศึกษาระดับประกาศนียบัตรวิชาการศึกษา แต่ยังคงจัดการศึกษาเฉพาะระดับประกาศนียบัตรวิชาการศึกษาชั้นสูงและระดับปริญญาตรี
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge">
                            <p class="text-info">2535</p>
                        </div>
                        <div class="timeline-card">
                            <h5>เมื่อวันที่ 14 กุมภาพันธ์ 2535</h5>
                            <p class="text-muted">พระบาทสมเด็จพระเจ้าอยู่หัว ทรงพระกรุณาโปรดเกล้าโปรดกระหม่อมพระราชทานนาม สถาบันราชภัฏแก่วิทยาลัยครูทั่วประเทศดังนั้นวิทยาลัยครูเพชรบุรีวิทยาลงกรณ์ในพระบรมราชูปถัมภ์ จึงใช้นามว่า สถาบันราชภัฏเพชรบุรีวิทยาลงกรณ์ในพระบรมราชูปถัมภ์และในปี
                                พ.ศ.2538 พระราชบัญญัติสถาบันราชภัฏได้ปรับเปลี่ยนคณะวิชาวิทยาศาสตร์และเทคโนโลยีเป็นคณะวิทยาศาสตร์และเทคโนโลยี และผู้บริหารคณะเปลี่ยนจากตำแหน่งหัวหน้าคณะวิชาเป็นตำแหน่งคณบดี
                            </p>
                        </div>
                    </li>
                    <li class="inverted">
                        <div class="timeline-badge">
                            <p class="text-info">2540</p>
                        </div>
                        <div class="timeline-card">
                            <h5>ปี พ.ศ. 2540</h5>
                            <p class="text-muted">การบริหารงานเปลี่ยนแปลงจากรูปแบบภาควิชามาเป็นการบริหารแบบโปรแกรมวิชา</p>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge">
                            <p class="text-info">2547</p>
                        </div>
                        <div class="timeline-card">
                            <h5>วันที่ 14 มิถุนายน 2547</h5>
                            <p class="text-muted">ได้มีการประกาศใช้พระราชบัญญัติมหาวิทยาลัยราชภัฏพุทธศักราช 2547 โดยประกาศในราชกิจจานุเบกษา ลงวันที่ 14 มิถุนายน 2547 เป็นผลให้สถาบันราชภัฏวไลยอลงกรณ์ในพระบรมราชูปถัมภ์เปลี่ยนเป็นมหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์
                                จังหวัดปทุมธานี
                            </p>
                        </div>
                    </li>
                    <li class="inverted">
                        <div class="timeline-badge">
                            <p class="text-info">2548</p>
                        </div>
                        <div class="timeline-card">
                            <h5>ปี พ.ศ. 2548</h5>
                            <p class="text-muted">ในภาคเรียนที่ 2 ปีการศึกษา 2548 ได้เปลี่ยนแปลงการบริหารจากโปรแกรมวิชามาเป็นการบริหารแบบสาขาวิชาและหลักสูตรโดยมีคณะกรรมการบริหารหลักสูตรและสาขาวิชาเป็นผู้บริหารจัดการศึกษาภายในหลักสูตรและสาขาวิชาจวบจนถึงปัจจุบัน
                            </p>
                        </div>
                    </li>
                    <li class="timeline-arrow">
                        <i class="fa fa-chevron-down"></i>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- Section Timeline -->
    <section class="jarallax" data-jarallax='{ "speed": 0.6 }' style="background-image: url( assets/image/img/1.jpg); ">
        <div class="page-image ">

            <!-- <img class="img-fluid" src="assets/image/logo.png" width="150" alt=" "> -->
            <h1 class="text-white display-4 font-weight-bold ">ค่านิยมหลัก</h1>

        </div>
    </section>
    <section class="container py-5">
        <div class="row">
            <div class="col-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-badge">
                            <p class="text-info">S</p>
                        </div>
                        <div class="timeline-card">
                            <h5>Social Responsibility</h5>
                            <p class="text-muted">
                                Social Responsibility = ความรับผิดชอบต่อสังคม
                            </p>
                        </div>
                    </li>
                    <li class="inverted">
                        <div class="timeline-badge">
                            <p class="text-info">C</p>
                        </div>
                        <div class="timeline-card">
                            <h5>Co-Operative Organization</h5>
                            <p class="text-muted">Co-Operative Organization = ความร่วมมือขององค์กร
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge">
                            <p class="text-info">I</p>
                        </div>
                        <div class="timeline-card">
                            <h5>Integrated innovation</h5>
                            <p class="text-muted">
                                Integrated innovation = นวัตกรรมเชิงบูรณาการ
                            </p>
                        </div>
                    </li>

                    <li class="timeline-arrow">
                        <i class="fa fa-chevron-down"></i>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- Section About -->
    <section class="jarallax" data-jarallax='{ "speed": 0.6 }' style="background-image: url( assets/image/img/1.jpg); ">
        <div class="page-image">

            <img class="img-fluid" src="assets/image/logo.png" width="150" alt=" ">
            <h2 class="text-white display-4 font-weight-bold ">Scitech VRU</h2>
            <div class="star-rating">
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
                <div class="star-current" style="width:100%;">
                    <span>★</span>
                    <span>★</span>
                    <span>★</span>
                    <span>★</span>
                    <span>★</span>
                </div>
            </div>

        </div>
    </section>

<!-- Section footer -->
<?php include_once('includes/footer.php');?>
<?php include_once('php/userlogs.php') ?>
<!-- End Section footer -->
<!-- Your ปลั๊กอินแชท code -->
<div id="fb-customer-chat" class="fb-customerchat">
    </div>
    <!-- Section On to Top -->
    <div class="to-top">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN7pVYXyLuKkftPkDMFhpTjov4MYVxTnY&callback=initMap"></script>
    <script src="assets/js/main.js "></script>
    <script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "1739708992734392");
    chatbox.setAttribute("attribution", "biz_inbox");
    </script>

    <!-- Your SDK code -->
    <script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: 'v12.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

</body>

</html>