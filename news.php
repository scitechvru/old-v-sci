<?php include 'headerccc.php';?>
<?php include 'menubarccc.php';

$type_news = $_GET[type_news];
$thaimonth=array(
  "00"=>"",
  "01"=>"ม.ค.",
  "02"=>"ก.พ.",
  "03"=>"มี.ค.",
  "04"=>"เม.ย.",
  "05"=>"พ.ค.",
  "06"=>"มิ.ย.",
  "07"=>"ก.ค.",
  "08"=>"ส.ค.",
  "09"=>"ก.ย",
  "10"=>"ต.ค.",
  "11"=>"พ.ย.",
  "12"=>"ธ.ค.");
  $news_type=array(
      "PR"=>"ประชาสัมพันธ์",
      "STU"=>"ข่าวสำหรับนักศึกษา",
      "ED"=>"ศึกษาต่อ",
      "RE"=>"สมัครงาน",
      "PRM"=>"ประชาสัมพันธ์หลักสูตร",
      "AL"=>"ศิษย์เก่า",
      );
?>
<br>
<br>
<div class="container">
  <table class="table table-striped table-dark">
    <thead class="thead-dark">
      <tr>
        <th class="text-center" scope="col">ลำดับ</th>
        <th class="text-center" scope="col" colspan="2">ข่าว<?php echo $news_type[$type_news]?></th>
        <th class="text-center" scope="col" colspan="2">วันที่ประกาศ</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      <?php 
$sql = "select * from tb_news where type_news='$type_news' and major_news='OF' and status_news='1' ORDER BY date_news DESC ";
$result = mysql_db_query($dbname, $sql);
$num = mysql_num_rows($result);
$no = 0;
if ($num > 0) {
    while ($r = mysql_fetch_array($result)) {
        $title_news = $r[title_news];
        $name_news = $r[name_news];
        $link_news = $r[link_news];
        $date_news = $r[date_news];
        $date1 = explode("-", $date_news);
        $date_news = $date1[2] . "/" . $thaimonth[$date1[1]] . "/" . ($date1[0]+543);
        $no++;
        ?>
        <td class="text-center"><?php echo "$no";?></td>
        <td colspan="2"><?php if ($link_news) {
            echo "<A HREF='$link_news' target='_blank'>$title_news</A><HR>";
        } else {
            echo "<A HREF='backend/files_news/$name_news' target='_blank'>$title_news</A><HR>";
        }?></td>
        <td colspan="2" class="text-center"><?php echo "$date_news"; ?></td>
      </tr>
      <?php
    } //endif
  } //endwhile
?>
    </tbody>
  </table>
</div>

<?php include 'footerccc.php';?>