<? include('header.php');?>
<? include('menubar.php'); ?>
<?
$db=$ssrvru->select_db_by_id('TBL_SCE_ACTIVITIES',array('activitiesid='=>$_REQUEST['actid']));
$youtube=explode("=",$db['activitiesyoutube']);
?>

<section class="latest-project sec-padd four-column" style="margin-top:-70px;">
  
    <div class="container">
        <div class="row">
        
<div class="col-lg-12 col-md-12 col-sm-12">
                <div class="outer-box">

                  <div class="section-title" style="margin-top:10px;">
                      <h3><i class="fa fa-picture-o"></i> <a href="activity.php">กิจกรรมเยี่ยมชม</a></h3>
                  </div>

                    <div class="row wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <h4 class="title-2"><?=$db['activitiestitle']?></h4><br>
                            <div class="text">
                                <p style="font-size:14px;"><?=$db['activitiesdetail']?></p>
                                  <? if($db['activitiesfiles'] !=''){ ?>
                                  <p align="right" style="font-size:12px;"><a target="_blank" href="<?=$pathsce_dcacti?><?=$db['activitiesfiles']?>"><i class="fa fa-download fa-lg"></i> เปิดอ่านไฟล์เอกสาร</a></p>
                                 <? }?>
<br>
                                <? if($db['activitiesyoutube'] !=''){ ?>
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                    <div class="video-image-box">
                                    <figure class="image"><img src="<?=$pathsce_dcacti_img?><?=$db['activitiesimg']?>"  alt=""><a href="<?=$db['activitiesyoutube']?>" class="video-fancybox overlay-link lightbox-image"><span class="icon icon-arrows4"></span></a></figure>

                                    </div>
                                    </div>
                                 <? } ?>


                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="img-box"><a href="#"><img src="<?=$pathsce_dcacti_img?><?=$db['activitiesimg']?>" width="590"></a></div>
                        </div>
                    </div><br><br>
                    <div class="border-bottom"></div>

<br>

<?
$sqls = "SELECT * FROM TBL_SCE_ACTIVITIES_GALLERY WHERE activitiesid='".$_REQUEST['actid']."'  ORDER BY agid DESC";
$rs = $condb->query($sqls);
$NRow=$rs->num_rows;
?>
<? if($NRow >0){?>

        <?

          while($dbimg=$rs->fetch_assoc())
            {
            ?>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="single-project">
                      <!--  <figure class="imghvr-shutter-in-out-horiz">
                          <img src="<?=$pathsce_dcacti_img?><?=$dbimg['agimg']?>">
                        </figure> -->
                        <div class="video-image-box">
                        <figure class="image"><img src="<?=$pathsce_dcacti_img?><?=$dbimg['agimg']?>" width="170" height="150"><a href="<?=$pathsce_dcacti_img?><?=$dbimg['agimg']?>" class="fancybox overlay-link lightbox-image" title="คลิกดูภาพใหญ่"></a></figure>

                        </div>
                    </div>
                </div>
            </article>
          <? } ?>


        </div>


    </div>
    <? } ?>
    <div align="right">
      <i class="fa fa-calendar"></i> วันที่ <?=$ssrvru->datethai($db['datecreated'])?><br/>
      <i class="fa fa-user"></i>  <?=$db['createdby']?><br/>
    </div>
</section>


                </div>
            </div>


<? include ('footer.php');?>