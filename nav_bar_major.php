<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
	.navbar {
      margin-bottom: 0;
      border-color:#ffdc6a; 
  	  background-color: #FFEB3B;
	}

  </style>
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar" >
      <ul class="nav navbar-nav ">
		  <li><a href="home.php"><span class="glyphicon glyphicon-home"></span> HOME</a></li>
			<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">เกี่ยวกับหลักสูตร <span class="caret"></span></a>
			  <ul class="dropdown-menu">
					<li><a href="#">ประวัติหลักสูตร</a></li>
					<li><a href="#">โครงสร้างหลักสูตร</a></li>
					<li><a href="#">รายละเอียดหลักสูตร</a></li>
					<li><a href="#">โครงสร้างบริหารหลักสูตร</a></li>
					<li><a href="#">นโยบายด้านคุณภาพ</a></li>					
					<li><a href="#">ปรัชญา วิสัยทัศน์ พันธกิจ</a></li>
	        </ul>
<!--
 <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">หน่วยงานภายใน<span class="caret"></span></a>
        <ul class="dropdown-menu">
<?php
$no=0;
include "connect.php";
$sql="select * from tb_major where int_major != 'OF' and status_major='1' order by nameen_major";
$result=mysql_db_query($dbname,$sql);
$num=mysql_num_rows($result);
if($num>0) {
	while ($r=mysql_fetch_array($result)) {
		$nameth_major=$r[nameth_major];
		$link_major=$r[link_major];
		$no++;
?>
          <li><a href="<?php echo"$link_major";?>" target="_blank"><?php echo"$nameth_major";?></a></li>
<?php } //end while ?>
        </ul>
<?php } //end if ?>


 <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">อาจารย์และบุคลากร <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="#">อาจารย์และบุคลากรประจำหลักสูตร</a></li>
		<li><a href="#news">คณะกรรมการบริหารคณะ</a></li>
		  <li><a href="per_aca.php">บุคลากรสายวิชาการ</a></li>
		  <li><a href="per_sup.php">บุคลากรสายสนับสนุน</a></li>
        </ul>-->

<li><a href="home.php">อาจารย์และบุคลากร</a></li>

 <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">แบบฟอร์ม / คู่มือ <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="form_student.php">นักศึกษา</a></li>
		  <li><a href="manual_students.php">คู่มือนักศึกษา</a></li>
		  <li><a href="form_personal.php">อาจารย์/เจ้าหน้าที่</a></li>
		  <li><a href="form_reg.php">ระเบียบ/ข้อบังคับ/กฎหมาย</a></li>
		  <li><a href="form_research.php">วิจัย</a></li>
		  <li><a href="form_information.php">สารสนเทศ</a></li>
		  <li><a href="form_qua.php">ประกันคุณภาพ</a></li>
	  </ul>
<!--
 <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">วาระการประชุม <span class="caret"></span></a>
        <ul class="dropdown-menu">
		  <li><a href="report_exedir.php">มติกรรมการบริหารคณะ</a></li>
		  <li><a href="report_acacom.php">มติกรรมการวิชาการคณะ</a></li>
	  </ul>
	    <li><a href="http://sci.vru.ac.th/oldweb/" target="blank"><B>Old Web</B></a></li>
-->

	  </ul>
      <ul class="nav navbar-nav navbar-right">
	    <li><a href="https://forms.gle/4WYGkwpKAhX387Du6" target="blank">รับสมัครนักศึกษา</a></li>
		<li><a href="contact.php"><span class="glyphicon glyphicon-phone"></span> Contact</a></li>
<!--
		<li><a href="register_sm.php">Register</a></li>
        <li><a href="admin_login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
-->
      </ul>
    </div>
  </div>
</nav>

