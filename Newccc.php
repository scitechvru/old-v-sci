<?php
$thaimonth=array(
    "00"=>"",
    "01"=>"ม.ค.",
    "02"=>"ก.พ.",
    "03"=>"มี.ค.",
    "04"=>"เม.ย.",
    "05"=>"พ.ค.",
    "06"=>"มิ.ย.",
    "07"=>"ก.ค.",
    "08"=>"ส.ค.",
    "09"=>"ก.ย.",
    "10"=>"ต.ค.",
    "11"=>"พ.ย.",
    "12"=>"ธ.ค.");
?>
<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 0px solid #ccc;
  background-color: #fff;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>
<div class="container">
    <div class="section-title">
                <h2>ประกาศ</h2>
    </div>
    <div class="container">
      <h1 class="my-4" style="font-family: 'Fira Sans Extra Condensed', sans-serif;color: #086A87; font-size:28px" >ประกาศรายชื่อผู้มีสิทธิ์ศึกษาต่อ(สมัครผ่านทางคณะวิทยาศาสตร์และเทคโนโลยี)</h1>
      <div class="row" style="text-align: center;margin-top: 5%">
			
			<ul class="w3-ul w3-margin-top" >
 <?php
$thaimonth=array(
    "00"=>"",
    "01"=>"ม.ค.",
    "02"=>"ก.พ.",
    "03"=>"มี.ค.",
    "04"=>"เม.ย.",
    "05"=>"พ.ค.",
    "06"=>"มิ.ย.",
    "07"=>"ก.ค.",
    "08"=>"ส.ค.",
    "09"=>"ก.ย.",
    "10"=>"ต.ค.",
    "11"=>"พ.ย.",
    "12"=>"ธ.ค.");
    $no = 0;
    $sql = "select * from tb_news where type_news='ED' and major_news='OF' and status_news='1' order by  date_news desc ";
    $result = $conn->query($sql) or die($conn->error);
    if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()){



        
        if ($row['link_news'] ) {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='$row[link_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } else {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='backend/files_news/$row[name_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } //endif
        if ($no == 5) {
        break;
    } //endif
    $no++;
    } //endwhile
    echo " <div align='right'><A HREF='news.php?type_news=ED'>More...<BR></A></div>";
} //endif
?>
</div>
</div>
    <br>

<div class="container">
    <div class="section-title">
                <h2>ข่าว</h2>
    </div>
    <div class="tab">
  <button class="tablinks" onclick="openCity(event, 'PR')" id="defaultOpen" >ข่าวประชาสัมพันธ์</button>
  <button class="tablinks" onclick="openCity(event, 'STU')">ข่าวสำหรับนักศึกษา</button>
  <button class="tablinks" onclick="openCity(event, 'ED')">ข่าวศึกษาต่อ</button>
  <button class="tablinks" onclick="openCity(event, 'RE')">ข่าวรับสมัครงาน</button>
  <button class="tablinks" onclick="openCity(event, 'AL')">ข่าวศิษย์เก่า</button>
  <button class="tablinks" onclick="openCity(event, 'ITA')">Work@Home</button>
</div>

<div id="PR" class="tabcontent">
<?php
include "inc/connect.php";
$no = 0;
$sql = "select * from tb_news where type_news='PR' and major_news='OF' and status_news='1' order by  date_news desc";
$result = $conn->query($sql) or die($conn->error);
if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()){
        
        if ($row['link_news'] ) {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='$row[link_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } else {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='backend/files_news/$row[name_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } //endif
        
        if ($no == 5) {
            break;
        } //endif
        $no++;
    } //endif
    echo " <div align='right'><A HREF='news.php?type_news=PR'>More...<BR></A></div>";
} //endwhile
?>            </p>
        </div><!--<<<<PR-->

        <div id="STU" class="tabcontent">
<?php
include "inc/connect.php";
$no=0;
$sql = "select * from tb_news where type_news='STU' and major_news='OF' and status_news='1' order by  date_news desc";
$result = $conn->query($sql) or die($conn->error);
if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()){
        if ($row['link_news'] ) {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='$row[link_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } else {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='backend/files_news/$row[name_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } //endif
        
        if ($no == 5) {
            break;
        } //endif
        $no++;
    } //endif
    echo " <div align='right'><A HREF='news.php?type_news=STU'>More...<BR></A></div>";
} //endwhile
?>            </p>
        </div><!--<<<<STU-->

 <div id="ED" class="tabcontent">
<?php
include "inc/connect.php";
$no=0;
$sql = "select * from tb_news where type_news='ED' and major_news='OF' and status_news='1' order by date_news desc";
$result = $conn->query($sql) or die($conn->error);
if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()){
        if ($row['link_news'] ) {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='$row[link_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } else {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='backend/files_news/$row[name_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } //endif
        
        if ($no == 5) {
            break;
        } //endif
        $no++;
    } //endif
    echo " <div align='right'><A HREF='news.php?type_news=ED'>More...<BR></A></div>";
} //endwhile
?>            </p>
        </div><!--<<<<PR-->

<div id="RE" class="tabcontent">
<?php
include "inc/connect.php";
$no=0;
$sql = "select * from tb_news where type_news='RE' and major_news='OF' and status_news='1' order by  date_news desc";
$result = $conn->query($sql) or die($conn->error);
if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()){
        if ($row['link_news'] ) {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='$row[link_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } else {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='backend/files_news/$row[name_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } //endif
        
        if ($no == 5) {
            break;
        } //endif
        $no++;
    } //endif
    echo " <div align='right'><A HREF='news.php?type_news=RE'>More...<BR></A></div>";
} //endwhile
?>            </p>
        </div><!--<<<<PR-->


    <div id="AL" class="tabcontent">
    <?php
include "inc/connect.php";
$no=0;
$sql = "select * from tb_news where type_news='AL' and major_news='OF' and status_news='1' order by date_news desc";
$result = $conn->query($sql) or die($conn->error);
if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()){
        if ($row['link_news'] ) {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='$row[link_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } else {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='backend/files_news/$row[name_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } //endif
        
        if ($no == 5) {
            break;
        } //endif
        $no++;
    } //endif
    echo " <div align='right'><A HREF='news.php?type_news=AL'>More...<BR></A></div>";
} //endwhile
?>            </p>
        </div><!--<<<<PR-->

        <div id="ITA" class="tabcontent">
    <?php
include "inc/connect.php";
$no=0;
$sql = "select * from tb_news where type_news='ITA' and major_news='OF' and status_news='1' order by date_news desc";
$result = $conn->query($sql) or die($conn->error);
if ($result->num_rows > 0){
    while ($row = $result->fetch_assoc()){
        if ($row['link_news'] ) {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='$row[link_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } else {
            echo " <div align='left' style=' color: #086A87; '>  <A HREF='backend/files_news/$row[name_news]' target='_blank'>$row[title_news] </A><HR></div>";
        } //endif
        
        if ($no == 5) {
            break;
        } //endif
        $no++;
    } //endif
    echo " <div align='right'><A HREF='news.php?type_news=ITA'>More...<BR></A></div>";
} //endwhile
?>            </p>
        </div><!--<<<<PR-->
    </div>
</div>



</div>
<br>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();
</script>

