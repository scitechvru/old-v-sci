<?php
include 'headerccc.php';
include 'menubarccc.php';
require 'inc/connect.php';
include 'backend/educationlist.php';
include "backend/array.php";
?>

<style>
.our-team {
    border: 1px solid #d3d3d3;
    position: relative;
    overflow: hidden;
}

.our-team img {
    width: 100%;
    height: 100%;
}

.our-team .team-content {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    padding: 45px 18px;
    background: rgba(0, 0, 0, 0.7);
    transform: translateX(-100%);
    transition: all 0.20s ease 0s;
}

.our-team:hover .team-content {
    transform: translateX(0);
}

.our-team .team-content .post-title {
    font-size: 18px;
    color: #fff;
    text-transform: uppercase;
}

.our-team .team-content .post {
    font-size: 14px;
    color: #cb95e1;
    display: block;
    margin-bottom: 20px;
}

.our-team .description {
    font-size: 14px;
    line-height: 25px;
    color: #fff;
    margin-bottom: 20px;
}

.our-team .team_social {
    margin: 0;
    padding: 0;
    list-style: none;
}

.our-team .team_social li {
    display: inline-block;
    margin-right: 5px;
}

.our-team .team_social li a {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    border: 2px solid #f5f5f5;
    font-size: 17px;
    color: #f5f5f5;
    line-height: 40px;
    text-align: center;
    display: inline-block;
    transition: border 0.3s ease 0s;
}

.our-team .team_social li a:hover {
    border-color: transparent;
}

.our-team .team-prof {
    width: 100%;
    position: absolute;
    bottom: 0;
    text-align: right;
    padding: 20px 16px;
    background: rgba(0, 0, 0, 0.7);
    transform: translateX(0);
    transition: all 0.20s ease 0s;
}

.our-team:hover .team-prof {
    transform: translateX(100%);
}

.our-team .team-prof .post-title {
    font-size: 14px;
    color: #fff;
    margin: 0 0 8px 0;
    text-transform: uppercase;
}

.our-team .team-prof .post {
    font-size: 14px;
    color: #cb95e1;
    margin-bottom: 0;
}

@media only screen and (max-width: 990px) {
    .our-team {
        margin-bottom: 20px;
    }
}
</style>
<div class="container">
    <div class="row">
    <h1 class="text-center">บุคลากรสายวิชาการ</h1>
        <?php $no = 0;
$sql2 = "SELECT * from tb_per tp where (tp.type_per='1' or tp.type_per='3') and  tp.status_per='1' Order by major_per";
$result2 = mysql_db_query($dbname, $sql2);
$num2 = mysql_num_rows($result2);
if ($num2 > 0) {
    while ($r2 = mysql_fetch_array($result2)) {
        $id_per = $r2[id_per];
        $idcard_per = $r2[idcard_per];
        $tname2_per = $r2[tname2_per];
        $tname1_per = $r2[tname1_per];
        $tname_per = $r2[tname_per];
        $fnamet_per = $r2[fnamet_per];
        $email_per = $r2[email_per];
        $lnamet_per = $r2[lnamet_per];
        $major_per = $r2[major_per];
        $type_per = $r2[type_per];
        $img_per = $r2[img_per];
        $status_per = $r2[status_per];

        if ($tname2_per or $tname1_per) {
            $tname_per = $tname2_per.$tname1_per;
        }

        ++$no;
        if ($img_per) { ?>

        <div class="col-md-3 col-sm-6">
            <div class="our-team">
                <img src="./backend/img_resize/<?php echo "$img_per"; ?>"
                    alt="<?php echo "$tname_per $fnamet_per  $lnamet_per"; ?>">
                <div class="team-content">
                    <h3 class="post-title"><?php echo "$tname_per $fnamet_per  $lnamet_per"; ?></h3>
                    <span class="post"><?php echo "$major[$major_per]"; ?></span>
                    <span class="post"><?php echo "($email_per)"; ?></span>
                    
                    <ul class="team_social">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo "$idcard_per"; ?>">
                            ดูข้อมูลเพิ่มเติม
                        </button>
                    </ul>
                </div>
                <div class="team-prof">
                    <h3 class="post-title"><?php echo "$tname_per $fnamet_per  $lnamet_per"; ?></h3><br>
                    <span class="post"><?php echo "$major[$major_per]"; ?></span><br>
                    <span class="post"><?php echo "($email_per)"; ?></span>
                </div>
            </div>
        </div>
        
<!-- Modal-->
<div class="modal fade bd-example-modal-lg" id="modal<?php echo "$idcard_per"; ?>" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="exampleModalLabel">ข้อมูลเพิ่มเติม</h3>

            </div>
           
            <div class="modal-body">
                <div class="col">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-4">ชื่ออาจารย์</dt>
                                <dd class="col-sm-8"><?php echo "$tname_per $fnamet_per  $lnamet_per"; ?></dd>
                                <dt class="col-sm-4">ประจำหลักสูตร</dt>
                                <dd class="col-sm-8"><?php echo "$major[$major_per]"; ?></dd>
                                
                                <dt class="col-sm-4">วุฒิการศึกษา</dt>
                                <?php $no = 0;
                                $sql3 = "SELECT * from tb_ed te where te.idcard_ed = '$idcard_per'";
                                $result3 = mysql_db_query($dbname, $sql3);
                                $num3 = mysql_num_rows($result3);
                                if ($num3 > 0) {
                                    while ($r3 = mysql_fetch_array($result3)) {
                                        
                                        $level_ed = $r3[level_ed];
                                        $major_ed = $r3[major_ed];
                                        $abb_ed = $r3[abb_ed];
                                        $edu_ed = $r3[edu_ed];
                                        $y_ed = $r3[y_ed]; ?>
                                        <dt class="col-sm-4"></dt>
                                <dd class="col-sm-8"><?php echo "$p[$edu_ed]"; ?><br> &nbsp; &nbsp; <?php echo "$major_ed"; ?> <?php echo "$abb_ed"; ?> จบการศึกษาปี พ.ศ.<?php echo "$y_ed"; ?>
                                </dd>
                                <?php
                                    }
                                } ?>
                                <!--<dt class="col-sm-4">ผลงาน</dt>
                                <dd class="col-sm-8">Fusce dapibus, tellus ac cursus commodo, tortor mauris
                                    condimentum nibh, ut fermentum massa justo
                                    sit amet risus.
                                </dd>-->
                            </dl>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
  
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>

            </div>
        </div>
    </div>
</div>

        <?php         } // end while
    }// end while
}// end if

?>
    </div>
</div>

<?php include 'footerccc.php'; ?>