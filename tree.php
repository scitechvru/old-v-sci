<?php include "headerccc.php"; ?>
<?php include "menubarccc.php"; ?>
<br><br>
<style>
/*Now the CSS*/
/*
* {margin: 0; padding: 0;}

.tree ul {
	padding-top: 20px; position: relative;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

.tree li {
	float: left; text-align: center;
	list-style-type: none;
	position: relative;
	padding: 20px 5px 0 5px;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}
*/
/*We will use ::before and ::after to draw the connectors*/
/*
.tree li::before, .tree li::after{
	content: '';
	position: absolute; top: 0; right: 50%;
	border-top: 1px solid #ccc;
	width: 50%; height: 20px;
}
.tree li::after{
	right: auto; left: 50%;
	border-left: 1px solid #ccc;
}
*/
/*We need to remove left-right connectors from elements without 
any siblings*/
/*
.tree li:only-child::after, .tree li:only-child::before {
	display: none;
}
*/
/*Remove space from the top of single children*/
/*
.tree li:only-child{ padding-top: 0;}
*/
/*Remove left connector from first child and 
right connector from last child*/
/*
.tree li:first-child::before, .tree li:last-child::after{
	border: 0 none;
}*/
/*Adding back the vertical connector to the last nodes*/
/*
.tree li:last-child::before{
	border-right: 1px solid #ccc;
	border-radius: 0 5px 0 0;
	-webkit-border-radius: 0 5px 0 0;
	-moz-border-radius: 0 5px 0 0;
}
.tree li:first-child::after{
	border-radius: 5px 0 0 0;
	-webkit-border-radius: 5px 0 0 0;
	-moz-border-radius: 5px 0 0 0;
}
*/
/*Time to add downward connectors from parents*/
/*
.tree ul ul::before{
	content: '';
	position: absolute; top: 0; left: 50%;
	border-left: 1px solid #ccc;
	width: 0; height: 20px;
}

.tree li a{
	border: 1px solid #ccc;
	padding: 5px 10px;
	text-decoration: none;
	color: #666;
	font-family: arial, verdana, tahoma;
	font-size: 11px;
	display: inline-block;
	
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}
*/
/*Time for some hover effects*/
/*We will apply the hover effect the the lineage of the element also*/
/*
.tree li a:hover, .tree li a:hover+ul li a {
	background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
}*/
/*Connector styles on hover*/
/*
.tree li a:hover+ul li::after, 
.tree li a:hover+ul li::before, 
.tree li a:hover+ul::before, 
.tree li a:hover+ul ul::before{
	border-color:  #94a0b4;
}*/

main-timeline {
    font-family: 'Roboto', sans-serif;
}

.main-timeline:after {
    content: '';
    display: block;
    clear: both;
}

.main-timeline:before {
    content: '';
    height: 100%;
    width: 2px;
    border: 2px dashed #000;
    transform: translateX(-50%);
    position: absolute;
    left: 50%;
    top: 30px;
}

.main-timeline .timeline {
    width: 50%;
    padding: 100px 70px 0 25px;
    margin: 0 50px 0 0;
    float: left;
    position: relative;
}

.main-timeline .timeline-content {
    padding: 15px 15px 15px 40px;
    border: 2px solid #00A79B;
    border-radius: 15px 0 15px 15px;
    display: block;
    position: relative;
}

.main-timeline .timeline-content:hover {
    text-decoration: none;
}

.main-timeline .timeline-content:after {
    content: '';
    background-color: #00A79B;
    height: 18px;
    width: 15px;
    position: absolute;
    right: -15px;
    top: -2px;
    clip-path: polygon(100% 0, 0 0, 0 100%);
}

.main-timeline .timeline-year {
    color: #fff;
    background-color: #00A79B;
    font-size: 32px;
    font-weight: 900;
    text-align: center;
    line-height: 98px;
    height: 100px;
    width: 100px;
    border-radius: 50%;
    position: absolute;
    right: -120px;
    top: -85px;
}

.main-timeline .timeline-year:after {
    content: '';
    height: 130px;
    width: 130px;
    border: 8px solid #00A79B;
    border-left-color: transparent;
    border-radius: 50%;
    transform: translateX(-50%) translateY(-50%) rotate(-20deg);
    position: absolute;
    left: 50%;
    top: 50%;
}

.main-timeline .timeline-icon {
    color: #fff;
    background-color: #00A79B;
    font-size: 35px;
    text-align: center;
    line-height: 50px;
    height: 50px;
    width: 50px;
    border-radius: 50%;
    transform: translateY(-50%);
    position: absolute;
    top: 50%;
    left: -25px;
    transition: all 0.3s;
}

.main-timeline .title {
    color: #222;
    font-size: 20px;
    font-weight: 900;
    text-transform: uppercase;
    letter-spacing: 1px;
    margin: 0 0 7px 0;
}

.main-timeline .description {
    color: #222;
    font-size: 15px;
    letter-spacing: 1px;
    text-align: justify;
    margin: 0 0 5px;
}

.main-timeline .timeline:nth-child(even) {
    padding: 100px 25px 0 70px;
    margin: 0 0 0 50px;
    float: right;
}

.main-timeline .timeline:nth-child(even) .timeline-content {
    padding: 15px 40px 15px 15px;
    border-radius: 0 15px 15px 15px;
}

.main-timeline .timeline:nth-child(even) .timeline-content:after {
    transform: rotateY(180deg);
    right: auto;
    left: -15px;
}

.main-timeline .timeline:nth-child(even) .timeline-year {
    right: auto;
    left: -120px;
}

.main-timeline .timeline:nth-child(even) .timeline-year:after {
    transform: translateX(-50%) translateY(-50%) rotate(200deg);
}

.main-timeline .timeline:nth-child(even) .timeline-icon {
    left: auto;
    right: -25px;
}

.timeline:nth-child(4n+2) .timeline-content,
.timeline:nth-child(4n+2) .timeline-year:after {
    border-color: #9E005D;
}

.timeline:nth-child(4n+2) .timeline-year:after {
    border-left-color: transparent;
}

.timeline:nth-child(4n+2) .timeline-content:after,
.timeline:nth-child(4n+2) .timeline-icon,
.timeline:nth-child(4n+2) .timeline-year {
    background-color: #9E005D;
}

.timeline:nth-child(4n+3) .timeline-content,
.timeline:nth-child(4n+3) .timeline-year:after {
    border-color: #f24f0e;
}

.timeline:nth-child(4n+3) .timeline-year:after {
    border-left-color: transparent;
}

.timeline:nth-child(4n+3) .timeline-content:after,
.timeline:nth-child(4n+3) .timeline-icon,
.timeline:nth-child(4n+3) .timeline-year {
    background-color: #f24f0e;
}

.timeline:nth-child(4n+4) .timeline-content,
.timeline:nth-child(4n+4) .timeline-year:after {
    border-color: #0870C5;
}

.timeline:nth-child(4n+4) .timeline-year:after {
    border-left-color: transparent;
}

.timeline:nth-child(4n+4) .timeline-content:after,
.timeline:nth-child(4n+4) .timeline-icon,
.timeline:nth-child(4n+4) .timeline-year {
    background-color: #0870C5;
}

@media screen and (max-width:767px) {
    .main-timeline:before {
        display: none;
    }

    .main-timeline .timeline {
        width: 100%;
        padding-top: 80px;
        padding-right: 12px;
        margin-bottom: 20px;
    }

    .main-timeline .timeline:nth-child(even) {
        padding-left: 10px;
        padding-top: 80px;
        margin-bottom: 20px;
    }

    .main-timeline .timeline-content,
    .main-timeline .main-timeline .timeline:nth-child(even) .timeline-content {
        background-color: #fff;
        padding-top: 25px;
    }

    .main-timeline .timeline-content:after {
        display: none;
    }

    .main-timeline .timeline-year {
        font-size: 24px;
        line-height: 70px;
        height: 70px;
        width: 70px;
        right: 0;
        top: -65px;
    }

    .main-timeline .timeline-year:after {
        display: none;
    }

    .main-timeline .timeline:nth-child(even) .timeline-year {
        left: 3px;
    }
}

@media screen and (max-width:567px) {
    .main-timeline .title {
        font-size: 18px;
    }
}
</style>
<!--
<div class="tree">
	<ul>
		<li>
			<a href="#">Parent</a>
			<ul>
				<li>
					<a href="#">Child</a>
					<ul>
						<li>
							<a href="#">Grand Child</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="#">Child</a>
					<ul>
						<li><a href="#">Grand Child</a></li>
						<li>
							<a href="#">Grand Child</a>
							<ul>
								<li>
									<a href="#">Great Grand Child</a>
								</li>
								<li>
									<a href="#">Great Grand Child</a>
								</li>
								<li>
									<a href="#">Great Grand Child</a>
								</li>
							</ul>
						</li>
						<li><a href="#">Grand Child</a></li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</div>

-->

<div class="container">
    <h1>ค่านิยมหลัก</h1>
    <p> Core value ของคณะวิทยาศาสตร์และเทคโนโลยีมาจากคำว่า SCI </p>
    <br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="main-timeline">
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>S</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-rocket"></i>
                        </div>
                        <h3 class="title">Social Responsibility</h3>
                        <p class="description">
                            Social Responsibility = ความรับผิดชอบต่อสังคม
                        </p>
                    </a>
                </div>
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>C</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-space-shuttle"></i>
                        </div>
                        <h3 class="title">Co-Operative Organization</h3>
                        <p class="description">
                        Co-Operative Organization = ความร่วมมือขององค์กร
                        </p>
                    </a>
                </div>
                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <div class="timeline-year">
                            <span>I</span>
                        </div>
                        <div class="timeline-icon">
                            <i class="fa fa-star"></i>
                        </div>
                        <h3 class="title">Integrated innovation</h3>
                        <p class="description">
                        Integrated innovation = นวัตกรรมเชิงบูรณาการ
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
<?php include "footerccc.php";?>