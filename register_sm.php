<?php include 'headerccc.php';?>
<style>
/* Just for styling */
.styling {
    background: #f5f5f5;
    border: 1px solid #ccc;
    border-radius: 5px;
    padding: 20px;
    text-align: center;
    /* height: 150px;
  overflow: auto; */
}
</style>
<br>
<div class="container">
    
</div>

<form class="md-form" action="register_sm2.php" method="post"  ENCTYPE="multipart/form-data">
    <div class="container">
        <h2 style="text-align:center;">ลงทะเบียนอาจารย์และบุคลากร</h2>
        <h5 style="text-align:right;"><a href="manual.php" style="text-align:right;">คู่มือประกอบการลงทะเบียน</a> <a href="http://sci.vru.ac.th/backend/files/%E0%B8%84%E0%B8%B9%E0%B9%88%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%A5%E0%B8%87%E0%B8%97%E0%B8%B0%E0%B9%80%E0%B8%9A%E0%B8%B5%E0%B8%A2%E0%B8%99.pdf">pdf</a> </h4>
        
        
        <!--<h2>คู่มือการลงทะเบียน <input id="btnPassport" class="btn btn-primary" type="button" value="show" name="btnPassport"></h2>
        <div id="dvPassport" style="display: none">
        <img src="backend/images/manual.png" alt="manualregister">
    </div>-->
        <br><br>
        <div class="form-row">
            <div class="col-md-4 mb-3">
                <label for="inputState">คำนำหน้าชื่อ</label>
                <select name="NameTitle" id="NameTitle" class="form-control">
                    <option selected>Choose...</option>
                    <OPTION VALUE='นางสาว'>นางสาว (Ms.)</OPTION>
                    <OPTION VALUE='นาง'>นาง (Mrs.)</OPTION>
                    <OPTION VALUE='นาย'>นาย (Mr.)</OPTION>
                </select>
            </div>
            <div class="col-md-4 mb-3">
                <label for="inputEmail4">ชื่อ - ภาษาไทย</label>
                <input type="text" class="form-control" id="inputEmail4" name="NameFirst">
            </div>
            <div class="col-md-4 mb-3">
                <label for="inputPassword4">นามสกุล - ภาษาไทย</label>
                <input type="text" name="NameLast" class="form-control" id="inputPassword4">
            </div>
            <div>
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="inputAddress">หมายเลขบัตรประชาชน</label>
                        <input type="text" class="form-control" maxlength="13" NAME="DescCardId" placeholder="">
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="inputAddress">วัน/เดือน/ปีเกิด (ค.ศ.)</label>
                        <?php include "inputdate.php";?>
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control input-lg" name="DescBirthDate" />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="inputState">ประเภทบุคลากร</label>
                    <select id="TypePer" class="form-control" name="TypePer">
                        <option selected>Choose...</option>
                        <OPTION VALUE='1'>พนักงานมหาวิทยาลัยสายวิชาการ</OPTION>
                        <OPTION VALUE='2'>พนักงานมหาวิทยาลัยสายสนับสนุน</OPTION>
                        <OPTION VALUE='3'>ข้าราชการสายวิชาการ</OPTION>
                        <OPTION VALUE='4'>ข้าราชการสายสนับสนุน</OPTION>
                        <OPTION VALUE='5'>พนักงานประจำ</OPTION>
                    </select>
                    <small id="passwordHelpInline" class="text-muted">
                        โปรดเลือกประเภทบุคลากรของท่าน
                    </small>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="inputState">สังกัด</label>
                    <select name="Major" id="Major" class="form-control">
                        <option selected>Choose...</option>
                        <?php   
				include "inc/connect.php";
				$sql="select * from tb_major order by id_major";
				$result=mysql_db_query($dbname,$sql);
				$qr=mysql_query($sql);
				while($result=mysql_fetch_array($qr)){  
					$id_major = $result['id_major'];
					$nameth_major = $result['nameth_major'];
					$int_major = $result['int_major'];
			?>
                        <option value="<?php echo "$int_major"; ?>"><?php echo "$nameth_major"; ?></option>
                        <?php  } ?>
                    </select>
                    <small id="passwordHelpInline" class="text-muted">
                        โปรดระบุสังกัดของท่าน
                    </small>
                </div>
             <div class="col-md-4 mb-3">
                    <div class="file-field">
                        <div class="btn btn-unique btn-sm float-left">
                            <label for="inputState">โปรไฟล์</label>
                            <input class="btn btn-large btn-block btn-info" TYPE="file" NAME="fileupload">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    
                    
                    <button type="submit" class="btn btn-large btn-block btn-success"value="SAVE">ลงทะเบียน</button>
                    
                    <a class="btn btn-large btn-block btn-warning" href="home.php" >กลับหน้าหลัก</a>
                    
                </div>
            </div>

        </div>
    </div>
</form>
<br><br>
<?php include 'footerccc.php';?>




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js">
</script>
<Script>
$(function() {
    var bindDatePicker = function() {
        $(".date").datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        }).find('input:first').on("blur", function() {
            // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
            // update the format if it's yyyy-mm-dd
            var date = parseDate($(this).val());

            if (!isValidDate(date)) {
                //create date based on momentjs (we have that)
                date = moment().format('YYYY-MM-DD');
            }

            $(this).val(date);
        });
    }

    var isValidDate = function(value, format) {
        format = format || false;
        // lets parse the date to the best of our knowledge
        if (format) {
            value = parseDate(value);
        }

        var timestamp = Date.parse(value);

        return isNaN(timestamp) == false;
    }

    var parseDate = function(value) {
        var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
        if (m)
            value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

        return value;
    }

    bindDatePicker();
});
</script>
<script>
$(function() {
    $("#btnPassport").click(function() {
        if ($(this).val() == "show") {
            $("#dvPassport").show();
            $(this).val("hide");
        } else {
            $("#dvPassport").hide();
            $(this).val("show");
        }
    });
});
</script>